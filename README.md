<h1 align="center">UNICORN 🦄</h1>
<p align="center">Unified Net-based Interface for Competition Organization, Rules and News.</p>

TODO Make users-duck react on successful login dispatch
TODO Make auth reducer react on fetching own user

# Motivation

At The Gathering, there has always been a tradition for creative competitions with music, graphics and demos. In 2014 we started working on a competition system to support the creative competitions at The Gathering, and their needs. Summed up, this was handling entries, qualifying/disqualifying, enabling an API for showing entries on stage, votes and results.
Over the years this has escalated a bit by taking on game competitions and achievements.

The system has been the victim of about 4 rewrites as we have learned a lot, and made a ton of mistakes. "About" because it kind of depends of how you count. This is hopefully the last complete rewrite, where we have separated the frontend into it's own service based on React.

# Getting started

To run UNICORN yourself, you pretty much just have to fork and clone this project (and a few other things).

At this point of time, you will need a UNICORN backend running, and an authentication service. The UNICORN backend will be open sourced later this year, but for now you will need access to a running instance.

## Environmentvariables

Add the necessary variables to your environment before continuing:

| Name                    | Description                              |
| ----------------------- | ---------------------------------------- |
| REACT_APP_URL           | Backend service URL                      |
| REACT_APP_API_URL       | API URL (typically \$REACT_APP_URL/api/) |
| REACT_APP_AUTH_URL      | Authentication service URL               |
| REACT_APP_CLIENT_SECRET | Authentication service client secret     |
| REACT_APP_CLIENT_ID     | Authentication service client ID         |
| REACT_APP_OSCAR_LINK    | Authorization URL                        |

## Running

Run `yarn` and `yarn start` 🚀

# Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

# Contributing

Please send an MR for any cool features or changes you think we could benefit from 💕

# Deployment with now.sh from Zeit

Want to run UNICORN yourself? Awesome! Currently we've implemented support for GitLab with GitLab Runner and now.sh. This can be seen in the .gitlab-ci file, but you will need to know about a few variables:

`ZEIT_BASE_DOMAIN` your default zeit root domain.

`NOW_TOKEN` your now.sh token, which should be set through your projects CI/CD settings. The token could be found through https://zeit.co/account/tokens

`NOW_ALIAS_PRODUCTION` is a comma separated strings describing the aliases now.sh should use for the production environment. This should be set in the CI/CD settings of your project.
