import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
// import EditEntry from '../../components/EditEntry';
import EntryDetails from '../../components/EntryDetails';
// import { getEntry } from '../../actions/entries';
import { Button, Grid, Header, Label } from 'semantic-ui-react';

class EntriesAdmin extends React.Component {
    componentDidMount() {
        this.fetchEntry();
    }

    componentDidUpdate() {
        this.fetchEntry();
    }

    fetchEntry = () => {
        const { entry, isFetchingEntry, entryId } = this.props;

        if (entryId && (!entry || entry.isNested) && !isFetchingEntry) {
            // getEntry(entryId);
        }
    };

    render() {
        const { isEditing, entry } = this.props;

        if (!Object.keys(entry).length) {
            return <h1>Loading</h1>;
        }

        return (
            <Grid>
                <Grid.Column>
                    <Header>Status column</Header>
                    <div>
                        <Button isFullWidth isColor={isEditing ? 'warning' : ''} onClick={this.toggleEditState}>{`Edit${
                            isEditing ? 'ing' : ''
                        } entry`}</Button>
                    </div>
                    <div>
                        Status: <Label>{entry.status.label}</Label>
                    </div>
                </Grid.Column>
                <Grid.Column>
                    {/* {isEditing && entry && <EditEntry entryId={entry.id} onSave={this.toggleEditState} />} */}
                    {!isEditing && entry && <EntryDetails entry={entry} />}
                </Grid.Column>
            </Grid>
        );
    }
}

const mapStateToProps = (state, { match }) => {
    const entry = get(state.entries.data, match.params.id, {});

    return {
        entry,
        isEditing: match.path.includes('edit'),
    };
};

const mapDispatchToProps = {
    // getEntry,
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(EntriesAdmin)
);
