import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactTable from 'react-table';

// import { getEntries } from '../../actions/entries';
import { Button, Icon } from 'semantic-ui-react';

class EntriesOverviewAdmin extends React.Component {
    componentDidMount() {
        if (!this.props.isFetching && !this.props.entries.length) {
            // this.props.getEntries();
        }
    }

    qualifyEntry = id => {
        // TODO Call action for qualifying entry
    };

    render() {
        return (
            <ReactTable
                columns={[
                    {
                        Header: 'Id',
                        accessor: 'id',
                        maxWidth: 50,
                    },
                    {
                        Header: 'Title',
                        accessor: 'title',
                    },
                    {
                        Header: 'Owner',
                        accessor: 'owner.display_name',
                    },
                    {
                        Header: 'Status',
                        accessor: 'status.label',
                    },
                    {
                        Header: 'Actions',
                        accessor: 'id',
                        Cell: props => (
                            <div className="buttons">
                                <Button size="small">
                                    <Link to={`${this.props.match.url}/${props.value}`} title="View entry">
                                        <Icon name="image" />
                                    </Link>
                                </Button>
                                {!isQualified(props.row._original.status.value) && (
                                    <Button
                                        size="small"
                                        color="green"
                                        title="Qualify"
                                        onClick={this.qualifyEntry(props.row._original.id)}
                                    >
                                        <Icon name="check" />
                                    </Button>
                                )}
                                {!isQualified(props.row._original.status.value) && (
                                    <Button
                                        size="small"
                                        color="red"
                                        title="Disqualify"
                                        onClick={this.qualifyEntry(props.row._original.id)}
                                    >
                                        <Icon name="times" />
                                    </Button>
                                )}
                            </div>
                        ),
                    },
                ]}
                data={this.props.entries}
            />
        );
    }
}

const isQualified = status => {
    if (status === (4 || 1)) {
        return true;
    }

    return false;
};

const mapStateToProps = (state, { match }) => {
    const competitionId = parseInt(match.params.id, 10);

    return {
        isFetching: !!state.entries.isFetching,
        entries: Object.values(state.entries.data).filter(e => !e.isNested && e.competition.id === competitionId),
    };
};

const mapDispatchToProps = {
    // getEntries,
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(EntriesOverviewAdmin)
);
