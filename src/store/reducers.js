import { combineReducers } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';

import ducks from '../ducks';

const reducers = history =>
    combineReducers({
        router: connectRouter(history),
        ...ducks,
    });

export default reducers;
