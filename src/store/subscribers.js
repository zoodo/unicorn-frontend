import throttle from 'lodash/throttle';
import * as authenticationDucks from '../ducks/authentication.duck';
import { saveState, createObjectCache } from './persistant';

export const refreshToken = store =>
    throttle(() => {
        const state = store.getState();
        const exp = authenticationDucks.tokenIsValid(state);

        if (exp === 0) {
            return;
        }

        const now = Math.floor(Date.now() / 1000);
        const refreshTime = exp - 3600 / 2;

        if (exp - now > 0 && now > refreshTime && !authenticationDucks.isFetching(state)) {
            store.dispatch(authenticationDucks.refreshToken(authenticationDucks.getRefreshToken(state)));
        }
    }, 10000);

export const saveReduxState = store =>
    throttle(() => {
        const state = {
            authentication: store.getState()['authentication'],
        };

        saveState(createObjectCache(state));
    }, 500);
