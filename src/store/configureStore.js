import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import get from 'lodash/get';
import * as Sentry from '@sentry/browser';
import createSentryMiddleware from 'redux-sentry-middleware';

import rootReducer from './reducers';
import { refreshToken, saveReduxState } from './subscribers';
import { createObjectCache, excludeWords, loadState } from './persistant';

const environment = get(process.env, 'REACT_APP_ENVIRONMENT');
const isTesting = get(process.env, 'JEST_WORKER_ID');

if (environment && !isTesting) {
    Sentry.init({
        dsn: 'https://d6acc50beb9d4de59400e6cf13e794c5@sentry.io/1252132',
        environment,
    });
}

export const history = createBrowserHistory();

const preloadedState = loadState();

export const store = configureStore({
    reducer: rootReducer(history),
    preloadedState: preloadedState || {},
    middleware: [
        ...getDefaultMiddleware(),
        routerMiddleware(history),
        createSentryMiddleware(Sentry, {
            getUserContext: state => {
                const user = {};
                const id = get(state, 'authentication.meta.uid');
                if (id) user.id = id;

                return user;
            },
            // TODO Set this up to send limited or relevant data stores
            stateTransformer: state => createObjectCache(state, 0, 4, [], [...excludeWords, 'authentication'], true),
        }),
    ],
});

/* if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('./reducers', () => {
        const newRootReducer = require('./reducers').default;
        store.replaceReducer(newRootReducer);
    });
}
 */
store.subscribe(refreshToken(store));
store.subscribe(saveReduxState(store));

window.addEventListener('click', refreshToken(store));
window.addEventListener('keydown', refreshToken(store));
window.addEventListener('focus', refreshToken(store));

export default store;
