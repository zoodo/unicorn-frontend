import isPlainObject from 'lodash/isPlainObject';

export const DEDUPE_KEY = '__redux-store__';

// keys that safely can be cached as-is
export const safeWords = ['authentication'];

// keys that should be omitted from caching
export const excludeWords = ['data', 'isFetching', 'error', 'router', 'errorLog'];

export const createObjectCache = (obj, depth = 0, maxDepth = 4, include = safeWords, exclude = excludeWords) => {
    // not any types we care about
    // Did you know that typeof null evals to 'object'?
    if (!obj || typeof obj !== 'object') {
        return false;
    }

    // You're way too deep dude
    if (depth > maxDepth) {
        return false;
    }

    // Whatever we should send back up
    const filteredObj = {};
    for (const [key, val] of Object.entries(obj)) {
        // Ka ching! 💰💰💰
        if (include.includes(key)) {
            filteredObj[key] = val;
            continue;
        }

        // Skip keys we know only contain irrelevant data
        if (!exclude.includes(key)) {
            const potential = createObjectCache(val, depth + 1, maxDepth, include, exclude);
            // TODO this breaks values that's something other than an object
            if (isPlainObject(potential)) {
                filteredObj[key] = potential;
            }
        }
    }

    // Return whatever we found
    return filteredObj;
};

export const loadState = () => {
    try {
        const strState = localStorage.getItem(DEDUPE_KEY + 'state');
        if (!strState) {
            return undefined;
        }

        const serializedState = JSON.parse(strState);

        if (serializedState.authentication.exp < Date.now() / 1000) {
            return undefined;
        }

        return {
            authentication: serializedState.authentication,
        };
    } catch (err) {
        return undefined;
    }
};

export const saveState = data => {
    try {
        const serializedState = JSON.stringify(data);
        localStorage.setItem(DEDUPE_KEY + 'state', serializedState);
    } catch (err) {
        // Ignore write errors.
    }
};
