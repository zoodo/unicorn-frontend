import React from 'react';
import { shallow } from 'enzyme';

import FeaturedCompetitions from './FeaturedCompetitions';
import { REGISTRATION_OPEN } from '../../../utils/competitions';

const competitions = [
    {
        name: 'Cosplaykonkurransen',
        brief_description: 'Den Store Cosplay Konkurransen!',
        id: 13,
        state: {
            value: REGISTRATION_OPEN,
        },
    },
    {
        name: 'Fortnite',
        brief_description: 'Format: 2v2',
        id: 14,
        state: {
            value: REGISTRATION_OPEN,
        },
    },
    {
        name: 'Counter Strike Global Offensive',
        brief_description: 'Format: 5v5',
        id: 15,
        state: {
            value: REGISTRATION_OPEN,
        },
    },
    {
        name: 'League of Legends',
        brief_description: 'Format: 5v5',
        id: 16,
        state: {
            value: REGISTRATION_OPEN,
        },
    },
    {
        name: 'FIFA 19',
        brief_description: 'Format: 1v1',
        id: 17,
        state: {
            value: REGISTRATION_OPEN,
        },
    },
];

describe('Featured Competitions component renders as expected', () => {
    it("returns null when there's no competitions", () => {
        const component = shallow(<FeaturedCompetitions />);
        expect(component.type()).toEqual(null);
    });

    it('renders with multiple items', () => {
        const component = shallow(<FeaturedCompetitions competitions={competitions} />);
        expect(component).toMatchSnapshot();
    });
});
