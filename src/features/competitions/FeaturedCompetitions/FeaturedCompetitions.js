import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Header, Segment, Button } from 'semantic-ui-react';
import isEqual from 'lodash/isEqual';
import { RUNNING_CLOSED, VOTING_OPEN } from '../../../utils/competitions';
import Carousel from '../../../components/Carousel';

const FeaturedCompetitions = ({ competitions }) => {
    // TODO Implement loading state
    if (!competitions.length) {
        return null;
    }

    return (
        <div className="featured-competitions">
            <Carousel backgroundColor="black" activeColor={null}>
                {competitions.map(competition => (
                    <Segment basic className="featured-competitions-slide" key={competition.id}>
                        <div className="featured-competitions-slide-before">
                            <Header inverted as="h1">
                                {competition.name}
                            </Header>
                            <Header inverted disabled>
                                {competition.brief_description}
                            </Header>
                            {competition.state.value < RUNNING_CLOSED && (
                                <Button primary as={Link} to={`/competitions/${competition.id}`}>
                                    JOIN
                                </Button>
                            )}
                            {competition.state.value === VOTING_OPEN && (
                                <Button primary as={Link} to={`/competitions/${competition.id}`}>
                                    VOTE
                                </Button>
                            )}
                        </div>
                        <div
                            className="featured-competitions-slide-after"
                            style={{ backgroundImage: `url("${competition.header_image}")` }}
                        />
                    </Segment>
                ))}
            </Carousel>
        </div>
    );
};

FeaturedCompetitions.defaultProps = {
    competitions: [],
};

FeaturedCompetitions.propTypes = {
    competitions: PropTypes.array,
};

export default memo(FeaturedCompetitions, (prevProps, nextProps) => isEqual(prevProps, nextProps));
