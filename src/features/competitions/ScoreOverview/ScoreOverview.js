import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import { Link } from 'react-router-dom';

import LoadingGate from '../../../components/LoadingGate';

const byScoreAsc = (a, b) => a.score - b.score;

const ScoreOverview = ({ entries, fetchEntries, isLoading, competitionID }) => {
    return (
        <LoadingGate onStartLoading={fetchEntries} isLoading={isLoading}>
            <ReactTable
                columns={[
                    {
                        Header: 'id',
                        Cell: props => (
                            <Link to={`/competitions/${competitionID}/entries/${props.original.id}`}>
                                {props.original.id}
                            </Link>
                        ),
                        maxWidth: 50,
                    },
                    {
                        Header: 'Title',
                        accessor: 'title',
                    },
                    {
                        Header: 'Owner',
                        accessor: 'owner.display_name',
                    },
                    {
                        Header: 'Place',
                        maxWidth: 50,
                        Cell: props => props.index + 1,
                    },
                    {
                        Header: 'Score',
                        accessor: 'score',
                        maxWidth: 100,
                    },
                ]}
                data={entries.sort(byScoreAsc).reverse()}
            />
        </LoadingGate>
    );
};

ScoreOverview.defaultProps = {
    entries: [],
    isLoading: false,
};

ScoreOverview.propTypes = {
    entries: PropTypes.array,
    fetchEntries: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
};

export default ScoreOverview;
