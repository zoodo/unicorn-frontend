import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';

import * as entryDucks from '../../../ducks/entries.duck';

import ScoreOverview from './ScoreOverview';

const mapStateToProps = (state, ownProps) => {
    const competitionID = get(ownProps, 'match.params.id');

    return {
        competitionID,
        entries: competitionID ? entryDucks.getEntriesByCompetition(state, Number(competitionID)) : [],
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const competitionID = get(ownProps, 'match.params.id');

    return {
        fetchEntries: () => dispatch(entryDucks.fetchEntriesForCompetition(competitionID)),
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(ScoreOverview)
);
