import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Segment, Header } from 'semantic-ui-react';
import { getOscarUrl } from '../../../utils/apiFetch';

const RegisterCard = ({ action, entry, location, competition }) => (
    <Segment.Group>
        <Segment color="orange">
            <Header>{headerText(action)}</Header>
        </Segment>
        <Content action={action} to={location.pathname} entry={entry} competition={competition} />
    </Segment.Group>
);

const headerText = action => {
    switch (action) {
        case EXTERNAL:
        case LOGIN:
        case REGISTER:
            return 'Want to participate?';

        case MY_REGISTRATION:
            return 'You are registered';

        case RESULT:
            return null;

        default:
            return null;
    }
};

const Content = ({ action, to, entry, competition }) => {
    switch (action) {
        case LOGIN:
            return (
                <Segment basic>
                    <Header as="a" href={getOscarUrl({ to })} color="orange">
                        Log in to register!
                    </Header>
                </Segment>
            );

        case REGISTER:
            return (
                <Link to={to + '/register'}>
                    <Segment basic>
                        <Header color="orange">Click here to register!</Header>
                    </Segment>
                </Link>
            );

        case MY_REGISTRATION:
            if (entry.is_owner) {
                return (
                    <Link to={to + '/register/' + entry.id}>
                        <Segment basic>
                            <Header color="orange">Edit your registration</Header>
                        </Segment>
                    </Link>
                );
            }
            return (
                <Link to={to + '/register/' + entry.id}>
                    <Segment basic>
                        <Header color="orange">Check out your registration</Header>
                    </Segment>
                </Link>
            );

        case RESULT:
            return null;

        case EXTERNAL:
            return (
                <Segment basic>
                    <Header as="a" href={competition.external_url_info} color="orange">
                        This is an externally hosted competition, click here for more information!
                    </Header>
                    <br />
                    <Header size="small" as="a" href={competition.external_url_login} color="grey">
                        Direct to login
                    </Header>
                </Segment>
            );

        default:
            return null;
    }
};

export const LOGIN = 1;
export const REGISTER = 2;
export const MY_REGISTRATION = 4;
export const RESULT = 8;
export const EXTERNAL = 16;

RegisterCard.defaultProps = {
    entry: {},
};

RegisterCard.propTypes = {
    action: PropTypes.oneOf([REGISTER, MY_REGISTRATION, RESULT]).isRequired,
    location: PropTypes.object.isRequired,
    entry: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

export default withRouter(memo(RegisterCard));
