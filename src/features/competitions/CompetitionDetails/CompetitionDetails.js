import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Grid, Header, Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import draftToHtml from 'draftjs-to-html';
import dayjs from 'dayjs';
import { hasPreRegistration, hasVote } from '../../../utils/competitions';
import LoadingGate from '../../../components/LoadingGate';
import { formatNumber } from '../../../utils/numbers';
import { RegisterCard } from '.';
import CompetitionHeader from '../CompetitionHeader';

class CompetitionDetails extends React.PureComponent {
    to = url => `${this.props.match.url}/${url}`;
    toRoute = url => `${this.props.match.path}/${url}`;

    convertDraftToHtml = data => {
        let html = data;

        try {
            html = draftToHtml(JSON.parse(data));
        } catch (_) {
            return html;
        }

        return html;
    };

    ts = time => dayjs(time).format('ddd MMM DD HH:mm');

    get competitionDescription() {
        return this.convertDraftToHtml(this.props.competition.description);
    }

    get competitionRules() {
        return this.convertDraftToHtml(this.props.competition.rules);
    }

    render() {
        const { fetchCompetition, competition, isLoading, action, entry, isAdmin } = this.props;

        return (
            <LoadingGate onStartLoading={fetchCompetition} isLoading={isLoading} spinner={!competition}>
                {competition && (
                    <CompetitionHeader
                        competition={competition}
                        renderRight={() => (
                            <Grid>
                                {isAdmin && (
                                    <Grid.Column verticalAlign="bottom" style={{ paddingBottom: 'unset' }}>
                                        <Button
                                            as={Link}
                                            to={`/competitions/${competition.id}/admin`}
                                            floated="right"
                                            icon="wrench"
                                            content="Admin"
                                            color="yellow"
                                        />
                                    </Grid.Column>
                                )}
                            </Grid>
                        )}
                    />
                )}
                {/*
                TODO Reimplement with useful menus
                <Menu pointing secondary>
                    {tabs.map(tab => (
                        <Menu.Item as={NavLink} key={tab.url} to={this.to(tab.url)}>
                            {tab.label}
                        </Menu.Item>
                    ))}
                </Menu> */}
                {competition && (
                    <Segment basic>
                        <Grid columns="2" stackable>
                            <Grid.Column width="10">
                                <Segment.Group>
                                    <Segment color="blue" inverted>
                                        <Header>General information</Header>
                                    </Segment>
                                    <Segment dangerouslySetInnerHTML={{ __html: this.competitionDescription }} />
                                </Segment.Group>
                                <Segment.Group>
                                    <Segment color="red" inverted>
                                        <Header>Rules</Header>
                                    </Segment>
                                    <Segment>
                                        <p>
                                            <Link to="/competitions/general_rules">
                                                Remember that The Gathering general rules applies in all competitions.
                                            </Link>
                                        </p>
                                        <div dangerouslySetInnerHTML={{ __html: this.competitionRules }} />
                                    </Segment>
                                </Segment.Group>
                            </Grid.Column>
                            <Grid.Column width="6">
                                {action && <RegisterCard action={action} entry={entry} competition={competition} />}
                                {competition.state.value === 32 && (
                                    <Segment basic>
                                        <Header color="orange" as={Link} to={'/vote/' + competition.id}>
                                            <Icon name="heart" />
                                            {competition.state.label}
                                        </Header>
                                    </Segment>
                                )}
                                <Segment.Group>
                                    <Segment color="teal">
                                        <Header>Schedule</Header>
                                    </Segment>
                                    {hasPreRegistration(competition) && (
                                        <Segment>
                                            Registration:{' '}
                                            <strong>
                                                {this.ts(competition.register_time_start)} -{' '}
                                                {this.ts(competition.register_time_end)}
                                            </strong>
                                        </Segment>
                                    )}
                                    <Segment>
                                        Competition:{' '}
                                        <strong>
                                            {this.ts(competition.run_time_start)} - {this.ts(competition.run_time_end)}
                                        </strong>
                                    </Segment>
                                    {hasVote(competition) && (
                                        <Segment>
                                            Voting:{' '}
                                            <strong>
                                                {this.ts(competition.vote_time_start)} -{' '}
                                                {this.ts(competition.vote_time_end)}
                                            </strong>
                                        </Segment>
                                    )}
                                </Segment.Group>
                                {!!competition.prizes.length && (
                                    <Segment.Group>
                                        <Segment color="green">
                                            <Header>Prizes</Header>
                                        </Segment>
                                        {competition.prizes.map((prize, i) => (
                                            <Segment key={prize}>
                                                {formatNumber(i + 1)} prize – {prize}
                                            </Segment>
                                        ))}
                                    </Segment.Group>
                                )}
                            </Grid.Column>
                        </Grid>
                    </Segment>
                )}
            </LoadingGate>
        );
        /**
         * TODO Reimplement
         * {competition && (
                    <Switch>
                        <Route
                            path={this.toRoute('register')}
                            render={() => <RegisterForm competition={competition} />}
                        />
                        <Route
                            path={this.toRoute('registration')}
                            render={() => <EntryDetailsPage entryId={ownEntry.id} />}
                        />
                        <Route
                            path={this.toRoute('rules')}
                            component={() => <div dangerouslySetInnerHTML={{ __html: this.competitionRules }} />}
                        />
                        <Route render={() => <TabGeneral competition={competition} />} />
                    </Switch>
                )}
         */
    }

    static defaultProps = {
        isLoading: true,
    };

    static propTypes = {
        fetchCompetition: PropTypes.func.isRequired,
        competition: PropTypes.object,
        isLoading: PropTypes.bool,
    };
}

export default CompetitionDetails;
