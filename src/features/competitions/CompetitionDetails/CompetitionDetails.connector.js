import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import { isExternal, hasPreRegistration } from '../../../utils/competitions';

import * as competitionDuck from '../../../ducks/competitions.duck';
import { getUserId, userIsLoggedin } from '../../../ducks/authentication.duck';
import { userIsContributorInCompetition, isFetching, fetchEntriesForCompetition } from '../../../ducks/entries.duck';
import { userIsCrew } from '../../../ducks/users.duck';

import CompetitionDetails from './CompetitionDetails';
import { LOGIN, REGISTER, MY_REGISTRATION, RESULT, EXTERNAL } from './RegisterCard';

const mapStateToProps = (state, ownProps) => {
    const { id } = ownProps.match.params;
    const competition = competitionDuck.getCompetition(state, id);
    const authenticated = userIsLoggedin(state);
    const entry = userIsContributorInCompetition(state, get(competition, 'id'));
    const action = findRegisterAction(competition, entry, authenticated);

    return {
        isLoading: competitionDuck.isFetching(state) || isFetching(state),
        authenticated,
        competition,
        action,
        entry,
        isAdmin: userIsCrew(state, getUserId(state)),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const { id } = ownProps.match.params;

    // TODO This needs to be fixed asap
    return {
        fetchCompetition: () =>
            dispatch(
                competitionDuck.fetchCompetition(id, competition => {
                    dispatch(fetchEntriesForCompetition(competition.id));
                })
            ),
    };
};

const findRegisterAction = (competition, ownEntry, authenticated) => {
    if (!competition) {
        return null;
    }

    // Add functionality based on state
    switch (competition.state.value) {
        case 2: // Register
            if (isExternal(competition)) {
                return EXTERNAL;
            }

            if (!authenticated) {
                return LOGIN;
            }

            if (ownEntry) {
                return MY_REGISTRATION;
            } else {
                return REGISTER;
            }

        case 4: // Show registration
            if (isExternal(competition)) {
                return EXTERNAL;
            }

            if (ownEntry && authenticated) {
                return MY_REGISTRATION;
            }
            break;

        case 8: // Register or hand in
            if (isExternal(competition)) {
                return EXTERNAL;
            }

            if (authenticated) {
                if (ownEntry && !!Object.keys(ownEntry).length) {
                    return MY_REGISTRATION;
                } else {
                    if (hasPreRegistration(competition)) {
                        return null;
                    }

                    return REGISTER;
                }
            } else {
                return LOGIN;
            }

        case 16: // Show information
            if (isExternal(competition)) {
                return EXTERNAL;
            }

            if (ownEntry && authenticated) {
                return MY_REGISTRATION;
            }
            break;

        case 256: // Show result details
            if (isExternal(competition)) {
                return null;
            }

            return RESULT;

        default:
            return null;
    }

    return null;
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompetitionDetails));
