import CompetitionDetails from './CompetitionDetails';
import CompetitionDetailsConnector from './CompetitionDetails.connector';
import RegisterCard from './RegisterCard';

export { CompetitionDetails };
export { RegisterCard };

export default CompetitionDetailsConnector;
