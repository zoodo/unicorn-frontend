import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import CompetitionDashboard from './CompetitionDashboard';
import * as competitionDuck from '../../../ducks/competitions.duck';

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: competitionDuck.isFetching(state),
        competition: competitionDuck.getCompetition(state, ownProps.match.params.id),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const competitionId = ownProps.match.params.id;
    return {
        fetchCompetition: () => dispatch(competitionDuck.fetchCompetition(competitionId)),
        updateRules: rules =>
            dispatch(
                competitionDuck.updateCompetition(
                    competitionId,
                    { rules },
                    id => {
                        toast.success('Updated rules!');
                    },
                    error => {
                        console.log(error);
                    }
                )
            ),
        publish: published =>
            dispatch(
                competitionDuck.updateCompetition(
                    competitionId,
                    {
                        published,
                    },
                    id =>
                        toast.success(
                            published ? 'Competition has been published' : 'Competition has been unpublished'
                        ),
                    error => toast.error('Updating published status failed')
                )
            ),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompetitionDashboard));
