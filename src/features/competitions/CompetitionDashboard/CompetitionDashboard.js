import React, { useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Header, Table, Button, Input, Form, Divider, Segment, Label } from 'semantic-ui-react';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import LoadingGate from '../../../components/LoadingGate';
import {
    convertRawToContentState,
    CLOSED,
    REGISTRATION_CLOSED,
    RUNNING_CLOSED,
    VOTING_CLOSED,
    REGISTRATION_OPEN,
    RUNNING_OPEN,
    SHOWTIME,
    FINISHED,
    VOTING_OPEN,
} from '../../../utils/competitions';
import CompetitionHeader from '../CompetitionHeader';

const stateColor = stateValue => {
    switch (stateValue) {
        case CLOSED:
        case REGISTRATION_CLOSED:
        case RUNNING_CLOSED:
        case VOTING_CLOSED:
            return 'red';
        case REGISTRATION_OPEN:
        case RUNNING_OPEN:
        case VOTING_OPEN:
            return 'green';
        case SHOWTIME:
            return 'pink';
        case FINISHED:
            return 'blue';
        default:
            return null;
    }
};

const CompetitionDashboard = ({ competition, fetchCompetition, isLoading, match, updateRules, publish }) => {
    const [entryFilter, setEntryFilter] = useState('');
    const [rules, setRules] = useState(EditorState.createEmpty());
    const [rulesChanged, setRulesChanged] = useState(false);

    useEffect(() => {
        fetchCompetition();
    }, [fetchCompetition]);

    useEffect(() => {
        competition && setRules(EditorState.createWithContent(convertRawToContentState(competition.rules)));
    }, [competition]);

    const resetFilter = () => setEntryFilter('');

    const filteredEntries = useMemo(
        () =>
            competition && competition.entries
                ? competition.entries.filter(entry => entry.title.toLowerCase().includes(entryFilter.toLowerCase()))
                : [],
        [entryFilter, competition]
    );

    const onRulesChange = editorState => {
        setRulesChanged(true);
        setRules(editorState);
    };

    const onSubmitRules = () => {
        setRulesChanged(false);
        updateRules(JSON.stringify(convertToRaw(rules.getCurrentContent())));
    };

    return (
        <LoadingGate
            onStartLoading={fetchCompetition}
            isLoading={isLoading}
            style={{ marginLeft: '-1rem', marginRight: '-1rem', paddingTop: 'unset' }}
            basic
        >
            {competition && (
                <>
                    <CompetitionHeader
                        competition={competition}
                        renderLeft={() => (
                            <Grid>
                                <Grid.Column verticalAlign="bottom" style={{ paddingBottom: 'unset' }}>
                                    <Label.Group>
                                        <Label color={stateColor(competition.state.value)}>
                                            {competition.state.label}
                                        </Label>
                                        <Label color={competition.published ? 'green' : 'red'}>
                                            {competition.published ? 'Published' : 'Not published'}
                                        </Label>
                                        {competition.featured && <Label color={'teal'}>Featured</Label>}
                                    </Label.Group>
                                </Grid.Column>
                            </Grid>
                        )}
                        renderRight={() => (
                            <Grid>
                                <Grid.Column verticalAlign="bottom" style={{ paddingBottom: 'unset' }}>
                                    <Button
                                        as={Link}
                                        to={`/competitions/${competition.id}/edit`}
                                        floated="right"
                                        icon="pencil"
                                        content="Edit"
                                        color="yellow"
                                    />

                                    <Button
                                        floated="right"
                                        icon="paper plane"
                                        content={competition.published ? 'Unpublish' : 'Publish'}
                                        onClick={() => publish(!competition.published)}
                                    />
                                </Grid.Column>
                            </Grid>
                        )}
                    />
                    <Segment basic>
                        <Grid divided stackable>
                            <Grid.Row columns="2">
                                <Grid.Column>
                                    <Grid columns="2">
                                        <Grid.Column verticalAlign="middle">
                                            <Header>
                                                Participants ({competition.entries_count}
                                                {!!competition.participant_limit && `/${competition.participant_limit}`}
                                                )
                                            </Header>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Input
                                                fluid
                                                placeholder="Search..."
                                                icon={{ name: 'close', link: true, onClick: resetFilter }}
                                                onChange={(e, { value }) => setEntryFilter(value)}
                                                value={entryFilter}
                                            />
                                        </Grid.Column>
                                    </Grid>
                                    <Header as="h5">
                                        <Link to={`/competitions/${match.params.id}/entries`}>View all</Link>
                                        <Link to={`/competitions/${match.params.id}/scores`} style={{ float: 'right' }}>
                                            Scores
                                        </Link>
                                    </Header>
                                    <Table basic="very">
                                        <Table.Body>
                                            {filteredEntries.map(entry => (
                                                <Table.Row key={entry.id} positive={entry.is_contributor}>
                                                    <Table.Cell>
                                                        <Link
                                                            to={`/competitions/${match.params.id}/entries/${entry.id}`}
                                                        >
                                                            {entry.title}
                                                        </Link>
                                                    </Table.Cell>
                                                </Table.Row>
                                            ))}
                                        </Table.Body>
                                    </Table>
                                </Grid.Column>
                                <Grid.Column>
                                    <Grid columns="2">
                                        <Grid.Column verticalAlign="middle">
                                            <Header>Edit rules</Header>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Button
                                                floated="right"
                                                positive
                                                disabled={!rulesChanged}
                                                icon="save"
                                                content="Save"
                                                onClick={onSubmitRules}
                                            />
                                        </Grid.Column>
                                    </Grid>
                                    <Divider hidden />
                                    <Form>
                                        <Form.Field name="rules">
                                            <Form.Field>
                                                <Editor
                                                    toolbarClassName="toolbarClassName"
                                                    wrapperClassName="wrapperClassName"
                                                    editorClassName="wysiwig-editor"
                                                    onEditorStateChange={onRulesChange}
                                                    editorState={rules}
                                                    toolbar={{
                                                        options: [
                                                            'inline',
                                                            'blockType',
                                                            'list',
                                                            'textAlign',
                                                            'colorPicker',
                                                            'link',
                                                            'emoji',
                                                            'remove',
                                                            'history',
                                                        ],
                                                        list: { inDropdown: true },
                                                        textAlign: { inDropdown: true },
                                                        image: {
                                                            uploadEnabled: false,
                                                        },
                                                    }}
                                                />
                                            </Form.Field>
                                        </Form.Field>
                                    </Form>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Segment>
                </>
            )}
        </LoadingGate>
    );
};

CompetitionDashboard.propTypes = {
    competition: PropTypes.object,
    fetchCompetition: PropTypes.func,
    updateRules: PropTypes.func,
    isLoading: PropTypes.bool,
    match: PropTypes.object,
};

export default CompetitionDashboard;
