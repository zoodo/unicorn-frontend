import React from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Header } from 'semantic-ui-react';
import { CATEGORY_ICON_MAP } from '../../../../ducks/genres.duck';

const CategoryCard = ({ category, onClick, raised }) => (
    <Card
        onClick={onClick}
        raised={raised}
        style={{
            minHeight: '175px',
        }}
        className={`category-select-item-${category.value}`}
    >
        <Card.Content
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
            }}
        >
            <Card.Header textAlign="center">
                <Icon size="big" inverted name={CATEGORY_ICON_MAP[category.value]} />
            </Card.Header>
            <Header as="h1" content={category.label} textAlign="center" />
        </Card.Content>
    </Card>
);

CategoryCard.defaultProps = {
    raised: false,
    onClick: () => null,
};

CategoryCard.propTypes = {
    category: PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.number,
    }).isRequired,
    onClick: PropTypes.func,
    raised: PropTypes.bool,
};

export default CategoryCard;
