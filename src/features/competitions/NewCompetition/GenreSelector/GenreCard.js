import React from 'react';
import PropTypes from 'prop-types';
import { Card, Header } from 'semantic-ui-react';

const GenreCard = ({ genre, onClick, raised }) => (
    <Card
        raised={raised}
        style={{
            minHeight: '175px',
        }}
        className={`genre-select-item genre-select-item-${genre.category.value}`}
        onClick={onClick}
    >
        <Card.Content
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
            }}
        >
            <Header as="h1" content={genre.name} textAlign="center" />
        </Card.Content>
    </Card>
);

GenreCard.defaultProps = {
    onClick: () => null,
    raised: false,
};

GenreCard.propTypes = {
    genre: PropTypes.object.isRequired,
    onClick: PropTypes.func,
    raised: PropTypes.bool,
};

export default GenreCard;
