import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import sortBy from 'lodash/sortBy';
import get from 'lodash/get';
import { useSelector, useDispatch } from 'react-redux';
import { Card, Grid, Button, Header } from 'semantic-ui-react';
import { getCategories, getGenresForCategory, isFetching, fetchGenres } from '../../../../ducks/genres.duck';
import GenreCard from './GenreCard';
import CategoryCard from './CategoryCard';

let didFetch = false;

const GenreSelector = ({ onChange, initialValues }) => {
    const [category, setCategory] = useState(null);
    const [genre, setGenre] = useState(null);

    useEffect(() => {
        if (initialValues) {
            const category = get(initialValues, 'category');
            setCategory(category);
            setGenre(initialValues);
        }
    }, [initialValues]);

    const dispatch = useDispatch();
    const categories = useSelector(state => getCategories(state), isEqual);
    const isFetchingGenres = useSelector(state => isFetching(state), isEqual);
    const genres = useSelector(state => getGenresForCategory(state, category && category.value), isEqual);

    useEffect(() => {
        if (!isFetchingGenres && !categories.length && !didFetch) {
            dispatch(fetchGenres());
            didFetch = true;
        }
    });

    useEffect(() => {
        if (genre) {
            onChange(genre);
        } else if (!category && !genre) {
            onChange(null);
        }
    }, [category, genre, onChange]);

    useEffect(() => {
        if (category && !genre && genres.length === 1) {
            setGenre(genres[0]);
        }
    }, [category, genre, genres]);

    const clear = () => {
        setGenre(null);
        setCategory(null);
    };

    const sortedCategories = useMemo(() => sortBy(categories, 'label'), [categories]);
    const sortedGenres = useMemo(() => sortBy(genres, 'label'), [genres]);

    return (
        <Grid className="genre-select">
            <Grid.Row>
                <Grid.Column>
                    <Header>Select genre</Header>
                    <Button
                        icon="close"
                        circular
                        size="mini"
                        compact
                        floated="right"
                        disabled={!category}
                        onClick={clear}
                    />
                </Grid.Column>
            </Grid.Row>

            {category && (
                <Grid.Row>
                    <Grid.Column>
                        <Card.Group stackable>
                            <CategoryCard category={category} />
                        </Card.Group>
                    </Grid.Column>
                </Grid.Row>
            )}

            <Grid.Row>
                <Grid.Column>
                    <Card.Group stackable centered>
                        {!category &&
                            sortedCategories.map(c => (
                                <CategoryCard
                                    key={c.value}
                                    category={c}
                                    onClick={() => setCategory(c)}
                                    raised={c.value === category}
                                />
                            ))}
                        {category &&
                            sortedGenres.length > 1 &&
                            sortedGenres.map(g => (
                                <GenreCard
                                    key={g.id}
                                    raised={g.id === (genre && genre.id)}
                                    onClick={() => !isEqual(g, genre) && setGenre(g)}
                                    genre={g}
                                />
                            ))}
                    </Card.Group>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
};

GenreSelector.defaultProps = {
    onChange: () => null,
};

GenreSelector.propTypes = {
    onChange: PropTypes.func,
};

export default GenreSelector;
