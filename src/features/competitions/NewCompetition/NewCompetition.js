import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Field } from 'react-final-form';
import { Form, Header, Message, Popup, Icon, Modal, Button, List } from 'semantic-ui-react';
import get from 'lodash/get';
import throttle from 'lodash/throttle';
import uuid from 'uuid';
import { push } from 'connected-react-router';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { toast } from 'react-toastify';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import GenreSelector from './GenreSelector';
import PrizeEdit from '../EditCompetition/PrizeEdit';
import FileEdit from '../EditCompetition/FileEdit';
import { Wizard, Slide } from '../../../components/FormWizard';
import DateTimePicker from '../../../components/DateTimePicker';
import { createCompetition } from '../../../ducks/competitions.duck';
import { CATEGORY_OTHER, CATEGORY_CREATIVE } from '../../../ducks/genres.duck';
import InputLength from '../../../components/InputLength';
import { convertRawToContentState } from '../../../utils/competitions';
import { parseError } from '../../../utils/error';

const FORM_KEY = '__NEWFORM_FORM__';
const META_KEY = '__NEWFORM_META__';

const cacheForm = throttle((key, values) => localStorage.setItem(FORM_KEY + key, JSON.stringify(values)), 1000);
const removeCachedForm = (formKey, formVal) => {
    localStorage.removeItem(formKey);
    localStorage.removeItem(META_KEY + formVal.meta._id);
};

const _id = uuid();

dayjs.extend(relativeTime);

const NewCompetition = () => {
    const [genre, setGenre] = useState(null);
    const [loadCached, setLoadCached] = useState([]);
    const [initialValues, setInitialValues] = useState({});
    const dispatch = useDispatch();

    useEffect(() => {
        let formIds = '';
        // load all accessible forms
        const forms = Object.entries(localStorage)
            .filter(([k, v]) => k.includes(FORM_KEY))
            .map(([k, v]) => {
                formIds += k;
                const form = JSON.parse(v);
                const meta = JSON.parse(localStorage.getItem(form.meta));
                return [k, { ...form, meta }];
            });

        if (forms.length) {
            setLoadCached(forms);
        }

        // remove meta keys with no paired form
        Object.entries(localStorage)
            .filter(([k, v]) => k.includes(META_KEY))
            .filter(([k, v]) => !formIds.includes(JSON.parse(v)._id))
            .forEach(([k, v]) => localStorage.removeItem(k));
    }, []);

    useEffect(() => {
        localStorage.setItem(META_KEY + _id, JSON.stringify({ genre, _id, _created: Date.now() }));
    }, [genre]);

    const onSubmit = values => {
        const r = {
            ...values,
            description: values.description
                ? JSON.stringify(convertToRaw(values.description.getCurrentContent()))
                : null,
            rules: values.rules ? JSON.stringify(convertToRaw(values.rules.getCurrentContent())) : null,
            genre: genre.id,
        };

        dispatch(
            createCompetition(
                r,
                id => {
                    toast.success('Created competition ' + r.name);
                    dispatch(push('/competitions/' + id + '/admin'));
                },
                error => {
                    toast.error('Error creating competition');
                    parseError(error).forEach(e => toast.error(e));
                }
            )
        );
    };

    return (
        <>
            <Modal open={!!loadCached.length}>
                <Modal.Header>
                    Unfinished competition{loadCached.length > 1 && 's'} found. Do you want to load it?
                </Modal.Header>
                <Modal.Content>
                    <List divided relaxed>
                        {loadCached.map(([key, val]) => (
                            <List.Item key={key}>
                                <Button
                                    circular
                                    icon="trash alternate"
                                    floated="right"
                                    size="small"
                                    title="Delete item from cache"
                                    onClick={() => {
                                        removeCachedForm(key, val);
                                        setLoadCached(loadCached.filter(([k, v]) => k !== key));
                                    }}
                                />
                                <List.Content>
                                    <List.Header
                                        as="a"
                                        onClick={() => {
                                            const values = { ...val.form };

                                            ['description', 'rules'].forEach(k => {
                                                if (values[k]) {
                                                    values[k] = EditorState.createWithContent(
                                                        convertRawToContentState(values[k])
                                                    );
                                                }
                                            });

                                            removeCachedForm(key, val);
                                            setGenre(val.meta.genre);
                                            setInitialValues(values);
                                            setLoadCached([]);
                                        }}
                                    >
                                        {val.form.name}
                                    </List.Header>
                                    <List.Description as="a">
                                        Created {dayjs(val.meta._created).fromNow()}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                        ))}
                    </List>
                </Modal.Content>
                <Modal.Actions>
                    {loadCached.length > 1 && (
                        <Button
                            color="yellow"
                            floated="left"
                            content="Remove all"
                            onClick={() => {
                                loadCached.forEach(([k, v]) => removeCachedForm(k, v));
                                setLoadCached([]);
                            }}
                        />
                    )}
                    <Button negative icon={'close'} onClick={() => setLoadCached([])} />
                </Modal.Actions>
            </Modal>
            <Wizard
                onSubmit={onSubmit}
                changeSubscription={{ values: true }}
                onChange={props => {
                    const form = { ...props.values };
                    ['description', 'rules'].forEach(k => {
                        if (form[k]) {
                            try {
                                form[k] = JSON.stringify(convertToRaw(form[k].getCurrentContent()));
                            } catch (e) {
                                form[k] = JSON.stringify(convertToRaw(form[k]));
                            }
                        }
                    });

                    if (Object.keys(props.values).length && props.values.name) {
                        cacheForm(_id, { form, meta: META_KEY + _id });
                    }
                }}
                initialValues={initialValues}
            >
                <Slide disabled={!genre}>
                    <GenreSelector onChange={setGenre} initialValues={genre} />
                    <Message warning visible>
                        <p>
                            <strong>NOTE:</strong> To make competition creation easier, some features may be hidden
                            based on what genre you choose. Though, all features will be available once the competition
                            is created.
                        </p>
                    </Message>
                </Slide>

                <Slide
                    validate={values => {
                        const errors = {};
                        const keys = [
                            'name',
                            'brief_description',
                            'header_image',
                            'header_credit',
                            'run_time_start',
                            'run_time_end',
                        ];

                        keys.forEach(v => {
                            if (!values[v]) errors[v] = 'Required';
                        });

                        if (!values.description || !values.description.getCurrentContent().hasText()) {
                            errors.description = 'Required';
                        }

                        return errors;
                    }}
                >
                    <Field
                        name="name"
                        component={({ input, meta }) => (
                            <Form.Input
                                {...input}
                                label="Competition title"
                                error={meta.touched && meta.error ? meta.error : null}
                                autoFocus
                            />
                        )}
                    />
                    <Form.Group widths="equal">
                        <Form.Field>
                            <Field
                                name="run_time_start"
                                component={({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition start time"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Field
                                name="run_time_end"
                                component={({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition end time"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                    </Form.Group>
                    <Field
                        name="brief_description"
                        component={({ input, meta }) => (
                            <InputLength length={input.value.length} max={40}>
                                <Form.Input
                                    {...input}
                                    label={
                                        <label>
                                            Brief description{' '}
                                            <Popup
                                                trigger={<Icon name="question circle outline" />}
                                                content="A very short description of the competition"
                                            />
                                        </label>
                                    }
                                    error={meta.touched && meta.error ? meta.error : null}
                                    maxLength="40"
                                />
                            </InputLength>
                        )}
                    />
                    <Field
                        name="description"
                        component={({ input, meta }) => (
                            <Form.Field>
                                <label>
                                    Description
                                    <Popup
                                        trigger={<Icon name="question circle outline" />}
                                        content="A general description of the competition"
                                    />
                                </label>
                                <Editor
                                    toolbarClassName="toolbarClassName"
                                    wrapperClassName="wrapperClassName"
                                    editorClassName="wysiwig-editor"
                                    onEditorStateChange={e => input.onChange(e)}
                                    editorState={input.value || EditorState.createEmpty()}
                                    toolbar={{
                                        options: [
                                            'inline',
                                            'blockType',
                                            'list',
                                            'textAlign',
                                            'colorPicker',
                                            'link',
                                            'emoji',
                                            'remove',
                                            'history',
                                        ],
                                        list: { inDropdown: true },
                                        textAlign: { inDropdown: true },
                                        image: {
                                            uploadEnabled: false,
                                        },
                                    }}
                                />
                                {meta.touched && meta.error && <Message negative content={meta.error} />}
                            </Form.Field>
                        )}
                    />
                    <Field
                        name="header_image"
                        component={({ input, meta }) => (
                            <Form.Input
                                {...input}
                                type="url"
                                label="Header image url"
                                error={meta.touched && meta.error ? meta.error : null}
                            />
                        )}
                    />
                    <Field
                        name="header_credit"
                        component={({ input, meta }) => (
                            <Form.Input
                                {...input}
                                label="Header image credit"
                                error={meta.touched && meta.error ? meta.error : null}
                            />
                        )}
                    />
                </Slide>

                <Slide
                    validate={values =>
                        values.rules && values.rules.getCurrentContent().hasText() ? {} : { rules: 'Required' }
                    }
                >
                    <Header>Rules</Header>
                    <Field
                        name="rules"
                        component={({ input, meta }) => (
                            <>
                                <Editor
                                    toolbarClassName="toolbarClassName"
                                    wrapperClassName="wrapperClassName"
                                    editorClassName="wysiwig-editor"
                                    onEditorStateChange={e => input.onChange(e)}
                                    editorState={input.value || EditorState.createEmpty()}
                                    toolbar={{
                                        options: [
                                            'inline',
                                            'blockType',
                                            'list',
                                            'textAlign',
                                            'colorPicker',
                                            'link',
                                            'emoji',
                                            'remove',
                                            'history',
                                        ],
                                        list: { inDropdown: true },
                                        textAlign: { inDropdown: true },
                                        image: {
                                            uploadEnabled: false,
                                        },
                                    }}
                                />
                                {meta.touched && meta.error && <Message negative content={meta.error} />}
                            </>
                        )}
                    />
                </Slide>

                <Slide>
                    <Header>Prizes (optional)</Header>
                    <Field name="prizes" format={v => v} component={({ input }) => <PrizeEdit {...input} />} />
                </Slide>

                {[CATEGORY_OTHER, CATEGORY_CREATIVE].includes(get(genre, 'category.value')) ? (
                    <Slide
                        validate={values => {
                            const errors = {};

                            if (values.vote_time_start && !values.vote_time_end) {
                                errors.vote_time_end = 'Required when start time is set';
                            }

                            if (values.fileupload) {
                                values.fileupload.forEach(v =>
                                    Object.values(v).forEach(fv => {
                                        if (!fv) errors.fileupload = 'One or more file fields are empty';
                                    })
                                );
                            }

                            return errors;
                        }}
                    >
                        <Form.Group widths="equal">
                            <Form.Field>
                                <Field
                                    name="vote_time_start"
                                    component={({ input, meta }) => (
                                        <DateTimePicker
                                            label="Voting start time  (optional)"
                                            onChange={event => input.onChange(new Date(event).toISOString())}
                                            value={input.value}
                                            error={meta.touched && meta.error ? meta.error : null}
                                        />
                                    )}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Field
                                    name="vote_time_end"
                                    component={({ input, meta }) => (
                                        <DateTimePicker
                                            label="Voting end time (optional)"
                                            onChange={event => input.onChange(new Date(event).toISOString())}
                                            value={input.value}
                                            error={meta.touched && meta.error ? meta.error : null}
                                        />
                                    )}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Form.Group>
                            <Form.Field>
                                <Field
                                    name="team_min"
                                    component={({ input }) => (
                                        <Form.Input type="number" label="Minimum team size (optional)" {...input} />
                                    )}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Field
                                    name="team_max"
                                    component={({ input }) => (
                                        <Form.Input type="number" label="Maximum team size" {...input} />
                                    )}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Field
                            name="contributor_extra"
                            component={({ input }) => (
                                <Form.Input
                                    label={
                                        <label>
                                            Custom input field name (optional)
                                            <Popup
                                                trigger={<Icon name="question circle outline" />}
                                                content="Information like SteamID, OriginID and so on"
                                            />
                                        </label>
                                    }
                                    {...input}
                                />
                            )}
                        />
                        <Field
                            name="fileupload"
                            component={({ input, meta }) => (
                                <>
                                    <Header>File upload (optional)</Header>
                                    <FileEdit onChange={input.onChange} value={input.value || []} />
                                    {meta.touched && meta.error && <Message negative content={meta.error} />}
                                </>
                            )}
                        />
                    </Slide>
                ) : (
                    <Slide
                        validate={values => {
                            const errors = {};

                            if (values.team_min && !values.team_max) {
                                errors.team_max = 'Required when minimum team size is set';
                            }

                            if (values.team_min && values.team_max && values.team_min > values.team_max) {
                                errors.team_max = 'Maximum team size must be larger than minimum';
                            }

                            return errors;
                        }}
                    >
                        <Form.Group>
                            <Form.Field>
                                <Field
                                    name="team_min"
                                    component={({ input, meta }) => (
                                        <Form.Input
                                            type="number"
                                            label="Minimum team size (optional)"
                                            {...input}
                                            error={meta.touched && meta.error ? meta.error : null}
                                        />
                                    )}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Field
                                    name="team_max"
                                    component={({ input, meta }) => (
                                        <Form.Input
                                            type="number"
                                            label="Maximum team size (optional)"
                                            {...input}
                                            error={meta.touched && meta.error ? meta.error : null}
                                        />
                                    )}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Field
                            name="contributor_extra"
                            component={({ input }) => (
                                <Form.Input
                                    label={
                                        <label>
                                            Custom input field name (optional)
                                            <Popup
                                                trigger={<Icon name="question circle outline" />}
                                                content="Information like SteamID, OriginID and so on"
                                            />
                                        </label>
                                    }
                                    {...input}
                                />
                            )}
                        />
                        <Field
                            name="toornament"
                            component={({ input }) => (
                                <Form.Input
                                    label={
                                        <label>
                                            Toornament ID (optional)
                                            <Popup
                                                trigger={<Icon name="question circle outline" />}
                                                content="If toornament should be the host of brackets etc."
                                            />
                                        </label>
                                    }
                                    {...input}
                                />
                            )}
                        />
                        <Field
                            name="report_win_loss"
                            type="checkbox"
                            component={({ input }) => (
                                <Form.Checkbox
                                    checked={input.checked}
                                    label="Users can report win/loss (optional)"
                                    onChange={(_, { checked }) => input.onChange(checked)}
                                    toggle
                                />
                            )}
                        />
                    </Slide>
                )}

                <Slide
                    validate={values => {
                        const errors = {};

                        if (values.register_time_start && !values.register_time_end) {
                            errors.register_time_end = 'Required when start time is set';
                        }

                        if (values.show_time_start && !values.show_time_end) {
                            errors.show_time_end = 'Required when start time is set';
                        }

                        if (values.external_url_login && !values.external_url_info) {
                            errors.external_url_info = 'Required when login url is set';
                        }

                        return errors;
                    }}
                >
                    <Header>Misc</Header>

                    <strong>
                        Enable competition pre regstration (optional)
                        <Popup
                            wide="very"
                            trigger={<Icon name="question circle outline" />}
                            content="Let users sign up, but not actually do anything until the competition starts"
                        />
                    </strong>
                    <Form.Group widths="equal">
                        <Form.Field>
                            <Field
                                name="register_time_start"
                                component={({ input, meta }) => (
                                    <DateTimePicker
                                        label="Registration start time (optional)"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Field
                                name="register_time_end"
                                component={({ input, meta }) => (
                                    <DateTimePicker
                                        label="Registration end time (optional)"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths="equal">
                        <Form.Field>
                            <Field
                                name="show_time_start"
                                component={({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition show start time (optional)"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Field
                                name="show_time_end"
                                component={({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition show end time (optional)"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                    </Form.Group>
                    <Field
                        name="participant_limit"
                        type="number"
                        component={({ input, meta }) => (
                            <Form.Input
                                label={
                                    <label>
                                        Max submissions/participants (optional)
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            content="Leave empty if no max limit is set. For competitions using Toornament; this can at most be 128"
                                        />
                                    </label>
                                }
                                {...input}
                                error={meta.touched && meta.error ? meta.error : null}
                            />
                        )}
                    />
                    <strong>
                        Externally hosted competition
                        <Popup
                            trigger={<Icon name="question circle outline" />}
                            content="Is something else than unicorn actually running the competition?"
                        />
                    </strong>
                    <Form.Group widths="equal">
                        <Form.Field>
                            <Field
                                name="external_url_login"
                                component={({ input, meta }) => (
                                    <Form.Input
                                        type="url"
                                        label="Login URL (optional)"
                                        {...input}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Field
                                name="external_url_info"
                                component={({ input, meta }) => (
                                    <Form.Input
                                        type="url"
                                        label="Homepage URL (optional)"
                                        {...input}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            />
                        </Form.Field>
                    </Form.Group>
                    <Field
                        name="rsvp"
                        component={({ input }) => (
                            <Form.Checkbox
                                checked={input.checked}
                                label={
                                    <label>
                                        RSVP only
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            content="Is the competition just a sign up? E.g. Workshop or Talk"
                                        />
                                    </label>
                                }
                                onChange={(_, { checked }) => input.onChange(checked)}
                                toggle
                            />
                        )}
                    />
                    <Field
                        name="featured"
                        component={({ input }) => (
                            <Form.Checkbox
                                checked={input.checked}
                                label={
                                    <label>
                                        Featured
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            content="Should the competition be featured and specially exposed on the front page? Use case are e.g. sponsored competitions"
                                        />
                                    </label>
                                }
                                onChange={(_, { checked }) => input.onChange(checked)}
                                toggle
                            />
                        )}
                    />
                </Slide>
            </Wizard>
        </>
    );
};

export default NewCompetition;
