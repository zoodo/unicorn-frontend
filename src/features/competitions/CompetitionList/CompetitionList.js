import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Header, Item } from 'semantic-ui-react';

import LoadingGate from '../../../components/LoadingGate';
import { CompetitionListItem } from '.';

const CompetitionList = ({
    as: As = CompetitionListItem,
    competitions,
    fetchCompetitions,
    isLoading,
    emptyText,
    preTo,
    wrapper: Wrapper,
    wrapperProps,
    isAdmin,
    filter,
}) => {
    const filteredCompetitions = useMemo(() => {
        return filter(competitions);
    }, [filter, competitions]);

    const hasCompetitions = useMemo(
        () =>
            Array.isArray(filteredCompetitions)
                ? !!filteredCompetitions.length
                : !!Object.entries(filteredCompetitions).length,
        [filteredCompetitions]
    );

    return (
        <LoadingGate onStartLoading={fetchCompetitions} isLoading={isLoading} spinner={!hasCompetitions}>
            {!isLoading && !hasCompetitions && emptyText && (
                <Header as="h1" textAlign="center">
                    {emptyText}
                </Header>
            )}

            {Array.isArray(filteredCompetitions) ? (
                <Wrapper {...wrapperProps}>
                    {filteredCompetitions.map(competition => (
                        <As preTo={preTo} key={competition.id} competition={competition} isAdmin={isAdmin} />
                    ))}
                </Wrapper>
            ) : (
                Object.entries(filteredCompetitions).map(([key, values]) => (
                    <React.Fragment key={key}>
                        <h1>{key}</h1>
                        <hr />

                        <Wrapper {...wrapperProps}>
                            {values.map(competition => (
                                <As preTo={preTo} key={competition.id} competition={competition} isAdmin={isAdmin} />
                            ))}
                        </Wrapper>
                    </React.Fragment>
                ))
            )}
        </LoadingGate>
    );
};

CompetitionList.defaultProps = {
    fetchCompetitions: () => null,
    competitions: [],
    emptyText: 'No competitions found',
    wrapper: Item.Group,
    wrapperProps: { divided: true },
    isAdmin: false,
    filter: c => c,
};

CompetitionList.propTypes = {
    fetchCompetitions: PropTypes.func.isRequired,
    as: PropTypes.object,
    competitions: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    emptyText: PropTypes.string,
    wrapper: PropTypes.elementType,
    wrapperProps: PropTypes.object,
    isAdmin: PropTypes.bool,
    filter: PropTypes.func,
};

export default React.memo(CompetitionList);
