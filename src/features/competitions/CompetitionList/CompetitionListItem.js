import React from 'react';
import { Link } from 'react-router-dom';
import { Image, Item, Button } from 'semantic-ui-react';

const CompetitionListItem = ({ competition, preTo = 'competitions', isAdmin }) => (
    <Item>
        <Link to={`/${preTo}/${competition.id}`}>
            <Image
                size="small"
                spaced="right"
                rounded
                src={competition.header_image}
                style={{ objectFit: 'cover', height: '100%' }}
            />
        </Link>
        <Item.Content>
            <Item.Extra>{competition.state.label}</Item.Extra>
            <Item.Header as={Link} to={`/${preTo}/${competition.id}`}>
                {competition.name}
            </Item.Header>
            <Item.Extra>{competition.brief_description}</Item.Extra>
        </Item.Content>
        {isAdmin && (
            <Item.Content verticalAlign="bottom">
                <Item.Extra style={{ textAlign: 'right' }}>
                    <Button
                        as={Link}
                        to={`/${preTo}/${competition.id}/admin`}
                        title={'Preferences for competition' + competition.name}
                        icon="wrench"
                        size="mini"
                        circular
                        inverted
                        color="red"
                    />
                </Item.Extra>
            </Item.Content>
        )}
    </Item>
);

export default CompetitionListItem;
