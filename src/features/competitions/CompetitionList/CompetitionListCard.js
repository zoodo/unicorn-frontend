import React from 'react';
import { Link } from 'react-router-dom';

import { Card, Image } from 'semantic-ui-react';

const CompetitionCard = ({ competition, preTo = 'competitions' }) => (
    <Card fluid as={Link} to={`/${preTo}/${competition.id}`} color={competition.published ? null : 'red'}>
        {competition.header_image && <Image src={competition.header_image} />}
        <Card.Content>
            <Card.Header>{competition.name}</Card.Header>
            <Card.Meta>{competition.state.label}</Card.Meta>
            <Card.Description>{competition.brief_description}</Card.Description>
        </Card.Content>
    </Card>
);

export default React.memo(CompetitionCard);
