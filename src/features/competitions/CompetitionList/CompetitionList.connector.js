import { connect } from 'react-redux';

import * as competitionDucks from '../../../ducks/competitions.duck';

import CompetitionList from './CompetitionList';
import { userIsCrew } from '../../../ducks/users.duck';
import { getUserId } from '../../../ducks/authentication.duck';

const mapStateToProps = state => {
    return {
        competitions: competitionDucks.getCompetitions(state),
        isLoading: competitionDucks.isFetching(state),
        isAdmin: userIsCrew(state, getUserId(state)),
    };
};

const mapDispatchToProps = {
    fetchCompetitions: competitionDucks.fetchCompetitions,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CompetitionList);
