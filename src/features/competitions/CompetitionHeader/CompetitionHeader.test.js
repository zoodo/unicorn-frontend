import React from 'react';
import { shallow } from 'enzyme';

import CompetitionHeader from './';

const competitionMock = {
    name: "I'm a competition!",
    brief_description: 'And I am a description',
    header_image: 'https://i.imgur.com/LWd5Usl.jpg',
};

describe('<CompetitionHeader />', () => {
    it('matches snapshot', () => {
        const component = shallow(<CompetitionHeader competition={competitionMock} />);
        expect(component).toMatchSnapshot();
    });
});
