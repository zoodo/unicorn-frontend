import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Header, Grid } from 'semantic-ui-react';

const CompetitionHeader = ({ competition, renderLeft, renderRight }) => {
    if (!competition) {
        return null;
    }

    const inverted = !!competition.header_image;

    return (
        <Segment basic className="competition-header">
            <div className="competition-header-before">
                <Grid columns="2" stackable>
                    <Grid.Column stretched>
                        <Grid.Row>
                            <Header inverted={inverted} as="h1">
                                {competition.name}
                            </Header>
                            <Header inverted={inverted}>{competition.brief_description}</Header>
                        </Grid.Row>
                        <Grid.Row>{renderLeft()}</Grid.Row>
                    </Grid.Column>
                    <Grid.Column>{renderRight()}</Grid.Column>
                </Grid>
            </div>
            <div
                className="competition-header-after"
                style={{ backgroundImage: `url("${competition.header_image}")` }}
            />
        </Segment>
    );
};

CompetitionHeader.defaultProps = {
    competition: null,
    renderLeft: () => null,
    renderRight: () => null,
};

CompetitionHeader.propTypes = {
    competition: PropTypes.object.isRequired,
    renderLeft: PropTypes.func,
    renderRight: PropTypes.func,
};

export default CompetitionHeader;
