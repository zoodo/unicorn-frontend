import React from 'react';
import { Form } from 'react-final-form';
import { Form as SemanticForm } from 'semantic-ui-react';

const FormController = ({ initialValues, onSubmit, validate, children }) => (
    <Form initialValues={initialValues} onSubmit={onSubmit} validate={validate} keepDirtyOnReinitialize>
        {({ handleSubmit, submitting, values }) => <SemanticForm onSubmit={handleSubmit}>{children}</SemanticForm>}
    </Form>
);

export default FormController;
