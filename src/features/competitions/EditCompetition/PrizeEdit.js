import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'semantic-ui-react';

import FormList from '../../../components/FormList';
import { formatNumber } from '../../../utils/numbers';

const PrizeEdit = ({ value, onChange }) => {
    const onPrizeAdd = () => onChange([...value, '']);

    const onPrizeRemove = id => onChange(value.filter((_, i) => i !== id));

    const onPrizeChange = id => event => {
        const pl = [...value];
        pl[id] = event.target.value;
        onChange(pl);
    };

    return (
        <FormList emptyText="Add prize" onAdd={onPrizeAdd} onRemove={onPrizeRemove}>
            {value.map((prize, i) => (
                <Form.Input
                    key={`# ${formatNumber(i + 1)}`}
                    placeholder={`# ${formatNumber(i + 1)}`}
                    value={prize}
                    onChange={onPrizeChange(i)}
                />
            ))}
        </FormList>
    );
};

PrizeEdit.defaultProps = {
    onChange: () => null,
    value: [],
};

PrizeEdit.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.array,
};

export default PrizeEdit;
