import CompetitionForm from './CompetitionForm';
import EditCompetition from './EditCompetition.connector';

export { CompetitionForm };
export { EditCompetition };

export default EditCompetition;
