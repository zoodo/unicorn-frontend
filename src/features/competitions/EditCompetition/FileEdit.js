import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Form } from 'semantic-ui-react';
import uuid from 'uuid';

import List from '../../../components/FormList';

const FileEdit = ({ value, onChange }) => {
    const onFileAdd = () =>
        onChange([...value, { file: 'archive', input: '', type: value.length ? 'screenshot' : 'entry', id: uuid() }]);

    const onFileRemove = id => onChange(value.filter((_, i) => i !== id));

    const onFileChange = (id, field) => (event, data) => {
        onChange(
            value.map(file => (file.id === id ? { ...file, [field]: data ? data.value : event.target.value } : file))
        );
    };

    return (
        <>
            {!!value.length && (
                <Grid>
                    <Grid.Row columns={2}>
                        <Grid.Column width={14}>
                            <Grid>
                                <Grid.Column width={6}>
                                    <label className="label">File type</label>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <label className="label">Input name</label>
                                </Grid.Column>
                                <Grid.Column width={6}>
                                    <label className="label">What kind of file is this?</label>
                                </Grid.Column>
                            </Grid>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            )}
            <List emptyText="Add file specification" onAdd={onFileAdd} onRemove={onFileRemove} staticFirst>
                {value.map((item, i) => (
                    <Grid key={item.id}>
                        <Grid.Column width={6}>
                            <Form.Field>
                                <Form.Select
                                    options={[
                                        {
                                            value: 'music',
                                            text: 'Music (wav, mp3, flac)',
                                        },
                                        {
                                            value: 'archive',
                                            text: 'Archive (zip, rar)',
                                        },
                                        {
                                            value: 'picture',
                                            text: 'Picture (png, jpg)',
                                        },
                                        {
                                            value: 'video',
                                            text: 'Video (mov, mp4)',
                                        },
                                    ]}
                                    onChange={onFileChange(item.id, 'file')}
                                    value={item.file}
                                />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Form.Field>
                                <Form.Input onChange={onFileChange(item.id, 'input')} value={item.input} />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <Form.Field>
                                {i === 0 && (
                                    <Form.Select
                                        options={[
                                            {
                                                value: 'entry',
                                                text: 'Main entry',
                                            },
                                        ]}
                                        onChange={onFileChange(item.id, 'type')}
                                        value={item.type}
                                    />
                                )}
                                {i !== 0 && (
                                    <Form.Select
                                        options={[
                                            {
                                                value: 'screenshot',
                                                text: 'Screenshot',
                                            },
                                            {
                                                value: 'progress1',
                                                text: 'Progress #1',
                                            },
                                            {
                                                value: 'progress2',
                                                text: 'Progress #2',
                                            },
                                            {
                                                value: 'progress3',
                                                text: 'Progress #3',
                                            },
                                            {
                                                value: 'other',
                                                text: 'Other',
                                            },
                                        ]}
                                        onChange={onFileChange(item.id, 'type')}
                                        value={item.type}
                                    />
                                )}
                            </Form.Field>
                        </Grid.Column>
                    </Grid>
                ))}
            </List>
        </>
    );
};

FileEdit.propTypes = {
    value: PropTypes.arrayOf(PropTypes.object).isRequired,
    onChange: PropTypes.func.isRequired,
};

export default FileEdit;
