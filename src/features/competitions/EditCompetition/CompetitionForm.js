import React, { useState, useEffect, memo } from 'react';
import { Button, Form, Input, Transition, Confirm, Popup, Icon, Message, Segment } from 'semantic-ui-react';
import { Field } from 'react-final-form';
import { EditorState, convertToRaw } from 'draft-js';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import FileEdit from './FileEdit';
import PrizeEdit from './PrizeEdit';
import DateTimePicker from '../../../components/DateTimePicker';
import LoadingGate from '../../../components/LoadingGate';
import InputLength from '../../../components/InputLength/InputLength';
import ToornamentStarter from '../../toornament/ToornamentStarter/ToornamentStarter';
import { convertRawToContentState } from '../../../utils/competitions';
import FormController from './FormController';

const useCompetitionValue = (competition, key, defaultValue) => {
    const [val, setVal] = useState(false);
    useEffect(() => {
        if (typeof defaultValue === 'undefined') {
            setVal(!!competition[key]);
        } else {
            setVal(defaultValue);
        }
    }, [competition, key, defaultValue]);

    return [val, setVal];
};

const CompetitionForm = ({
    fetchCompetition,
    loadingCompetition,
    competitionId,
    deleteCompetition,
    onSave,
    competition,
}) => {
    const [showConfirm, setConfirm] = useState(false);
    const toggleConfirm = () => setConfirm(!showConfirm);

    const [hasPreRegestration, setHasPreRegistration] = useCompetitionValue(competition, 'pre_registration_start');
    const [hasVoting, setHasVoting] = useCompetitionValue(competition, 'vote_time_start');
    const [hasExternal, setHasExternal] = useCompetitionValue(competition, 'external_url_login');
    const [hasExtra, setHasExtra] = useCompetitionValue(competition, 'extra');
    const [hasTeams, setHasTeams] = useCompetitionValue(competition, 'team_min');
    const [hasToornament, setHasToornament] = useCompetitionValue(competition, 'toornament');
    const [hasUpload, setHasUpload] = useCompetitionValue(
        competition,
        'fileupload',
        !!get(competition, 'fileupload', []).length
    );

    const validate = values => {
        const errors = {};

        const requiredKeys = [
            'name',
            'brief_description',
            'header_image',
            'header_credit',
            'run_time_start',
            'run_time_end',
        ];

        requiredKeys.forEach(v => {
            if (!values[v]) errors[v] = 'Required';
        });

        ['description', 'rules'].forEach(k => {
            if (!values[k] || !values[k].getCurrentContent().hasText()) {
                errors[k] = 'Required';
            }
        });

        if (hasVoting && values.vote_time_start && !values.vote_time_end) {
            errors.vote_time_end = 'Required when start time is set';
        }

        if (hasPreRegestration && values.register_time_start && !values.register_time_end) {
            errors.register_time_end = 'Required when start time is set';
        }

        if (values.show_time_start && !values.show_time_end) {
            errors.show_time_end = 'Required when start time is set';
        }

        if (hasUpload && values.fileupload) {
            values.fileupload.forEach(v =>
                Object.values(v).forEach(fv => {
                    if (!fv) errors.fileupload = 'One or more file fields are empty';
                })
            );
        }

        if (hasTeams) {
            if (values.team_min && !values.team_max) {
                errors.team_max = 'Required when minimum team size is set';
            }
            if (values.team_min && values.team_max && values.team_min > values.team_max) {
                errors.team_max = 'Maximum team size must be larger than minimum';
            }
        }

        if (hasExternal && values.external_url_login && !values.external_url_info) {
            errors.external_url_info = 'Required when login url is set';
        }

        return errors;
    };

    return (
        <Segment basic>
            <LoadingGate onStartLoading={fetchCompetition} isLoading={loadingCompetition}>
                <FormController
                    initialValues={{
                        ...competition,
                        rules: EditorState.createWithContent(convertRawToContentState(competition.rules)),
                        description: EditorState.createWithContent(convertRawToContentState(competition.description)),
                    }}
                    initialValuesEqual={(a, b) => isEqual(a, b)}
                    validate={validate}
                    onSubmit={(formData, formApi) => {
                        // pick changed fields from formData
                        const fields = Object.entries(formData).reduce(
                            (acc, [key, val]) => (!isEqual(competition[key], val) ? { ...acc, [key]: val } : acc),
                            {}
                        );

                        ['description', 'rules'].forEach(value => {
                            if (fields[value]) {
                                fields[value] = JSON.stringify(convertToRaw(fields[value].getCurrentContent()));
                            }
                        });

                        // reset disabled fields
                        [
                            { val: hasPreRegestration, key: 'pre_registration_start' },
                            { val: hasPreRegestration, key: 'pre_registration_end' },
                            { val: hasVoting, key: 'vote_time_start' },
                            { val: hasVoting, key: 'vote_time_end' },
                            { val: hasExternal, key: 'external_url_login' },
                            { val: hasExternal, key: 'external_url_info' },
                            { val: hasExtra, key: 'extra' },
                            { val: hasTeams, key: 'team_min' },
                            { val: hasTeams, key: 'team_max' },
                            { val: hasToornament, key: 'toornament' },
                            { val: hasToornament, key: 'report_win_loss' },
                            { val: hasUpload, key: 'fileupload' },
                        ].forEach(({ key, val, def = '' }) => {
                            if (!val && fields[val]) {
                                fields[key] = def;
                            }
                        });

                        onSave(fields);
                        // TODO use callback to add field errors
                    }}
                >
                    <h1>Information</h1>
                    <Field name="name">
                        {({ input, meta }) => (
                            <Form.Input
                                label="Competition name"
                                {...input}
                                error={meta.touched && meta.error ? meta.error : null}
                            />
                        )}
                    </Field>
                    <Field name="brief_description">
                        {({ input, meta }) => (
                            <Form.Field>
                                <label>
                                    Brief description{' '}
                                    <Popup
                                        trigger={<Icon name="question circle outline" />}
                                        content="A very short description of the competition"
                                    />
                                </label>
                                <InputLength length={input.value.length} max={40}>
                                    <Input
                                        {...input}
                                        maxLength={40}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                </InputLength>
                            </Form.Field>
                        )}
                    </Field>
                    <Field name="description">
                        {({ input, meta }) => (
                            <Form.Field>
                                <label>
                                    Description
                                    <Popup
                                        trigger={<Icon name="question circle outline" />}
                                        content="A general description of the competition"
                                    />
                                </label>
                                <Editor
                                    toolbarClassName="toolbarClassName"
                                    wrapperClassName="wrapperClassName"
                                    editorClassName="wysiwig-editor"
                                    onEditorStateChange={e => input.onChange(e)}
                                    editorState={input.value || EditorState.createEmpty()}
                                    toolbar={{
                                        options: [
                                            'inline',
                                            'blockType',
                                            'list',
                                            'textAlign',
                                            'colorPicker',
                                            'link',
                                            'emoji',
                                            'remove',
                                            'history',
                                        ],
                                        list: { inDropdown: true },
                                        textAlign: { inDropdown: true },
                                        image: {
                                            uploadEnabled: false,
                                        },
                                    }}
                                />
                                {meta.touched && meta.error && <Message negative content={meta.error} />}
                            </Form.Field>
                        )}
                    </Field>
                    <Field name="header_image">
                        {({ input, meta }) => (
                            <Form.Input
                                label={
                                    <label>
                                        Header image URL{' '}
                                        <Popup
                                            wide="very"
                                            trigger={<Icon name="question circle outline" />}
                                            content="URL to a picture to appear on top of the competition page. Recommended at least 1200px wide"
                                        />
                                    </label>
                                }
                                {...input}
                                error={meta.touched && meta.error ? meta.error : null}
                                type="url"
                            />
                        )}
                    </Field>
                    <Field name="header_credit">
                        {({ input, meta }) => (
                            <Form.Input
                                label={
                                    <label>
                                        Header image credit{' '}
                                        <Popup
                                            wide="very"
                                            trigger={<Icon name="question circle outline" />}
                                            content="Who took the picture?"
                                        />
                                    </label>
                                }
                                {...input}
                                error={meta.touched && meta.error ? meta.error : null}
                            />
                        )}
                    </Field>
                    <Field name="rules">
                        {({ input, meta }) => (
                            <Form.Field>
                                <label>Rules</label>
                                <Editor
                                    toolbarClassName="toolbarClassName"
                                    wrapperClassName="wrapperClassName"
                                    editorClassName="wysiwig-editor"
                                    onEditorStateChange={e => input.onChange(e)}
                                    editorState={input.value || EditorState.createEmpty()}
                                    toolbar={{
                                        options: [
                                            'inline',
                                            'blockType',
                                            'list',
                                            'textAlign',
                                            'colorPicker',
                                            'link',
                                            'emoji',
                                            'remove',
                                            'history',
                                        ],
                                        list: { inDropdown: true },
                                        textAlign: { inDropdown: true },
                                        image: {
                                            uploadEnabled: false,
                                        },
                                    }}
                                />
                                {meta.touched && meta.error && <Message negative content={meta.error} />}
                            </Form.Field>
                        )}
                    </Field>
                    <h1>Competition settings</h1>
                    <Form.Group widths="equal">
                        <Form.Field>
                            <Field name="run_time_start">
                                {({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition start time"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            </Field>
                        </Form.Field>
                        <Form.Field>
                            <Field name="run_time_end">
                                {({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition end time"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            </Field>
                        </Form.Field>
                    </Form.Group>
                    <Field name="participant_limit" type="number">
                        {({ input, meta }) => (
                            <Form.Input
                                label={
                                    <label>
                                        Max submissions/participants (optional){' '}
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            content="Leave empty if no max limit is set. For competitions using Toornament; this can at most be 128"
                                        />
                                    </label>
                                }
                                {...input}
                                error={meta.touched && meta.error ? meta.error : null}
                            />
                        )}
                    </Field>
                    <Form.Checkbox
                        checked={hasPreRegestration}
                        label={
                            <label>
                                Enable pre registration{' '}
                                <Popup
                                    wide="very"
                                    trigger={<Icon name="question circle outline" />}
                                    content="Let users sign up, but not actually do anything until the competition starts. Useful for game competitions"
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasPreRegistration(checked)}
                        toggle
                    />
                    <Transition visible={hasPreRegestration}>
                        <div>
                            <Form.Group widths="equal">
                                <Form.Field>
                                    <Field name="register_time_start">
                                        {({ input, meta }) => (
                                            <DateTimePicker
                                                label="Registration start time (optional)"
                                                onChange={event => input.onChange(new Date(event).toISOString())}
                                                value={input.value}
                                                error={meta.touched && meta.error ? meta.error : null}
                                            />
                                        )}
                                    </Field>
                                </Form.Field>
                                <Form.Field>
                                    <Field name="register_time_end">
                                        {({ input, meta }) => (
                                            <DateTimePicker
                                                label="Registration end time (optional)"
                                                onChange={event => input.onChange(new Date(event).toISOString())}
                                                value={input.value}
                                                error={meta.touched && meta.error ? meta.error : null}
                                            />
                                        )}
                                    </Field>
                                </Form.Field>
                            </Form.Group>
                        </div>
                    </Transition>
                    <Form.Checkbox
                        checked={hasVoting}
                        label={
                            <label>
                                Enable voting{' '}
                                <Popup
                                    trigger={<Icon name="question circle outline" />}
                                    content="Set a period after competition run time users can vote for entries"
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasVoting(checked)}
                        toggle
                    />
                    <Transition visible={hasVoting}>
                        <div>
                            <Form.Group widths="equal">
                                <Form.Field>
                                    <Field name="vote_time_start">
                                        {({ input, meta }) => (
                                            <DateTimePicker
                                                label="Voting start time  (optional)"
                                                onChange={event => input.onChange(new Date(event).toISOString())}
                                                value={input.value}
                                                error={meta.touched && meta.error ? meta.error : null}
                                            />
                                        )}
                                    </Field>
                                </Form.Field>
                                <Form.Field>
                                    <Field name="vote_time_end">
                                        {({ input, meta }) => (
                                            <DateTimePicker
                                                label="Voting end time (optional)"
                                                onChange={event => input.onChange(new Date(event).toISOString())}
                                                value={input.value}
                                                error={meta.touched && meta.error ? meta.error : null}
                                            />
                                        )}
                                    </Field>
                                </Form.Field>
                            </Form.Group>
                        </div>
                    </Transition>
                    <Form.Checkbox
                        checked={hasExternal}
                        label={
                            <label>
                                Externally hosted competition{' '}
                                <Popup
                                    trigger={<Icon name="question circle outline" />}
                                    content="Is something else than unicorn actually running the competition?"
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasExternal(checked)}
                        toggle
                    />
                    <Transition visible={hasExternal}>
                        <div>
                            <Form.Group>
                                <Field name="external_url_login">
                                    {({ input, meta }) => (
                                        <Form.Field>
                                            <Form.Input
                                                type="url"
                                                label="Login URL"
                                                {...input}
                                                error={meta.touched && meta.error ? meta.error : null}
                                            />
                                        </Form.Field>
                                    )}
                                </Field>
                                <Field name="external_url_info">
                                    {({ input, meta }) => (
                                        <Form.Field>
                                            <Form.Input
                                                label="Info URL"
                                                {...input}
                                                error={meta.touched && meta.error ? meta.error : null}
                                            />
                                        </Form.Field>
                                    )}
                                </Field>
                            </Form.Group>
                        </div>
                    </Transition>
                    <Form.Checkbox
                        checked={hasUpload}
                        label={
                            <label>
                                Enable file upload{' '}
                                <Popup
                                    trigger={<Icon name="question circle outline" />}
                                    content="Allow file upload during the competition run time"
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasUpload(checked)}
                        toggle
                    />
                    <Transition visible={hasUpload}>
                        <div>
                            <Field name="fileupload">
                                {({ input, meta }) => (
                                    <Form.Group>
                                        <div>
                                            <FileEdit onChange={input.onChange} value={input.value || []} />
                                        </div>
                                        {meta.touched && meta.error && <Message negative content={meta.error} />}
                                    </Form.Group>
                                )}
                            </Field>
                        </div>
                    </Transition>
                    <Form.Checkbox
                        checked={hasExtra}
                        label={
                            <label>
                                Enable custom input field{' '}
                                <Popup
                                    trigger={<Icon name="question circle outline" />}
                                    content="Information like SteamID, OriginID and so on"
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasExtra(checked)}
                        toggle
                    />
                    <Transition visible={hasExtra}>
                        <div>
                            <Field name="contributor_extra">
                                {({ input }) => <Form.Input label="Field name" {...input} />}
                            </Field>
                        </div>
                    </Transition>
                    <Form.Checkbox
                        checked={hasTeams}
                        label={
                            <label>
                                Enable teams{' '}
                                <Popup
                                    trigger={<Icon name="question circle outline" />}
                                    content="Allow teams to register. This will not let single persons register"
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasTeams(checked)}
                        toggle
                    />
                    <Transition visible={hasTeams}>
                        <div>
                            <Form.Group>
                                <Form.Field>
                                    <Field name="team_min">
                                        {({ input }) => (
                                            <Form.Input type="number" label="Minimum team size (optional)" {...input} />
                                        )}
                                    </Field>
                                </Form.Field>
                                <Form.Field>
                                    <Field name="team_max">
                                        {({ input }) => (
                                            <Form.Input type="number" label="Maximum team size" {...input} />
                                        )}
                                    </Field>
                                </Form.Field>
                            </Form.Group>
                        </div>
                    </Transition>
                    <Form.Checkbox
                        checked={hasToornament}
                        label={
                            <label>
                                Enable Toornament{' '}
                                <Popup
                                    trigger={<Icon name="question circle outline" />}
                                    content="If toornament should be the host of brackets etc."
                                />
                            </label>
                        }
                        onChange={(e, { checked }) => setHasToornament(checked)}
                        toggle
                    />
                    <Transition visible={hasToornament}>
                        <div>
                            <Form.Group>
                                <Field name="toornament">
                                    {({ input, meta }) => (
                                        <Form.Input
                                            label={
                                                <label>
                                                    Toornament ID (optional){' '}
                                                    <Popup
                                                        trigger={<Icon name="question circle outline" />}
                                                        content="If toornament should be the host of brackets etc."
                                                    />
                                                </label>
                                            }
                                            {...input}
                                            error={meta.touched && meta.error ? meta.error : null}
                                        />
                                    )}
                                </Field>
                                <Field name="report_win_loss" type="checkbox">
                                    {({ input }) => (
                                        <Form.Checkbox
                                            checked={input.checked}
                                            label="Users can report win/loss (optional)"
                                            onChange={(_, { checked }) => input.onChange(checked)}
                                            toggle
                                        />
                                    )}
                                </Field>
                            </Form.Group>
                        </div>
                    </Transition>
                    <Field
                        name="featured"
                        component={({ input }) => (
                            <Form.Checkbox
                                checked={input.value}
                                label={
                                    <label>
                                        Featured{' '}
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            content="Should the competition be featured and specially exposed on the front page? Use case are e.g. sponsored competitions"
                                        />
                                    </label>
                                }
                                onChange={(_, { checked }) => input.onChange(checked)}
                                toggle
                            />
                        )}
                    />
                    <h1>MISC</h1>
                    <Field name="rsvp">
                        {({ input }) => (
                            <Form.Checkbox
                                checked={input.checked}
                                label={
                                    <label>
                                        RSVP only{' '}
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            content="Is the competition just a sign up? E.g. Workshop or Talk"
                                        />
                                    </label>
                                }
                                onChange={(_, { checked }) => input.onChange(checked)}
                                toggle
                            />
                        )}
                    </Field>
                    <Form.Group widths="equal">
                        <Form.Field>
                            <Field name="show_time_start">
                                {({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition show start time  (optional)"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            </Field>
                        </Form.Field>
                        <Form.Field>
                            <Field name="show_time_end">
                                {({ input, meta }) => (
                                    <DateTimePicker
                                        label="Competition show end time (optional)"
                                        onChange={event => input.onChange(new Date(event).toISOString())}
                                        value={input.value}
                                        error={meta.touched && meta.error ? meta.error : null}
                                    />
                                )}
                            </Field>
                        </Form.Field>
                    </Form.Group>
                    <Form.Field className="Field">
                        <label className="label">Prizes</label>
                        <Field name="prizes" format={v => v}>
                            {({ input }) => <PrizeEdit {...input} />}
                        </Field>
                    </Form.Field>

                    <Button color="blue" type="submit">
                        Save
                    </Button>
                    {/* <Button color="yellow" type="button" onClick={togglePreview}>
                        Preview
                    </Button> */}
                    <Button color="red" type="button" onClick={toggleConfirm}>
                        Delete
                    </Button>
                    {competition.toornament && <ToornamentStarter id={competitionId} />}
                    <Confirm
                        open={showConfirm}
                        content="Are you sure you want to delete this competition?"
                        onCancel={toggleConfirm}
                        onConfirm={deleteCompetition}
                    />
                </FormController>
            </LoadingGate>
        </Segment>
    );
};

export default memo(CompetitionForm, (prevProps, nextProps) => isEqual(prevProps, nextProps));
