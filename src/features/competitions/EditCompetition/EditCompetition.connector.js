import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import get from 'lodash/get';

import * as competitionDuck from '../../../ducks/competitions.duck';
import CompetitionForm from './CompetitionForm';
import { parseError } from '../../../utils/error';

const mapStateToProps = (state, ownProps) => {
    const competitionId = ownProps.match.params.id;
    const competition = competitionDuck.getCompetition(state, competitionId) || {};

    return {
        competition,
        loadingCompetition: competitionDuck.isFetching(state),
        error: get(competitionDuck.getError(state), 'body', []),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const competitionId = ownProps.match.params.id;

    return {
        fetchCompetition: () => dispatch(competitionDuck.fetchCompetition(competitionId)),
        onSave: data =>
            dispatch(
                competitionDuck.updateCompetition(
                    competitionId,
                    data,
                    id => {
                        if (!competitionId) {
                            dispatch(push('competitions/' + id + '/admin'));
                        }

                        toast.success('Successfully saved competition!');
                    },
                    error => {
                        parseError(error).forEach(e => toast.error(e));
                    }
                )
            ),
        deleteCompetition: () =>
            dispatch(
                competitionDuck.deleteCompetition(
                    competitionId,
                    () => {
                        dispatch(push('/competitions'));
                    },
                    error => {
                        if (error.status === 404) {
                            toast.error('Competition not found. Redirecting...');
                            dispatch(push('/competitions'));
                        }
                    }
                )
            ),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompetitionForm));
