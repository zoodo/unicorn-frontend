import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import DisplayNameFormat from './DisplayNameFormat';

import * as usersDuck from '../../../ducks/users.duck';
import * as usersCombiner from '../../../ducks/users.combiner';

const mapStateToProps = state => {
    const user = usersCombiner.getOwnUserObject(state);
    const choices = usersDuck.getDisplayNameFormatChoices(state) || [];
    const isFetching = usersDuck.isFetching(state);

    return {
        user,
        choices,
        isFetching,
    };
};

const mapDispatchToProps = dispatch => ({
    fetchChoices: () => dispatch(usersDuck.fetchChoices()),
    handleOnChange: (e, { value }) =>
        dispatch(usersCombiner.updateOwnUser({ display_name_format: value }, () => toast.success('Saved!'))),
    updateParameter: (name, value) =>
        dispatch(usersCombiner.updateOwnUser({ [name]: value }, () => toast.success('Saved!'))),
});

export default connect(mapStateToProps, mapDispatchToProps)(DisplayNameFormat);
