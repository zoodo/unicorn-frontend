import React, { memo } from 'react';
import { Form, Header } from 'semantic-ui-react';

import LoadingGate from '../../../components/LoadingGate';
import InputSubmit from '../../../components/InputSubmit';

const DisplayNameFormat = ({ user, choices, isFetching, fetchChoices, handleOnChange, updateParameter }) => {
    return (
        <LoadingGate onStartLoading={fetchChoices} isLoading={isFetching}>
            <Header>
                You are currently known as <i>{user.display_name}</i>
            </Header>

            <Form>
                <Form.Dropdown
                    label="How do you want to be adressed? This is also how you will be presented in the result overview"
                    selection
                    value={user.display_name_format.value}
                    options={choices.map(choice => ({
                        key: choice.value,
                        text: choice.label,
                        value: choice.value,
                    }))}
                    onChange={handleOnChange}
                />

                <InputSubmit label="Nick" initialValue={user.nick} onSave={e => updateParameter('nick', e)} />
            </Form>
        </LoadingGate>
    );
};

export default memo(DisplayNameFormat);
