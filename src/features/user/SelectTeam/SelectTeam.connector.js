import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import SelectTeam from './SelectTeam';

import * as usersDuck from '../../../ducks/users.duck';
import * as usersCombiner from '../../../ducks/users.combiner';

const mapStateToProps = state => {
    const user = usersCombiner.getOwnUserObject(state);
    const isFetching = usersDuck.isFetching(state);

    return {
        user,
        isFetching,
    };
};

const mapDispatchToProps = dispatch => ({
    updateParameter: (name, value) =>
        dispatch(usersCombiner.updateOwnUser({ [name]: value }, () => toast.success('Saved!'))),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectTeam);
