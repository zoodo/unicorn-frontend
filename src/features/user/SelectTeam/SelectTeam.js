import React from 'react';
import { Dropdown } from 'semantic-ui-react';

import { TEAM_AI, TEAM_HUMAN } from '../../../ducks/users.duck';

const SelectTeam = ({ user, isFetching, updateParameter }) => {
    const currentTeam = user.team;
    const teamId = (currentTeam && currentTeam.id) || null;

    const onChange = (e, { value }) => {
        updateParameter('team', value);
    };

    return (
        <Dropdown
            fluid
            loading={isFetching}
            selection
            options={[
                {
                    value: TEAM_AI,
                    text: 'AI',
                },
                {
                    value: TEAM_HUMAN,
                    text: 'Human',
                },
            ]}
            value={teamId}
            onChange={onChange}
        />
    );
};

export default SelectTeam;
