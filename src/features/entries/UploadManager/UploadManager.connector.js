import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';

import * as competitionDuck from '../../../ducks/competitions.duck';
import * as entryDuck from '../../../ducks/entries.duck';

import UploadManager from './UploadManager';

const mapStateToProps = (state, ownProps) => {
    const { id, entryId } = ownProps.match.params;
    const competition = competitionDuck.getCompetition(state, id);
    const entry = entryDuck.getEntry(state, entryId);

    return {
        isLoading: competitionDuck.isFetching(state) || entryDuck.isFetching(state),
        shouldFetch: !entry || !(get(entry, 'obj_type') === 'full') || !competition,
        files: get(entry, 'files'),
        entry,
        uploadSchema: get(competition, 'fileupload'),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const { id, entryId } = ownProps.match.params;

    return {
        fetchData: () => {
            dispatch(entryDuck.fetchEntry(entryId));
            dispatch(competitionDuck.fetchCompetition(id));
        },
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UploadManager)
);
