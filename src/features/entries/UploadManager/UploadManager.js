import React from 'react';
import { Card } from 'semantic-ui-react';
import LoadingGate from '../../../components/LoadingGate';
import FileUpload from '../../../components/FileUpload';

const UploadManager = ({ shouldFetch, fetchData, uploadSchema, files, entry }) => {
    return (
        <LoadingGate onStartLoading={fetchData} shouldStartLoading={shouldFetch}>
            <Card.Group itemsPerRow={2} stackable>
                {uploadSchema.map(up => (
                    <FileUpload
                        entry={entry}
                        fileType={up.file}
                        inputId={up.id}
                        label={up.input}
                        uploadType={up.type}
                        key={up.id}
                        file={files && files.find(f => f.active && f.type === up.type)}
                        refreshEntry={fetchData}
                    />
                ))}
            </Card.Group>
        </LoadingGate>
    );
};

export default UploadManager;
