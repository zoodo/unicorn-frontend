import { connect } from 'react-redux';
import { withRouter, generatePath } from 'react-router-dom';
import { toast } from 'react-toastify';
import get from 'lodash/get';

import * as entryDuck from '../../../ducks/entries.duck';

import EntryManager from './EntryManager';

const mapStateToProps = (state, ownProps) => {
    const competitionId = ownProps.match.params.id;
    const entryId = ownProps.match.params.entryId;

    const entries = entryDuck.getEntriesByCompetition(state, Number(competitionId));
    const entry = entryDuck.getEntry(state, entryId);

    const basePath = '/competitions/:id/entries/:entryId';
    const entryIndex = entry ? entries.findIndex(e => e.id === entry.id) : -1;

    return {
        entry,
        shouldFetchEntries: get(entry, 'obj_type') !== 'full',
        nextEntry: !!entries[entryIndex + 1]
            ? generatePath(basePath, { id: competitionId, entryId: entries[entryIndex + 1].id })
            : false,
        previousEntry: !!entries[entryIndex - 1]
            ? generatePath(basePath, { id: competitionId, entryId: entries[entryIndex - 1].id })
            : false,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const competitionId = ownProps.match.params.id;
    const entryId = ownProps.match.params.entryId;

    return {
        fetchEntries: () => dispatch(entryDuck.fetchEntriesForCompetition(competitionId)),
        qualify: () => dispatch(entryDuck.qualifyEntry(entryId, () => toast.success('Qualified entry!'))),
        disqualify: comment => {
            if (!comment) {
                return toast.error('Missing required comment when disqualifying entry');
            }

            dispatch(entryDuck.disqualifyEntry(entryId, comment, () => toast.success('Disqualified entry')));
        },
        preSelect: () =>
            dispatch(entryDuck.preSelect(entryId, () => toast.success('Entry is disqualified as not preselected'))),
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(EntryManager)
);
