import React, { memo, useState } from 'react';
import { Grid, Divider, Button, Icon, Modal, Header, Input } from 'semantic-ui-react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';

import {
    STATUS_NEW,
    STATUS_QUALIFIED,
    STATUS_DISQUALIFIED,
    STATUS_NOT_PRE_SELECTED,
} from '../../../ducks/entries.duck';
import LoadingGate from '../../../components/LoadingGate';

const EntryManager = ({
    previousEntry,
    nextEntry,
    entry,
    qualify,
    disqualify,
    preSelect,
    fetchEntries,
    shouldFetchEntries,
}) => {
    const [disqualifyModal, setDisqualifyModalVisible] = useState(false);
    const [reason, setReason] = useState('');

    const disqualifyClick = () => {
        disqualify(reason);
        setDisqualifyModalVisible(false);
        setReason('');
    };

    const preSelectClick = () => {
        preSelect();
        setDisqualifyModalVisible(false);
        setReason('');
    };

    return (
        <>
            <LoadingGate onStartLoading={fetchEntries} shouldStartLoading={shouldFetchEntries}>
                <Grid columns="equal" celled="internally">
                    <Grid.Column>
                        <Button
                            disabled={previousEntry === false}
                            color="teal"
                            icon
                            labelPosition="left"
                            as={Link}
                            to={previousEntry}
                        >
                            <Icon name="arrow left" />
                            Previous
                        </Button>
                    </Grid.Column>
                    <Grid.Column textAlign="center">
                        {entry && (
                            <Button.Group>
                                {entry.status.value !== STATUS_QUALIFIED && (
                                    <Button positive onClick={qualify}>
                                        Qualify
                                    </Button>
                                )}
                                {entry.status.value === STATUS_NEW && <Button.Or />}
                                {![STATUS_DISQUALIFIED, STATUS_NOT_PRE_SELECTED].includes(entry.status.value) && (
                                    <Button negative onClick={() => setDisqualifyModalVisible(true)}>
                                        Disqualify
                                    </Button>
                                )}
                            </Button.Group>
                        )}
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <Button
                            disabled={nextEntry === false}
                            color="teal"
                            icon
                            labelPosition="right"
                            as={Link}
                            to={nextEntry}
                        >
                            Next
                            <Icon name="arrow right" />
                        </Button>
                    </Grid.Column>
                </Grid>
            </LoadingGate>
            <Divider />
            <Modal open={disqualifyModal} onClose={() => setDisqualifyModalVisible(false)} basic size="small">
                <Header icon="exclamation circle" content="Why do you want to disqualify this entry?" />
                <Modal.Content>
                    <Header as="h3" inverted>
                        If entry is disqualified because of preselection, a reason is not required
                    </Header>
                    <Input
                        value={reason}
                        onChange={e => setReason(e.target.value)}
                        placeholder="Disqualification reason"
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color="yellow" floated="left" onClick={preSelectClick} inverted>
                        <Icon name="close" /> Don't preselect
                    </Button>
                    <Button color="red" onClick={disqualifyClick} inverted>
                        <Icon name="close" /> Disqualify
                    </Button>
                    <Button color="green" onClick={() => setDisqualifyModalVisible(false)} inverted>
                        <Icon name="close" /> Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
        </>
    );
};

export default memo(EntryManager);
