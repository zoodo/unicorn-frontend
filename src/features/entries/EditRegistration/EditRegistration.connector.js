import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as competitionDuck from '../../../ducks/competitions.duck';
import * as entryDuck from '../../../ducks/entries.duck';

import EditRegistration from './EditRegistration';

const mapStateToProps = (state, ownProps) => {
    const { id, entryId } = ownProps.match.params;
    const competition = competitionDuck.getCompetition(state, id);
    const entry = entryDuck.getEntry(state, entryId);

    return {
        competition,
        entry,
        isOwner: entryDuck.amIOwner(state, entryId),
        owner: entryDuck.getEntryOwner(state, entryId),
    };
};

export default withRouter(connect(mapStateToProps)(EditRegistration));
