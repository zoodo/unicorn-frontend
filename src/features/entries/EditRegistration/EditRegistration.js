import React, { memo } from 'react';
import { Header, Divider, Message } from 'semantic-ui-react';
import View from '../../../components/View';
import RegisterEntry from '../RegisterEntry';
import ContributorEditor from '../ContributorEditor';
import UploadManager from '../UploadManager';
import ToornamentMatches from '../../toornament/ToornamentMatches';
import { hasTeams, hasFileupload, hasToornament } from '../../../utils/competitions';

const EditRegistration = ({ competition, isOwner, owner, entry }) => (
    <View icon="pencil" header="Edit your registration">
        {!isOwner && (
            <Message warning>
                Only <strong>{owner.display_name}</strong> can edit this registration
            </Message>
        )}

        <Header>General</Header>
        <RegisterEntry />
        {competition && hasTeams(competition) && (
            <>
                <Divider /> <ContributorEditor />
            </>
        )}
        {competition && hasFileupload(competition) && (
            <>
                <Divider /> <UploadManager />
            </>
        )}
        {competition && hasToornament(competition) && entry && (
            <>
                <Divider /> <ToornamentMatches entryId={entry.id} />
            </>
        )}
    </View>
);

export default memo(EditRegistration);
