import { connect } from 'react-redux';

import ContributorInfo from './ContributorInfo';

const mapStateToProps = (state, ownProps) => {
    const { entry } = ownProps;

    const contributors = (entry && entry.contributors) || [];

    return {
        contributors,
    };
};

export default connect(mapStateToProps)(ContributorInfo);
