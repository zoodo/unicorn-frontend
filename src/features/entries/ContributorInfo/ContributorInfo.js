import React from 'react';
import { Card } from 'semantic-ui-react';

const ContributorInfo = ({ contributors }) => {
    return (
        <Card.Group itemsPerRow={2} doubling>
            {contributors.map(contributor => {
                const { user } = contributor;

                return (
                    <Card key={contributor.id}>
                        <Card.Content>
                            <Card.Header>{user.display_name}</Card.Header>
                            <Card.Description>{user.email}</Card.Description>
                            <Card.Description>{user.phone_number}</Card.Description>
                            <Card.Meta>
                                Row / Seat: {user.row} / {user.seat}
                            </Card.Meta>
                        </Card.Content>
                    </Card>
                );
            })}
        </Card.Group>
    );
};

export default ContributorInfo;
