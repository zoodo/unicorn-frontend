import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';

import * as entryDucks from '../../../ducks/entries.duck';

import EntryTable from './EntryTable';

const mapStateToProps = (state, ownProps) => {
    const competitionID = get(ownProps, 'match.params.id');

    return {
        entries: competitionID ? entryDucks.getEntriesByCompetition(state, Number(competitionID)) : [],
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const competitionID = get(ownProps, 'match.params.id');

    return {
        fetchEntries: () => dispatch(entryDucks.fetchEntriesForCompetition(competitionID)),
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(EntryTable)
);
