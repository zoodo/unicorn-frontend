import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import { Link } from 'react-router-dom';
import { Button, Icon, Label } from 'semantic-ui-react';
import 'react-table/react-table.css';

import LoadingGate from '../../../components/LoadingGate';

import {
    STATUS_NEW,
    STATUS_QUALIFIED,
    STATUS_DISQUALIFIED,
    STATUS_NOT_PRE_SELECTED,
} from '../../../ducks/entries.duck';

const statusColor = status => {
    switch (status) {
        case STATUS_NEW:
            return 'yellow';

        case STATUS_QUALIFIED:
            return 'green';

        case STATUS_DISQUALIFIED:
        case STATUS_NOT_PRE_SELECTED:
            return 'red';

        default:
            return null;
    }
};

const EntryTable = ({ entries, fetchEntries, isLoading, location }) => {
    return (
        <LoadingGate onStartLoading={fetchEntries} isLoading={isLoading} spinner={!entries.length}>
            <ReactTable
                columns={[
                    {
                        Header: 'id',
                        accessor: 'id',
                        maxWidth: 50,
                    },
                    {
                        Header: 'Title',
                        accessor: 'title',
                    },
                    {
                        Header: 'Owner',
                        accessor: 'owner.display_name',
                    },
                    {
                        Header: 'Status',
                        Cell: props => (
                            <Label color={statusColor(props.original.status.value)}>
                                {props.original.status.label}
                            </Label>
                        ),
                    },
                    {
                        Header: 'Actions',
                        Cell: props => (
                            <Button.Group size="mini">
                                <Button icon color="teal" as={Link} to={`${location.pathname}/${props.original.id}`}>
                                    <Icon name="eye" />
                                </Button>
                            </Button.Group>
                        ),
                    },
                ]}
                data={entries}
            />
        </LoadingGate>
    );
};

EntryTable.defaultProps = {
    entries: [],
    isLoading: false,
};

EntryTable.propTypes = {
    entries: PropTypes.array,
    fetchEntries: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
};

export default EntryTable;
