import React, { useState } from 'react';
import { Segment, Item, Grid, Dropdown } from 'semantic-ui-react';
import { toast } from 'react-toastify';

import { unicornFetch } from '../../../utils/apiFetch';
import { parseError } from '../../../utils/error';

const OrderItem = ({ entry, index, options, addEntry }) => {
    const [loading, setLoading] = useState(false);

    const onChange = (e, { value }) => {
        setLoading(true);
        unicornFetch(
            { url: `competitions/entries/${entry.id}`, method: 'PATCH' },
            {
                order: value,
            }
        )
            .then(data => {
                addEntry(data);
                setLoading(false);
                toast.success('Saved!');
            })
            .catch(error => {
                parseError(error).map(e => toast.error(e));
                setLoading(false);
            });
    };

    return (
        <Segment>
            <Item>
                <Grid columns="3" divided>
                    <Grid.Column width={1}>
                        <Item.Header># {index + 1}</Item.Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                        <Item.Content>
                            <Item.Header>{entry.title}</Item.Header>
                        </Item.Content>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <Dropdown
                            options={options}
                            selection
                            value={entry.order}
                            loading={loading}
                            onChange={onChange}
                        />
                    </Grid.Column>
                </Grid>
            </Item>
        </Segment>
    );
};

export default OrderItem;
