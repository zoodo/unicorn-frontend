import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';

import * as entryDucks from '../../../ducks/entries.duck';

import OrderEntries from './OrderEntries';

const mapStateToProps = (state, ownProps) => {
    const competitionID = get(ownProps, 'match.params.id');

    return {
        entries: competitionID ? entryDucks.getEntriesByCompetition(state, Number(competitionID)) : [],
        isLoading: entryDucks.isFetching(state),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const competitionID = get(ownProps, 'match.params.id');

    return {
        fetchEntries: () => dispatch(entryDucks.fetchEntriesForCompetition(competitionID)),
        addEntry: data => dispatch(entryDucks.addEntry(data.id, data)),
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(OrderEntries)
);
