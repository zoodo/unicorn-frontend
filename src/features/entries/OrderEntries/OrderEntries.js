import React, { memo } from 'react';
import View from '../../../components/View';
import LoadingGate from '../../../components/LoadingGate';

import OrderItem from './OrderItem';

const byOrder = (a, b) => a.order - b.order;

const OrderEntries = ({ fetchEntries, isLoading, entries, addEntry }) => {
    const options = entries.map((e, i) => ({
        value: i + 1,
        key: i + 1,
        text: '# ' + (i + 1),
    }));

    return (
        <View icon="ordered list" header="Set entry order">
            <LoadingGate onStartLoading={fetchEntries} isLoading={isLoading}>
                {entries.sort(byOrder).map((entry, i) => (
                    <OrderItem entry={entry} index={i} options={options} addEntry={addEntry} />
                ))}
            </LoadingGate>
        </View>
    );
};

export default memo(OrderEntries);
