import React from 'react';
import { Message } from 'semantic-ui-react';

import {
    STATUS_DISQUALIFIED,
    STATUS_NEW,
    STATUS_QUALIFIED,
    STATUS_NOT_PRE_SELECTED,
} from '../../../ducks/entries.duck';

const EntryStatus = ({ status, comment }) => {
    switch (status.value) {
        case STATUS_NEW:
            return <Message color="yellow">Your registration is not reviewed yet</Message>;

        case STATUS_QUALIFIED:
            return <Message positive>Congratulations, your registration is qualified</Message>;

        case STATUS_DISQUALIFIED:
            return (
                <Message negative>
                    Your registration is disqualified: <strong>{comment}</strong>
                </Message>
            );

        case STATUS_NOT_PRE_SELECTED:
            return <Message negative>Your registration did not make it through the pre selection</Message>;

        default:
            return null;
    }
};

export default EntryStatus;
