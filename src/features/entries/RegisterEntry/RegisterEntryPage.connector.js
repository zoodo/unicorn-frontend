import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as competitionDuck from '../../../ducks/competitions.duck';
import * as entryDuck from '../../../ducks/entries.duck';

import RegisterEntryPage from './RegisterEntryPage';

const mapStateToProps = (state, ownProps) => {
    const { id, entryId } = ownProps.match.params;
    const competition = competitionDuck.getCompetition(state, id);
    const entry = entryDuck.getEntry(state, entryId);

    return { entry, competition };
};

export default withRouter(connect(mapStateToProps)(RegisterEntryPage));
