import React, { useState } from 'react';
import { Form, Button, Confirm } from 'semantic-ui-react';
import get from 'lodash/get';
import { createNextState } from '@reduxjs/toolkit';
import { getEventValue } from '../../../utils/formHelpers';
import LoadingGate from '../../../components/LoadingGate';
import EntryStatus from '../EntryStatus';

const RegisterEntry = ({
    isLoading,
    competition,
    shouldFetch,
    fetchEntry,
    entry,
    onSave,
    onResign,
    registrationType,
    isOwner,
    isEditing,
}) => {
    const [resignIsVisible, setResign] = useState(false);
    const [form, setForm] = useState({});

    const setFormValue = (name, raw = false) => (e, data) => {
        setForm(
            createNextState(form, draft => {
                draft[name] = raw ? e : getEventValue(e, data);
            })
        );
    };
    const getFormValue = (name, fallback = '') => {
        let entryVal = get(entry, name, fallback);
        if (entryVal === null) {
            entryVal = fallback;
        }

        return form[name] || entryVal;
    };

    const showResign = () => setResign(true);
    const hideResign = () => setResign(false);

    if (get(competition, 'rsvp')) {
        return (
            <LoadingGate isLoading={isLoading} onStartLoading={fetchEntry} shouldStartLoading={shouldFetch}>
                <Button onClick={onSave} color="green">
                    I would like to participate
                </Button>
            </LoadingGate>
        );
    }

    return (
        <LoadingGate isLoading={isLoading} onStartLoading={fetchEntry} shouldStartLoading={shouldFetch}>
            {entry && <EntryStatus status={entry.status} comment={entry.comment} />}

            <Form>
                <TitleField
                    registrationType={registrationType}
                    getFormValue={getFormValue}
                    setFormValue={setFormValue}
                />

                {get(competition, 'genre.category.value') === 2 && (
                    <Form.Group widths="equal">
                        <Form.Input
                            label="Message to big screen"
                            value={getFormValue('screen_msg')}
                            onChange={setFormValue('screen_msg')}
                        />

                        <Form.Input
                            label="Message to crew"
                            value={getFormValue('crew_msg')}
                            onChange={setFormValue('crew_msg')}
                        />
                    </Form.Group>
                )}

                {(!isEditing || (isEditing && isOwner)) && (
                    <Button onClick={() => onSave({ competition: competition.id, ...form })} color="green">
                        Save
                    </Button>
                )}

                {entry && isOwner && (
                    <Button onClick={showResign} color="red" floated="right">
                        Resign
                    </Button>
                )}
            </Form>

            <Confirm open={resignIsVisible} onCancel={hideResign} onConfirm={onResign} />
        </LoadingGate>
    );
};

const TitleField = ({ registrationType, getFormValue, setFormValue }) => {
    switch (registrationType) {
        case TEAM_ONLY:
            return <Form.Input label="Team name" value={getFormValue('title')} onChange={setFormValue('title')} />;

        case UPLOAD_TEAM:
            return (
                <>
                    {/* TODO Fix this, it does not scale */}
                    <Form.Input
                        label="Entry title"
                        value={getFormValue('title', ' by ').split(' by ')[0] || ''}
                        onChange={e =>
                            setFormValue(
                                'title',
                                true
                            )(e.target.value + ' by ' + getFormValue('title', ' by ').split(' by ')[1] || '')
                        }
                    />
                    <Form.Input
                        label="Team name"
                        value={getFormValue('title', ' by ').split(' by ')[1] || ''}
                        onChange={e =>
                            setFormValue(
                                'title',
                                true
                            )((getFormValue('title', ' by ').split(' by ')[0] || '') + ' by ' + e.target.value)
                        }
                    />
                </>
            );

        case UPLOAD_ONLY:
        default:
            return <Form.Input label="Entry title" value={getFormValue('title')} onChange={setFormValue('title')} />;
    }
};

export const UPLOAD_ONLY = 1;
export const TEAM_ONLY = 2;
export const UPLOAD_TEAM = 4;

export default RegisterEntry;
