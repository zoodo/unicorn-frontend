import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { replace, push } from 'connected-react-router';
import { toast } from 'react-toastify';
import get from 'lodash/get';

import * as entryDuck from '../../../ducks/entries.duck';
import * as competitionDuck from '../../../ducks/competitions.duck';
import * as usersCombiner from '../../../ducks/users.combiner';

import RegisterEntry, { UPLOAD_ONLY, TEAM_ONLY, UPLOAD_TEAM } from './RegisterEntry';
import { hasFileupload, hasTeams } from '../../../utils/competitions';

const mapStateToProps = (state, ownProps) => {
    const { id, entryId } = ownProps.match.params;
    const competition = competitionDuck.getCompetition(state, id);
    const entry = entryDuck.getEntry(state, entryId);

    const user = usersCombiner.getOwnUserObject(state);
    const isCrew = user && Number(user.role.value) === 1;

    return {
        isLoading: competitionDuck.isFetching(state) || entryDuck.isFetching(state),
        shouldFetch: (!!entryId && !entry) || !competition,
        competition,
        registrationType: registrationType(competition),
        entry,
        isEditing: entryId,
        isOwner: entry && entry.is_owner ? true : isCrew ? true : false,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const entryId = get(ownProps, 'match.params.entryId');
    const competitionId = get(ownProps, 'match.params.id');

    return {
        fetchEntry: () => {
            entryId && dispatch(entryDuck.fetchEntry(entryId));
            dispatch(competitionDuck.fetchCompetition(competitionId));
        },
        onSave: data =>
            dispatch(
                entryDuck.saveEntry(
                    entryId,
                    data,
                    competitionId,
                    id => {
                        id && dispatch(replace(ownProps.location.pathname + '/' + id));
                        if (entryId) {
                            toast.success('Saved changes!');
                        } else {
                            toast.success('Successfully registered!');
                        }
                    },
                    error => {
                        const errors = Object.entries(error.body).map(([key, val]) => {
                            if (Array.isArray(val)) {
                                return `${key}: ${val[0]}`;
                            }

                            return `${key}: ${val}`;
                        });

                        for (const e of errors) {
                            toast.error(e);
                        }
                    }
                )
            ),
        onResign: () =>
            entryId &&
            dispatch(
                entryDuck.deleteEntry(entryId, () => {
                    dispatch(push('/competitions/' + competitionId));
                    toast.success('Successfully resigned!');
                })
            ),
    };
};

const registrationType = competition => {
    if (!competition) {
        return null;
    }

    const team = hasTeams(competition);
    const upload = hasFileupload(competition);

    if (team && upload) {
        return UPLOAD_TEAM;
    } else if (team) {
        return TEAM_ONLY;
    } else if (upload) {
        return UPLOAD_ONLY;
    } else {
        return null;
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegisterEntry));
