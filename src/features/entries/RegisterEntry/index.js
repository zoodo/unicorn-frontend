import RegisterEntry from './RegisterEntry.connector';
import RegisterEntryPage from './RegisterEntryPage.connector';

export { RegisterEntry };

export { RegisterEntryPage };

export default RegisterEntry;
