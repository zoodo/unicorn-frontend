import React, { memo } from 'react';
import get from 'lodash/get';

import View from '../../../components/View';
import RegisterEntry from '.';

const RegisterEntryPage = ({ entry, competition }) => (
    <View header={entry ? entry.title : `Register ${(competition && 'entry for ' + get(competition, 'name')) || ''}`}>
        <RegisterEntry />
    </View>
);

export default memo(RegisterEntryPage);
