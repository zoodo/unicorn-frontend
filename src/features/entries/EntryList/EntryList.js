import React from 'react';
import PropTypes from 'prop-types';
import LoadingGate from '../../../components/LoadingGate';

const EntryList = props => {
    return (
        <LoadingGate {...props}>
            <h1>EntryList</h1>
        </LoadingGate>
    );
};

EntryList.PropTypes = {
    ...LoadingGate.propTypes,
};

export default EntryList;
