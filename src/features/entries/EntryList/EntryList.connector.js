import { connect } from 'react-redux';

import * as entryDucks from '../../../ducks/entries.duck';
import EntryList from './EntryList';

const mapStateToProps = state => {
    return {};
};

const mapDistpatchToProps = dispatch => {
    return {};
};

export default connect()(EntryList);
