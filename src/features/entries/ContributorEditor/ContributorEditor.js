import React, { useState, memo } from 'react';
import { Header, Table, Label, Button, Icon, Modal, Input, Divider } from 'semantic-ui-react';

import LoadingGate from '../../../components/LoadingGate';
import InputSubmit from '../../../components/InputSubmit/InputSubmit';
import UserSearch from '../../../components/UserSearch/UserSearch';

const ContributorEditor = ({
    isLoading,
    fetchEntry,
    shouldFetch,
    minContributors,
    contributors,
    isOwner,
    extraInfo,
    uid,
    updateContributor,
    addContributor,
    deleteContributor,
}) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedContributor, setContributor] = useState(null);
    const [newExtraInfo, setNewExtraInfo] = useState('');

    const handleAddContributor = () => {
        setModalVisible(false);

        const data = {
            user: selectedContributor,
        };

        if (extraInfo) {
            data.extra_info = newExtraInfo;
            setNewExtraInfo('');
        }

        addContributor(data);
    };

    return (
        <LoadingGate isLoading={isLoading} onStartLoading={fetchEntry} shouldStartLoading={shouldFetch}>
            <Header>Team members</Header>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>User</Table.HeaderCell>
                        {extraInfo && <Table.HeaderCell>{extraInfo}</Table.HeaderCell>}
                        {isOwner && <Table.HeaderCell>Actions</Table.HeaderCell>}
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {contributors.map((contributor, i) => (
                        <Table.Row key={contributor.id}>
                            {!!Object.keys(contributor).length && (
                                <>
                                    <Table.Cell>
                                        {contributor.is_owner && (
                                            <Label ribbon color="teal">
                                                Team leader
                                            </Label>
                                        )}
                                        {contributor.user.display_name}
                                    </Table.Cell>
                                    {extraInfo && (
                                        <>
                                            {(uid === contributor.user.id || isOwner) && (
                                                <Table.Cell>
                                                    <InputSubmit
                                                        initialValue={contributor.extra_info || ''}
                                                        onSave={extra_info =>
                                                            updateContributor(contributor.id, { extra_info })
                                                        }
                                                    />
                                                </Table.Cell>
                                            )}
                                            {uid !== contributor.user.id && !isOwner && (
                                                <Table.Cell>{contributor.extra_info}</Table.Cell>
                                            )}
                                        </>
                                    )}
                                    <Table.Cell>
                                        {isOwner && (
                                            <>
                                                <Button.Group size="mini">
                                                    {!contributor.is_owner && (
                                                        <Button
                                                            icon
                                                            color="red"
                                                            title="Remove user from team"
                                                            onClick={() => deleteContributor(contributor.id)}
                                                        >
                                                            <Icon name="delete" />
                                                        </Button>
                                                    )}
                                                </Button.Group>
                                            </>
                                        )}
                                    </Table.Cell>
                                </>
                            )}
                            {!Object.keys(contributor).length && (
                                <>
                                    <Table.Cell error={i < minContributors} colSpan={extraInfo ? 3 : 2}>
                                        {i < minContributors && <Icon name="attention" />}
                                        Missing {i < minContributors && 'mandatory'} member
                                    </Table.Cell>
                                    {isOwner && (
                                        <Table.Cell>
                                            {/* Create a user search component, just put it here with a onSelect method */}
                                            <Button icon size="mini" onClick={() => setModalVisible(true)}>
                                                <Icon name="search" />
                                            </Button>
                                        </Table.Cell>
                                    )}
                                </>
                            )}
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
            <Modal open={modalVisible} onClose={() => setModalVisible(false)} basic size="small">
                <Header icon="user" content="Add user" />
                <Modal.Content>
                    <UserSearch onChange={setContributor} />
                    <Divider hidden />
                    {extraInfo && (
                        <Input
                            label={extraInfo}
                            value={newExtraInfo}
                            onChange={(e, { value }) => setNewExtraInfo(value)}
                        />
                    )}
                </Modal.Content>
                <Modal.Actions>
                    <Button color="green" disabled={!selectedContributor} onClick={handleAddContributor} inverted>
                        <Icon name="save" /> Add
                    </Button>
                </Modal.Actions>
            </Modal>
        </LoadingGate>
    );
};

export default memo(ContributorEditor);
