import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import get from 'lodash/get';
import has from 'lodash/has';

import * as competitionDuck from '../../../ducks/competitions.duck';
import * as entryDuck from '../../../ducks/entries.duck';
import * as authenticationDuck from '../../../ducks/authentication.duck';
import * as userCombiner from '../../../ducks/users.combiner';

import ContributorEditor from './ContributorEditor';
import { parseError } from '../../../utils/error';

const mapStateToProps = (state, ownProps) => {
    const { id, entryId } = ownProps.match.params;
    const competition = competitionDuck.getCompetition(state, id);
    const entry = entryDuck.getEntry(state, entryId) || {};
    const uid = authenticationDuck.getUserId(state);
    const user = userCombiner.getOwnUserObject(state);

    const maxContributors = get(competition, 'teams.team_max');

    const contributors = buildContributorArray(entry.contributors || [], maxContributors);
    const isOwner = entry.is_owner || Number(user.role.value) === 1;

    return {
        isLoading: competitionDuck.isFetching(state) || entryDuck.isFetching(state),
        shouldFetch: (!!entryId && !entry) || !competition,
        competition,
        entry,
        contributors,
        isOwner,
        maxContributors,
        minContributors: get(competition, 'teams.team_min'),
        extraInfo: get(competition, 'extra.contributor_extra'),
        uid,
    };
};

const buildContributorArray = (contributors, maxContributors) => {
    const len = contributors.length;

    for (let i = 0; i < maxContributors - len; i++) {
        contributors.push({});
    }

    return [...contributors].sort((a, b) => {
        if (a.is_owner) {
            return -1;
        }

        if (b.is_owner) {
            return 1;
        }

        if (has(a, 'user.display_name') && has(b, 'user.display_name')) {
            if (a.user.display_name > b.user.display_name) {
                return 1;
            } else {
                return -1;
            }
        }

        return 0;
    });
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const isEditing = get(ownProps, 'match.params.entryId');
    const competitionId = get(ownProps, 'match.params.id');

    return {
        fetchEntry: () => {
            isEditing && dispatch(entryDuck.fetchEntry(isEditing));
            dispatch(competitionDuck.fetchCompetition(competitionId));
        },
        updateContributor: (id, data) =>
            dispatch(
                entryDuck.updateContributor(
                    id,
                    isEditing,
                    data,
                    () => {
                        toast.success('Saved!');
                    },
                    error => parseError(error).map(e => toast.error(e))
                )
            ),
        addContributor: data =>
            dispatch(
                entryDuck.createContributor(
                    isEditing,
                    data,
                    () => {
                        toast.success('Saved!');
                    },
                    error => parseError(error).map(e => toast.error(e))
                )
            ),
        deleteContributor: id =>
            dispatch(
                entryDuck.deleteContributor(
                    id,
                    isEditing,
                    () => {
                        toast.success('Saved!');
                    },
                    error => parseError(error).map(e => toast.error(e))
                )
            ),
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(ContributorEditor)
);
