import { connect } from 'react-redux';

import ParticipantLevelList from './ParticipantLevelList';

import * as achievementDucks from '../../../ducks/achievements.duck';
import * as authenticationDuck from '../../../ducks/authentication.duck';

const mapStateToProps = state => {
    return {
        userId: authenticationDuck.getUserId(state),
        levels: achievementDucks.getLevels(state) || [],
        isFetching: achievementDucks.isFetching(state),
    };
};

const mapDispatchToProps = dispatch => ({
    fetchLevels: userId => dispatch(achievementDucks.fetchLevels(userId)),
    setSeen: id => dispatch(achievementDucks.seenLevel(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ParticipantLevelList);
