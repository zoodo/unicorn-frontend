import React, { memo, useEffect } from 'react';
import { Card, Icon } from 'semantic-ui-react';
import dayjs from 'dayjs';

const ParticipantLevelList = ({ levels, fetchLevels, setSeen, isFetching, userId }) => {
    useEffect(() => {
        if (userId && !isFetching && !levels.length) {
            fetchLevels(userId);
        }
    }, [userId, fetchLevels, levels, isFetching]);

    const cardHover = level => {
        if (!level.seen) {
            setSeen(level.id);
        }
    };

    return (
        <Card.Group>
            {levels.map(level => (
                <Card onMouseEnter={() => cardHover(level)}>
                    <Card.Content>
                        <Card.Header>{level.category.name}</Card.Header>
                        <Card.Header>Level {level.level}</Card.Header>
                        <Card.Description>
                            {level.delivered ? (
                                <>
                                    <Icon name="check" color="green" />
                                    Handed out at {dayjs(level.delivered).format('DD-MM-YYYY HH:mm')}
                                </>
                            ) : (
                                <>
                                    <Icon
                                        name="exclamation"
                                        color={level.seen ? 'yellow' : 'red'}
                                        className={level.seen ? '' : 'fa-beat'}
                                    />
                                    Pick up your badge at the service desk
                                </>
                            )}
                        </Card.Description>
                    </Card.Content>
                </Card>
            ))}
        </Card.Group>
    );
};

export default memo(ParticipantLevelList);
