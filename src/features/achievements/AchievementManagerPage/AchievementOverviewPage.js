import React from 'react';
import { Link } from 'react-router-dom';
import View from '../../../components/View';
import { AchievementList, AchievementCard } from '../AchievementList';
import { Button, Divider } from 'semantic-ui-react';

const AchievementOverviewPage = () => (
    <View icon="trophy" header="Achievements">
        <Button as={Link} to="/admin/achievements/new">
            New achievement
        </Button>

        <Divider hidden />

        <AchievementList as={AchievementCard} />
    </View>
);

export default AchievementOverviewPage;
