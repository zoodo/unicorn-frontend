import React from 'react';

import View from '../../../components/View';
import AchievementManager from '../AchievementManager';

const AchievementManagePage = () => (
    <View icon="trophy" header="Achievements">
        <AchievementManager />
    </View>
);

export default AchievementManagePage;
