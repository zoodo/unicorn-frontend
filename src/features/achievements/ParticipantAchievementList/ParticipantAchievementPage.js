import React from 'react';
import { View } from '../../../components/View';
import { ParticipantAchievementList } from './index';
import ParticipantLevelList from '../ParticipantLevelList';
import { Divider } from 'semantic-ui-react';

export default ({ user }) => (
    <View icon="trophy" header={'Achievements'}>
        <ParticipantLevelList />
        <Divider hidden />
        <ParticipantAchievementList user={user} />
    </View>
);
