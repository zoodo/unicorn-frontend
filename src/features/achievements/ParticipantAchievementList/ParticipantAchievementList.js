import React, { useState } from 'react';
import { Card, Header, Icon, Accordion, Popup, Segment, Divider } from 'semantic-ui-react';
import LoadingGate from '../../../components/LoadingGate';
import logic from './logic';

export default ({ fetchAchievements, achievements }) => {
    const [accordionIndex, setAccordionIndex] = useState(-1);

    const byCategory = achievements.reduce(logic.splitIntoCategories, {});
    return (
        <LoadingGate onStartLoading={fetchAchievements}>
            <Accordion>
                {Object.keys(byCategory).map((categoryName, i) => {
                    const byCategoryAchievements = byCategory[categoryName];
                    return (
                        <>
                            <Accordion.Title
                                as={Header}
                                active={accordionIndex === i}
                                index={i}
                                onClick={(e, { index }) => setAccordionIndex(index === accordionIndex ? -1 : index)}
                            >
                                <Icon name="dropdown" />
                                {categoryName}
                            </Accordion.Title>
                            <Accordion.Content active={accordionIndex === i}>
                                <Card.Group stackable>
                                    {byCategoryAchievements.length > 0 &&
                                        byCategoryAchievements.map(achievement => (
                                            <Card color={logic.getColor(achievement.category.id)}>
                                                <Card.Content textAlign="center">
                                                    <Popup
                                                        trigger={
                                                            !achievement.my_award_count ? (
                                                                achievement.hidden ? (
                                                                    <Icon name="user secret" size="huge" color="red" />
                                                                ) : (
                                                                    <Icon name="close" size="huge" color="red" />
                                                                )
                                                            ) : achievement.hidden ? (
                                                                <Icon name="user secret" size="huge" />
                                                            ) : (
                                                                <Icon name="check" size="huge" color="green" />
                                                            )
                                                        }
                                                        content={achievement.requirement}
                                                    />
                                                </Card.Content>

                                                <Card.Content>
                                                    <Card.Header>{achievement.name}</Card.Header>
                                                    <Card.Meta>Points: {achievement.points}</Card.Meta>
                                                </Card.Content>
                                            </Card>
                                        ))}
                                </Card.Group>
                            </Accordion.Content>
                            {!byCategoryAchievements.length && (
                                <Header> You don't have any achievements in this category yet.</Header>
                            )}
                        </>
                    );
                })}
            </Accordion>

            <Divider />

            <Segment basic>
                <Header textAlign="center">
                    Not everything is visible to the naked eye. For the true heroes going the extra mile, the Resistance
                    will award you greatly.
                    <br />
                    Tread lightly, Mother sees it all.
                </Header>
            </Segment>
        </LoadingGate>
    );
};
