import { connect } from 'react-redux';
import ParticipantAchievementList from './ParticipantAchievementList';
import * as achievementDuck from '../../../ducks/achievements.duck';

const mapStateToProps = (state, props) => {
    const userId = props.user;
    const achievements = achievementDuck.getAchievements(state, userId);
    return {
        achievements,
    };
};

const mapDispatchToProps = {
    fetchAchievements: achievementDuck.fetchAchievements,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ParticipantAchievementList);
