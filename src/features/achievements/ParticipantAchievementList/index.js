import ParticipantAchievementList from './ParticipantAchievementList.connector';
import ParticipantAchievementPage from './ParticipantAchievementPage.connector';

export { ParticipantAchievementList, ParticipantAchievementPage };
export default ParticipantAchievementPage;
