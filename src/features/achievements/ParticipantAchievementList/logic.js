const splitIntoCategories = (byCategory, achievement) => {
    if (!byCategory[achievement.category.name]) {
        byCategory[achievement.category.name] = [];
    }

    byCategory[achievement.category.name].push(achievement);

    return byCategory;
};

const getColor = categoryId => {
    switch (categoryId) {
        case 1:
            return 'red';
        case 2:
            return 'green';
        case 3:
            return 'orange';
        case 4:
            return 'pink';
        case 5:
            return 'teal';
        default:
            return '';
    }
};

export default {
    splitIntoCategories,
    getColor,
};
