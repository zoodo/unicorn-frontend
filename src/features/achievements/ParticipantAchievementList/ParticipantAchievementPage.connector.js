import { connect } from 'react-redux';
import * as userCombiner from '../../../ducks/users.combiner';
import ParticipantAchievementPage from './ParticipantAchievementPage';

const mapStateToProps = state => ({
    user: userCombiner.getOwnUserObject(state),
});

export default connect(mapStateToProps)(ParticipantAchievementPage);
