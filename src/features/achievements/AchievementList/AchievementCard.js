import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'semantic-ui-react';

const AchievementCard = ({ achievement, adminMode }) => {
    return (
        <Card color={achievement.hidden ? 'red' : null}>
            <Card.Content>
                <Card.Header>{achievement.name}</Card.Header>
            </Card.Content>
            <Card.Content>{achievement.requirement}</Card.Content>
            <Card.Content>
                {adminMode && <Button icon="edit" as={Link} to={`/admin/achievements/${achievement.id}`} />}
            </Card.Content>
        </Card>
    );
};

export default AchievementCard;
