import React from 'react';
import PropTypes from 'prop-types';

import LoadingGate from '../../../components/LoadingGate';
import { Card } from 'semantic-ui-react';

const AchievementList = ({ as: As, achievements, fetchAchievements, adminMode }) => {
    return (
        <LoadingGate onStartLoading={fetchAchievements}>
            <Card.Group>
                {achievements.map(achievement => (
                    <As key={achievement.id} achievement={achievement} adminMode />
                ))}
            </Card.Group>
        </LoadingGate>
    );
};

AchievementList.defaultProps = {
    competitions: [],
};

AchievementList.propTypes = {
    fetchCompetitions: PropTypes.func.isRequired,
    as: PropTypes.object.isRequired,
    competitions: PropTypes.array,
};

export default React.memo(AchievementList);
