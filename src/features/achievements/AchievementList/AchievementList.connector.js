import { connect } from 'react-redux';

import AchievementList from './AchievementList';

import * as achievementDuck from '../../../ducks/achievements.duck';
import * as userDuck from '../../../ducks/users.duck';
import * as userCombiner from '../../../ducks/users.combiner';

const mapStateToProps = state => {
    const achievements = achievementDuck.getAchievements(state);
    const user = userCombiner.getOwnUserObject(state);
    const adminMode = userDuck.userIsCrew(user.id);

    return {
        achievements,
        adminMode,
    };
};

const mapDispatchToProps = {
    fetchAchievements: achievementDuck.fetchAchievements,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AchievementList);
