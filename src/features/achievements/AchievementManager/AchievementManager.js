import React, { memo, useState } from 'react';
import { Form } from 'semantic-ui-react';
import get from 'lodash/get';

import LoadingGate from '../../../components/LoadingGate';

const AchievementManager = ({
    achievement,
    fetchData,
    isLoading,
    category,
    categories,
    onSave,
    isEditing,
    onDelete,
}) => {
    const [formState, setFormState] = useState({});

    const onChange = target => (e, { value, checked }) => {
        setFormState({
            ...formState,
            [target]: value || checked,
        });
    };

    const getValue = (target, fallback = '') => {
        return formState[target] || get(achievement, target) || fallback;
    };

    const onSaveClick = () => {
        if (formState._category) {
            formState.category = formState._category;
            delete formState._category;
        }

        console.log(formState);

        onSave(formState);
    };

    return (
        <LoadingGate onStartLoading={fetchData} isLoading={isLoading}>
            <Form>
                <Form.Dropdown
                    value={getValue('_category', category)}
                    onChange={onChange('_category')}
                    selection
                    options={categories.map(c => ({
                        key: c.id,
                        value: c.id,
                        text: c.name,
                    }))}
                    label="Category"
                />
                <Form.Input value={getValue('name')} onChange={onChange('name')} label="Achievement name" />
                <Form.TextArea value={getValue('requirement')} onChange={onChange('requirement')} label="Requirement" />
                <Form.Input value={getValue('points')} onChange={onChange('points')} label="Points" />
                <Form.Checkbox checked={!!getValue('multiple')} onChange={onChange('multiple')} label="Multiple" />
                <Form.Checkbox checked={!!getValue('hidden')} onChange={onChange('hidden')} label="Hidden" />
                <Form.Dropdown
                    selection
                    multiple
                    options={[{ value: '1', text: 'Participants' }, { value: '2', text: 'Crew' }]}
                    onChange={onChange('type')}
                    value={getValue('type', [])}
                    label="Type"
                />

                <Form.Button positive onClick={onSaveClick}>
                    Save
                </Form.Button>

                {isEditing && (
                    <Form.Button negative onClick={onDelete}>
                        Delete
                    </Form.Button>
                )}
            </Form>
        </LoadingGate>
    );
};

export default memo(AchievementManager);
