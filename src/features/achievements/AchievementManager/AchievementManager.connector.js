import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { replace } from 'connected-react-router';
import { toast } from 'react-toastify';

import { parseError } from '../../../utils/error';
import AchievementManager from './AchievementManager';

import * as achievementDuck from '../../../ducks/achievements.duck';

const mapStateToProps = (state, ownProps) => {
    const { id } = ownProps.match.params;
    const isEditing = !!id;

    const achievement = isEditing && achievementDuck.getAchievement(state, id);
    const categories = achievementDuck.getCategories(state);

    return {
        isLoading: achievementDuck.isFetching(state),
        shouldFetchAchievement: isEditing && !achievement,
        isEditing,
        achievement,
        categories,
        category: achievement && achievement.category.id,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const { id } = ownProps.match.params;

    return {
        fetchData: () => {
            id && dispatch(achievementDuck.fetchAchievement(id));
            dispatch(achievementDuck.fetchCategories());
        },
        onSave: data =>
            dispatch(
                achievementDuck.saveAchievement(
                    id,
                    data,
                    data => {
                        toast.success('Saved!');
                        dispatch(replace('/admin/achievements/' + data.id));
                    },
                    error => {
                        console.log(error);
                        parseError(error).map(e => toast.error(e));
                    }
                )
            ),
        onDelete: () =>
            id &&
            dispatch(
                achievementDuck.deleteAchievement(id, () => {
                    toast.success('Deleted');
                    dispatch(replace('/admin/achievements/'));
                })
            ),
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(AchievementManager)
);
