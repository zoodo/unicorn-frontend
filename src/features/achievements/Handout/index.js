import Handout from './Handout.connector';
import HandoutPage from './HandoutPage';

export { Handout };

export { HandoutPage };

export default Handout;
