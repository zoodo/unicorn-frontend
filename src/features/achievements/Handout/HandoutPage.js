import React from 'react';

import View from '../../../components/View';
import Handout from './Handout.connector';

const HandoutPage = () => (
    <View icon="trophy" header="Achievements">
        <Handout />
    </View>
);

export default HandoutPage;
