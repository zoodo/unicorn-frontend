import React from 'react';
import { Button, Card, CardHeader, Dropdown, Divider, Header } from 'semantic-ui-react';
import LoadingGate from '../../../components/LoadingGate';
import NfcSearch from '../../../components/NfcSearch/NfcSearch';
import Levels from '../Levels';

const toOptions = (options, achievement) => {
    options.push({
        key: achievement.id,
        text: `"${achievement.name}" – ${achievement.requirement} ${
            !achievement.manual_validation ? '(Automatic)' : ''
        } ${achievement.hidden ? '(Easter egg)' : ''}`,
        value: achievement,
        disabled: !achievement.manual_validation,
    });

    return options;
};

const mapToOptions = achivements => achivements.reduce(toOptions, []);

class Handout extends React.Component {
    state = {
        user: null,
        selectedAchievement: null,
    };

    searchChange = e => {
        this.setState({ user: e });
    };

    searchClear = () => {
        this.setState({ user: null });
    };

    achievementChange = (e, { value }) => {
        this.setState({ selectedAchievement: value });
    };

    awardAchievement = () => {
        const { user, selectedAchievement } = this.state;
        this.props.awardAchievement(selectedAchievement.id, user.value);
    };

    render() {
        const { fetchAchievements, achievements = [] } = this.props;
        const { selectedAchievement, user } = this.state;

        return (
            <>
                <NfcSearch onChange={this.searchChange} />
                {user && (
                    <>
                        <Card>
                            <Card.Content>
                                <CardHeader>{user.text}</CardHeader>
                            </Card.Content>
                        </Card>
                        <LoadingGate onStartLoading={fetchAchievements} spinner={false}>
                            <Dropdown
                                clearable
                                selection
                                search
                                placeholder="Select achievement"
                                options={mapToOptions(achievements)}
                                onChange={this.achievementChange}
                            />
                        </LoadingGate>
                        {selectedAchievement && (
                            <Button positive onClick={this.awardAchievement}>
                                Submit
                            </Button>
                        )}
                        {!selectedAchievement && <Button onClick={this.searchClear}>Clear</Button>}
                        <Divider hidden />
                        <Header>Levels and badges</Header>
                        <Levels selectedUser={user} />
                    </>
                )}
            </>
        );
    }
}

export default Handout;
