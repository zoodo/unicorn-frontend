import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import Handout from './Handout';

import * as achievementDuck from '../../../ducks/achievements.duck';
import { parseError } from '../../../utils/error';

const mapStateToProps = (state, props) => {
    const achievements = achievementDuck.getAchievements(state);
    return {
        achievements,
    };
};

const mapDispatchToProps = dispatch => ({
    awardAchievement: (achievementId, userId) =>
        dispatch(
            achievementDuck.awardAchievement(
                achievementId,
                userId,
                () => toast.success('Saved!'),
                error => parseError(error).map(e => toast.error(e))
            )
        ),
    fetchAchievements: () => dispatch(achievementDuck.fetchAchievements()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Handout);
