import React, { useEffect, memo } from 'react';
import { Card, Button, Message } from 'semantic-ui-react';
import dayjs from 'dayjs';

const Levels = ({ levels, deliverLevel, fetchLevels, selectedUser = null, isFetching }) => {
    useEffect(() => {
        if (selectedUser) {
            fetchLevels(selectedUser.value);
        }
    }, [selectedUser, fetchLevels]);

    if (!levels.length) {
        return <Message warning>This user have not earned any levels yet</Message>;
    }

    return (
        <Card.Group>
            {levels.map(level => (
                <Card>
                    <Card.Content>
                        <Card.Header>{level.category.name}</Card.Header>
                        <Card.Header>Level {level.level}</Card.Header>
                        <Card.Description>
                            {level.delivered
                                ? `Handed out at ${dayjs(level.delivered).format('DD-MM-YYYY HH:mm')}`
                                : 'Not yet delivered'}
                        </Card.Description>
                    </Card.Content>
                    <Card.Content>
                        <Button
                            fluid
                            disabled={level.delivered}
                            onClick={() => deliverLevel(level.id)}
                            loading={isFetching}
                            color={level.delivered ? 'yellow' : 'green'}
                        >
                            Hand out badge
                        </Button>
                    </Card.Content>
                </Card>
            ))}
        </Card.Group>
    );
};

export default memo(Levels);
