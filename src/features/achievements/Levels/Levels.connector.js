import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import Levels from './Levels';

import * as achievementDucks from '../../../ducks/achievements.duck';
import { parseError } from '../../../utils/error';

const mapStateToProps = state => {
    return {
        levels: achievementDucks.getLevels(state) || [],
        isFetching: achievementDucks.isFetching(state),
    };
};

const mapDispatchToProps = dispatch => ({
    fetchLevels: userId => dispatch(achievementDucks.fetchLevels(userId)),
    deliverLevel: id =>
        dispatch(
            achievementDucks.deliverLevel(
                id,
                () => toast.success('Saved!'),
                error => parseError(error).map(e => toast.error(e))
            )
        ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Levels);
