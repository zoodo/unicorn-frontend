import React from 'react';
import { Button } from 'semantic-ui-react';
import { toast } from 'react-toastify';

import { unicornFetch } from '../../../utils/apiFetch';

class ToornamentStarter extends React.PureComponent {
    state = {
        loading: false,
    };

    onClick = () => {
        this.setState({ loading: true });
        unicornFetch({ url: `competitions/competitions/${this.props.id}/push_to_toornament`, method: 'POST' })
            .then(data => {
                this.setState({ loading: false });
                toast.success(JSON.stringify(data));
            })
            .catch(error => {
                this.setState({ loading: false });
                toast.error(JSON.stringify(error));
            });
    };

    render() {
        const { loading } = this.state;

        return (
            <Button disabled={loading} loading={loading} onClick={this.onClick}>
                Start Toornament
            </Button>
        );
    }
}

export default ToornamentStarter;
