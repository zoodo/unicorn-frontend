import React from 'react';
import { Segment, Item, Header } from 'semantic-ui-react';
import { unicornFetch } from '../../../utils/apiFetch';
class ToornamentMatches extends React.Component {
    state = {
        isFetching: false,
        matches: [],
    };

    componentDidMount() {
        this.fetchMatches();
    }

    fetchMatches = () => {
        this.setState({ isFetching: true });

        unicornFetch({ url: `competitions/entries/${this.props.entryId}/toornament_info` })
            .then(data => {
                this.setState({ isFetching: false, matches: data });
            })
            .catch(error => {
                this.setState({ isFetching: false });
                // parseError(error).map(e => toast.error(e));
            });
    };

    render() {
        if (!Array.isArray(this.state.matches)) {
            return null;
        }

        if (!this.state.matches.length) {
            return <Header>No matches yet...</Header>;
        }

        return this.state.matches.map(match => (
            <Segment key={match.id}>
                <Item.Group divided>
                    <Item.Content>
                        <Header
                            color={match.status === 'pending' ? 'red' : null}
                            style={{ textTransform: 'uppercase' }}
                        >
                            {match.status}
                        </Header>
                    </Item.Content>
                    {match.opponents.map(opponent => (
                        <Item key={opponent.number}>
                            <Item.Content>
                                {opponent.participant.name}

                                {!!opponent.participant.lineup.length &&
                                    opponent.participant.lineup.map(user => (
                                        <Item.Meta key={user.name}>
                                            {user.name} - {user.custom_user_identifier}
                                        </Item.Meta>
                                    ))}
                                {!opponent.participant.lineup.length && (
                                    <Item.Meta>{opponent.participant.custom_user_identifier}</Item.Meta>
                                )}
                            </Item.Content>
                        </Item>
                    ))}
                </Item.Group>
            </Segment>
        ));
    }
}

export default ToornamentMatches;
