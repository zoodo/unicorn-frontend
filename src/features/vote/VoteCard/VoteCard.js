import React from 'react';
import { Card, Rating, Image, Icon, Header } from 'semantic-ui-react';
import { toast } from 'react-toastify';

import { parseError } from '../../../utils/error';
import { unicornFetch, appendToUrlObject } from '../../../utils/apiFetch';

class VoteCard extends React.Component {
    state = {
        isFetching: false,
        score: null,
        vote: null,
    };

    onRate = (e, { rating }) => {
        this.setState({ score: rating }, () => {
            this.saveVote();
        });
    };

    saveVote = () => {
        if (!this.state.vote && !this.props.vote) {
            this.createVote();
        } else {
            this.updateVote();
        }
    };

    createVote = () => {
        this.setState({ isFetching: true });

        const { score } = this.state;

        unicornFetch(
            { url: 'competitions/votes', method: 'POST' },
            {
                entry: this.props.entry.id,
                score,
            }
        )
            .then(data => {
                toast.success('Saved!');
                this.setState({ isFetching: false, vote: data });
            })
            .catch(err => {
                parseError(err).map(e => toast.error(e));
                this.setState({ isFetching: false });
            });
    };

    updateVote = () => {
        this.setState({ isFetching: true });

        const { score } = this.state;
        const id = this.vote.id;

        unicornFetch(appendToUrlObject({ url: 'competitions/votes', method: 'PATCH' }, id), {
            score,
        })
            .then(data => {
                toast.success('Saved!');
                this.setState({ isFetching: false, vote: data });
            })
            .catch(err => {
                parseError(err).map(e => toast.error(e));
                this.setState({ isFetching: false });
            });
    };

    get vote() {
        return this.state.vote ? this.state.vote : this.props.vote || false;
    }

    render() {
        const { entry } = this.props;

        return (
            <Card>
                <FindFile entry={entry} />
                <Card.Content textAlign="center">
                    <Card.Header>{entry.title}</Card.Header>
                    {entry.owner && (
                        <Card.Meta>{entry.obj_type === 'full' ? entry.owner.display_name : entry.owner}</Card.Meta>
                    )}
                </Card.Content>
                <Card.Content textAlign="center">
                    <Rating
                        icon="star"
                        rating={this.vote.score || null}
                        min={0}
                        maxRating={5}
                        size="massive"
                        onRate={this.onRate}
                    />
                </Card.Content>
            </Card>
        );
    }
}

const FindFile = ({ entry }) => {
    const { files } = entry;

    if (!files.length) {
        return null;
    }

    const entryFile = files.find(f => f.type === 'entry');
    const screenshot = files.find(f => f.type === 'screenshot');

    if (!entryFile && !screenshot) {
        return null;
    }

    if (entryFile && entryFile.kind === 'picture') {
        return (
            <a href={entryFile.url} target="_blank" rel="noopener noreferrer">
                <Image src={entryFile.url} />
            </a>
        );
    }

    if (screenshot && screenshot.kind === 'picture') {
        return (
            <a href={screenshot.url} target="_blank" rel="noopener noreferrer">
                <Image src={screenshot.url} />
            </a>
        );
    }

    if (entryFile && entryFile.kind === 'music') {
        return (
            <Card.Content textAlign="center">
                <audio controls src={entryFile.url} style={{ width: '100%' }}>
                    <a href={entryFile.url} target="_blank" rel="noopener noreferrer">
                        <Icon name="download" size="huge" color="teal" />
                        <Header size="small" disabled>
                            Click here to view entry
                        </Header>
                    </a>
                </audio>
            </Card.Content>
        );
    }

    if (entryFile && entryFile.kind === 'video') {
        return (
            <Card.Content textAlign="center">
                <video controls width="100%" src={entryFile.url}>
                    <a href={entryFile.url} target="_blank" rel="noopener noreferrer">
                        <Icon name="download" size="huge" color="teal" />
                        <Header size="small" disabled>
                            Click here to view entry
                        </Header>
                    </a>
                </video>
            </Card.Content>
        );
    }

    if (entryFile) {
        return (
            <Card.Content textAlign="center">
                <a href={entryFile.url} target="_blank" rel="noopener noreferrer">
                    <Icon name="download" size="huge" color="teal" />
                    <Header size="small" disabled>
                        Click here to view entry
                    </Header>
                </a>
            </Card.Content>
        );
    }

    if (screenshot) {
        return (
            <Card.Content textAlign="center">
                <a href={screenshot.url} target="_blank" rel="noopener noreferrer">
                    <Icon name="download" size="huge" color="teal" />
                    <Header size="small" disabled>
                        Click here to view entry
                    </Header>
                </a>
            </Card.Content>
        );
    }
};

export default VoteCard;
