import React from 'react';
import { Card, Segment } from 'semantic-ui-react';

import View from '../../../components/View';
import { CompetitionList, CompetitionCard } from '../../competitions/CompetitionList';

const filterOpenCompetitions = competitions => competitions.filter(competition => competition.state.value === 32);

const VotePageRoot = () => (
    <View icon="heart" header="Competitions open for voting">
        <Segment basic>
            <CompetitionList
                emptyText="No competitions available for voting"
                filter={filterOpenCompetitions}
                as={CompetitionCard}
                preTo="vote"
                wrapper={Card.Group}
                wrapperProps={{ stackable: true, itemsPerRow: 3 }}
            />
        </Segment>
    </View>
);

export default VotePageRoot;
