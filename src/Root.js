import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import { store, history } from './store';
import App from './App';
import ScrollToTop from './components/ScrollToTop';

const Root = () => (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <ScrollToTop />
            <App />
        </ConnectedRouter>
    </Provider>
);

export default Root;
