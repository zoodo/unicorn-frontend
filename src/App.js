import React, { useEffect } from 'react';
import { Segment } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { Routes } from './views';
import { Mobile, Desktop } from './components/ResponsiveContainer';
import ErrorBoundary from './components/ErrorBoundary';
import { tokenIsValid } from './ducks/authentication.duck';
import { getOwnUserObject, fetchOwnUser } from './ducks/users.combiner';
import 'react-toastify/dist/ReactToastify.css';
import './styles/_imports.scss';

const App = () => {
    toast.configure();
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(state => tokenIsValid(state));

    const user = useSelector(state => getOwnUserObject(state));

    useEffect(() => {
        if (isAuthenticated && !user) {
            dispatch(fetchOwnUser());
        }
    }, [isAuthenticated, user, dispatch]);

    return (
        <ErrorBoundary>
            <Desktop>
                <Segment basic style={{ minHeight: 'calc(100vh - 80px)' }} className="no-padding">
                    <main id="content" style={{ minHeight: 'calc(100vh - 80px)' }}>
                        <Routes />
                    </main>
                </Segment>
            </Desktop>
            <Mobile>
                <Segment basic style={{ minHeight: 'calc(100vh - 80px)' }} className="no-padding">
                    <main id="content" style={{ minHeight: 'calc(100vh - 79px)' }}>
                        <Routes />
                    </main>
                </Segment>
            </Mobile>
            {/* <Segment vertical inverted>
                    <Container textAlign="center">
                        Made with <Icon name="heart" color="red" />
                        by <a href="https://zoodo.no">Zoodo</a>
                    </Container>
                </Segment> */}
        </ErrorBoundary>
    );
};

export default App;
