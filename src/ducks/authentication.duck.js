import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { replace } from 'connected-react-router';
import jwtDecode from 'jwt-decode';
import get from 'lodash/get';
import { authFetch, fetchToken } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';
import { fetchUser } from './users.duck';

const initialState = {
    isFetching: false,
    error: null,
    token: '',
    refreshToken: '',
    scope: null,
    exp: 0,
    iss: '',
    meta: {
        displayName: '',
        firstName: '',
        lastName: '',
        uid: '',
        username: '',
    },
};

const name = 'authentication';

// getters

export const getAuthenticationState = getState(name);
export const isFetching = state => getAuthenticationState(state).isFetching;
export const getError = state => getAuthenticationState(state).error;
export const tokenIsValid = state => getAuthenticationState(state).exp || 0;
export const userIsLoggedin = state =>
    !!tokenIsValid(state) ? getAuthenticationState(state).exp > Date.now() / 1000 : false;
export const getAccessToken = state => getAuthenticationState(state).token;
export const getRefreshToken = state => getAuthenticationState(state).refreshToken;
export const getUserId = state => get(getAuthenticationState(state), 'meta.uid');

// reducers

const authentication = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
        loginUser: (state, { payload }) => {
            Object.entries(parseToken(payload.data)).forEach(([key, val]) => (state[key] = val));
            state.token = payload.token;
            state.refreshToken = payload.refreshToken;
        },
        logoutUser: () => initialState,
    },
});

// exported actions

const { setIsFetching, setError, loginUser, logoutUser } = authentication.actions;

export const login = (code, state) => dispatch => {
    dispatch(setIsFetching());

    fetchToken({
        code,
        client_secret: process.env.REACT_APP_CLIENT_SECRET,
        client_id: process.env.REACT_APP_CLIENT_ID,
        grant_type: 'authorization_code',
        redirect_uri: document.location.origin + '/login',
    })
        .then(data => {
            batch(() => {
                dispatch(replace('/'));
                dispatch(setToken(data.access_token, data.refresh_token));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            if (typeof state === 'object') {
                if (state.path) {
                    dispatch(replace(state.path));
                }
            }
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error.body));
                dispatch(logoutUser());
            });
        });
};

export const refreshToken = token => (dispatch, _getState) => {
    if (isFetching(_getState())) {
        return;
    }

    dispatch(setIsFetching());

    fetchToken({
        client_secret: process.env.REACT_APP_CLIENT_SECRET,
        client_id: process.env.REACT_APP_CLIENT_ID,
        grant_type: 'refresh_token',
        refresh_token: token,
        redirect_uri: document.location.origin,
    })
        .then(data => {
            dispatch(setToken(data.access_token, data.refresh_token));
            dispatch(setIsFetching(false));
            dispatch(setError());
        })
        .catch(error => {
            dispatch(setIsFetching(false));
            dispatch(setError(error.body));
            dispatch(logout());
        });
};

const setToken = (accessToken, refreshToken) => dispatch => {
    const data = jwtDecode(accessToken);

    if (data.exp < Date.now() / 1000) {
        return dispatch(logoutUser());
    }

    dispatch(loginUser({ token: accessToken, refreshToken, data }));
    dispatch(fetchUser(data.uid));
};

export const logout = () => dispatch => {
    authFetch({
        url: '/api/accounts/logout',
        method: 'POST',
        mode: 'no-cors',
    });
    dispatch(logoutUser());
};

const parseToken = token => ({
    scope: token.scope,
    exp: token.exp,
    iss: token.iss,
    meta: {
        displayName: token.display_name,
        firstName: token.first_name,
        lastName: token.last_name,
        uid: token.uid,
        username: token.username,
    },
});

export { setIsFetching };

export default authentication.reducer;
