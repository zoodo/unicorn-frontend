const reducers = ['achievements', 'authentication', 'contributors', 'competitions', 'entries', 'genres', 'users'];

export const ducks = reducers.reduce((acc, value) => {
    acc[value] = require(`./${value}.duck`).default;
    return acc;
}, {});

export default ducks;
