import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { unicornFetch } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';
import dayjs from 'dayjs';

export const CATEGORY_OTHER = 1;
export const CATEGORY_CREATIVE = 2;
export const CATEGORY_GAME = 4;
export const CATEGORY_COMMUNITY = 8;

export const CATEGORY_ICON_MAP = {
    [CATEGORY_COMMUNITY]: 'group',
    [CATEGORY_CREATIVE]: 'paint brush',
    [CATEGORY_GAME]: 'game',
    [CATEGORY_OTHER]: 'qrcode',
};

const initialState = {
    data: {},
    isFetching: false,
    error: null,
    createdAt: null,
};

const name = 'genres';

// getters

const getGenreState = getState(name);
export const isFetching = state => getGenreState(state).isFetching;
export const getError = state => getGenreState(state).error;
export const getGenres = state => Object.values(getGenreState(state).data) || [];
export const getCategories = state =>
    Object.values(
        getGenres(state)
            .map(genre => genre.category)
            .reduce((unique, item) => (unique[item.value] ? unique : { ...unique, [item.value]: item }), {})
    );
export const getGenresForCategory = (state, categoryId) =>
    getGenres(state).filter(({ category }) => category.value === categoryId);

// reducers

const genres = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
        addGenres: (state, { payload }) => {
            payload.forEach(genre => (state.data[genre.id] = genre));
            state.createdAt = new Date().toISOString();
        },
    },
});

// actions

export const { setIsFetching, setError, addGenres } = genres.actions;

export const fetchGenres = (force = false) => (dispatch, _getState) => {
    const { createdAt } = getGenreState(_getState());
    const wasFetchedRecently = dayjs()
        .subtract('1', 'minute')
        .isBefore(dayjs(createdAt));

    if (createdAt && !wasFetchedRecently && !force) {
        return;
    }

    dispatch(setIsFetching());

    unicornFetch({ url: 'competitions/genres' })
        .then(data => {
            batch(() => {
                dispatch(addGenres(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export default genres.reducer;
