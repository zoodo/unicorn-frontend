import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { unicornFetch, appendToUrlObject } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';

const initialState = {
    data: {},
    isFetching: false,
    error: null,
};

const name = 'genres';

// getters

const getContributorState = getState(name);
export const isFetching = state => getContributorState(state).isFetching;
export const getError = state => getContributorState(state).error;
export const getContributors = state => getContributorState(state).data;

// reducers

const contributors = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
    },
    addContributor: (state, { payload }) => {
        state.data[payload.id] = payload;
    },
    addContributors: (state, { payload }) => {
        payload.forEach(contributor => (state.data[contributor.id] = contributor));
    },
});

// actions

export const { setIsFetching, setError, addContributors, addContributor } = contributors.actions;

export const fetchContributors = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch({ url: 'contributors' })
        .then(data => {
            batch(() => {
                dispatch(addContributors(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const fetchContributor = id => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject({ url: 'contributors' }, id))
        .then(data => {
            batch(() => {
                dispatch(addContributor(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export default contributors.reducer;
