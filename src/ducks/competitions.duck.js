import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { unicornFetch, appendToUrlObject } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';

const initialState = {
    data: {},
    isFetching: false,
    error: null,
};

const name = 'competitions';

// getters

const getCompetitionsState = getState(name);
export const isFetching = state => getCompetitionsState(state).isFetching;
export const getError = state => getCompetitionsState(state).error;
export const getCompetitionData = state => getCompetitionsState(state).data;
export const getCompetitions = state => Object.values(getCompetitionData(state));
export const getCompetition = (state, id) => getCompetitionData(state)[Number(id)] || null;

// reducers

const competitions = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
        addCompetitions: (state, { payload }) => {
            payload.forEach(competition => {
                state.data[competition.id] = competition;
                state.data[competition.id].createdAt = new Date().toISOString();
            });
        },
        addCompetition: (state, { payload }) => {
            state.data[payload.id] = payload;
            state.data[payload.id].createdAt = new Date().toISOString();
        },
        removeCompetition: (state, { payload }) => {
            delete state.data[payload];
        },
    },
});

// actions

const { setIsFetching, setError, addCompetition, addCompetitions, removeCompetition } = competitions.actions;

const url = 'competitions/competitions';
const endpoints = {
    get: {
        url,
    },
    create: {
        method: 'POST',
        url,
    },
    update: {
        method: 'PUT',
        url,
    },
    patch: {
        method: 'PATCH',
        url,
    },
    delete: {
        method: 'DELETE',
        url,
    },
};

export const fetchCompetitions = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.get)
        .then(data => {
            batch(() => {
                dispatch(addCompetitions(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

export const fetchCompetition = (id, onSuccess = () => null, force = false) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.get, id))
        .then(data => {
            batch(() => {
                dispatch(addCompetition(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess(data);
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

export const createCompetition = (competitionData, onSuccess, onError) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.create, competitionData)
        .then(data => {
            batch(() => {
                dispatch(setError());
                dispatch(setIsFetching(false));
            });
            onSuccess(data.id);
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
                if (onError) {
                    onError(error);
                }
            });
        });
};

export const updateCompetition = (id, competitionData, onSuccess, onError) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.patch, id), competitionData)
        .then(data => {
            batch(() => {
                dispatch(setError());
                dispatch(setIsFetching(false));
                dispatch(addCompetition(data));
            });
            onSuccess(data);
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
            if (onError) {
                onError(error);
            }
        });
};

export const deleteCompetition = (id, onSuccess, onError) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.delete, id))
        .then(() => {
            batch(() => {
                dispatch(removeCompetition(id));
                dispatch(setError());
                dispatch(setIsFetching(false));
            });
            onSuccess();
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
                if (error.status === 404) {
                    dispatch(removeCompetition(id));
                }
            });

            onError && onError(error);
        });
};

export const setPublished = (id, published = true) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.patch, id), { published })
        .then(data => {
            batch(() => {
                dispatch(setError());
                dispatch(setIsFetching(false));
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

export default competitions.reducer;
