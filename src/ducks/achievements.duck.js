import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { unicornFetch, appendToUrlObject } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';

const initialState = {
    achievements: {},
    categories: {},
    levels: {},
    stats: {},
    isFetching: false,
    error: null,
};

const name = 'achievements';

// getters

const getAchievementState = getState(name);
export const isFetching = state => getAchievementState(state).isFetching;
export const getError = state => getAchievementState(state).error;
export const getAchievements = state => Object.values(getAchievementState(state).data || {}) || [];
export const getAchievement = (state, id) => getAchievementState(state).achievements[id];
export const getCategories = state => Object.values(getAchievementState(state).categories) || [];
export const getLevels = state => Object.values(getAchievementState(state).levels) || [];
export const getStats = state => getAchievementState(state).stats;

// reducers

const genres = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
        addAchievements: (state, { payload }) => {
            payload.forEach(achievement => (state.achievements[achievement.id] = achievement));
        },
        addAchievement: (state, { payload }) => {
            state.achievements[payload.id] = payload;
        },
        addCategories: (state, { payload }) => {
            payload.forEach(category => (state.categories[category.id] = category));
        },
        addLevels: (state, { payload }) => {
            payload.forEach(level => (state.levels[level.id] = level));
        },
        addLevel: (state, { payload }) => {
            state.levels[payload.id] = payload;
        },
        addStats: (state, { payload }) => {
            state.stats = payload;
        },
    },
});

// actions

export const {
    setIsFetching,
    setError,
    addAchievements,
    addAchievement,
    addCategories,
    addLevels,
    addLevel,
    addStats,
} = genres.actions;

const achievementsUrl = 'achievements/achievements';
const categoriesUrl = 'achievements/categories';
const awardsUrl = 'achievements/awards';
const levelsUrl = 'achievements/levels';

const endpoints = {
    GET_ACHIEVEMENTS: {
        url: achievementsUrl,
    },
    CREATE_ACHIEVEMENT: {
        url: achievementsUrl,
        method: 'POST',
    },
    UPDATE_ACHIEVEMENT: {
        url: achievementsUrl,
        method: 'PATCH',
    },
    DELETE_ACHIEVEMENT: {
        url: achievementsUrl,
        method: 'DELETE',
    },
    GET_ACHIEVEMENT_STATS: {
        url: 'achievements/teams',
    },
    GET_CATEGORIES: {
        url: categoriesUrl,
    },
    GET_AWARDS: {
        url: awardsUrl,
    },
    POST_AWARD: {
        url: awardsUrl,
        method: 'POST',
    },
    GET_LEVELS: {
        url: levelsUrl,
    },
    PATCH_LEVEL_DELIVER: {
        url: levelsUrl,
        method: 'PATCH',
    },
    PATCH_LEVEL_SEEN: {
        url: levelsUrl,
        method: 'PATCH',
    },
};

export const fetchStats = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.GET_ACHIEVEMENT_STATS)
        .then(data => {
            batch(() => {
                dispatch(addStats(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const fetchAchievements = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.GET_ACHIEVEMENTS)
        .then(data => {
            batch(() => {
                dispatch(addAchievements(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const fetchAchievement = id => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.GET_ACHIEVEMENTS, id))
        .then(data => {
            batch(() => {
                dispatch(addAchievement(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const fetchCategories = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.GET_CATEGORIES)
        .then(data => {
            batch(() => {
                dispatch(addCategories(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const fetchLevels = userId => dispatch => {
    dispatch(setIsFetching());

    const userData = {};

    if (userId) {
        userData.user = userId;
    }

    unicornFetch(endpoints.GET_LEVELS, userData)
        .then(data => {
            batch(() => {
                dispatch(addLevels(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const deliverLevel = (id, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.PATCH_LEVEL_DELIVER, id + '/delivered'))
        .then(data => {
            batch(() => {
                dispatch(addLevel(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess(data);
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
            onError(error);
        });
};

export const seenLevel = id => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.PATCH_LEVEL_SEEN, id + '/seen'))
        .then(data => {
            batch(() => {
                dispatch(addLevel(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const awardAchievement = (achievement, user, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.POST_AWARD, {
        user,
        achievement,
    })
        .then(data => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess(data);
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
            onError(error);
        });
};

export const saveAchievement = (id, data, onSuccess = () => null, onError = () => null) => dispatch => {
    if (id) {
        return dispatch(updateAchievement(id, data, onSuccess, onError));
    }

    dispatch(createAchievement(data, onSuccess, onError));
};

export const updateAchievement = (id, data, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.UPDATE_ACHIEVEMENT, id), data)
        .then(data => {
            batch(() => {
                dispatch(addAchievement(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess(data);
        })
        .catch(error => {
            dispatch(setIsFetching(false));
            dispatch(setError(error));
            onError(error);
        });
};

export const createAchievement = (data, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.CREATE_ACHIEVEMENT, data)
        .then(data => {
            batch(() => {
                dispatch(addAchievement(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess(data);
        })
        .catch(error => {
            dispatch(setIsFetching(false));
            dispatch(setError(error));
            onError(error);
        });
};

export const deleteAchievement = (id, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.DELETE_ACHIEVEMENT, id))
        .then(data => {
            batch(() => {
                dispatch(fetchAchievements());
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess();
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
            onError(error);
        });
};

export default genres.reducer;
