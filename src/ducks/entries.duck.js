import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { unicornFetch, appendToUrlObject } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';

export const STATUS_DRAFT = 1;
export const STATUS_NEW = 2;
export const STATUS_QUALIFIED = 4;
export const STATUS_DISQUALIFIED = 8;
export const STATUS_NOT_PRE_SELECTED = 16;
export const STATUS_INVALID_FILE = 32;

const initialState = {
    data: {},
    isFetching: false,
    error: null,
};

const name = 'entries';

// getters

const getEntriesState = getState(name);
export const isFetching = state => getEntriesState(state).isFetching;
export const getError = state => getEntriesState(state).error;
export const getEntries = state => getEntriesState(state).data || {};
export const getEntry = (state, id) => getEntries(state)[id] || null;

export const getEntriesByCompetition = (state, competitionId) =>
    Object.values(getEntries(state)).filter(entry => entry.competition && entry.competition.id === competitionId);

export const userIsContributorInCompetition = (state, competitionId) => {
    if (!competitionId) {
        return false;
    }

    const entries = getEntriesByCompetition(state, competitionId);

    for (const entry of entries) {
        if (entry.is_contributor) {
            return entry;
        }
    }

    return false;
};

export const userIsOwnerInCompetition = (state, competitionId) => {
    if (!competitionId) {
        return false;
    }

    const entries = getEntriesByCompetition(state, competitionId);

    for (const entry of entries) {
        if (entry.is_owner) {
            return entry;
        }
    }

    return false;
};

export const amIOwner = (state, entryId) => {
    if (!entryId) {
        return false;
    }

    const entry = getEntry(state, entryId);

    if (!entry) {
        return false;
    }

    return entry.is_owner || false;
};

export const getEntryOwner = (state, entryId) => {
    if (!entryId) {
        return false;
    }

    const entry = getEntry(state, entryId);

    if (!entry) {
        return false;
    }

    return entry.owner || false;
};

// reducers

const contributors = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
        addEntries: (state, { payload }) => {
            const d = {};
            payload.forEach(entry => {
                d[entry.id] = entry;
            });

            state.data = d;
        },
        addEntry: (state, { payload }) => {
            state.data[payload.id] = payload;
        },
        removeEntry: (state, { payload }) => {
            delete state.data[payload];
        },
    },
});

// actions

export const { setIsFetching, setError, addEntries, addEntry, removeEntry } = contributors.actions;

const url = 'competitions/entries';
const endpoints = {
    GET: {
        url,
    },
    CREATE: {
        url,
        method: 'POST',
    },
    UPDATE: {
        url,
        method: 'PATCH',
    },
    DELETE: {
        url,
        method: 'DELETE',
    },
    UPDATE_CONTRIBUTOR: {
        url: 'competitions/contributors',
        method: 'PATCH',
    },
    CREATE_CONTRIBUTOR: {
        url: 'competitions/contributors',
        method: 'POST',
    },
    DELETE_CONTRIBUTOR: {
        url: 'competitions/contributors',
        method: 'DELETE',
    },
};

export const fetchEntries = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.GET)
        .then(data => {
            batch(() => {
                dispatch(addEntries(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

export const fetchEntriesForCompetition = (competition_id, votableOnly = false) => dispatch => {
    dispatch(setIsFetching());

    const params = {
        competition_id,
    };

    if (votableOnly) {
        params.vote = 1;
    }

    unicornFetch(endpoints.GET, params)
        .then(data => {
            batch(() => {
                dispatch(addEntries(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

export const fetchEntry = id => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.GET, id))
        .then(data => {
            batch(() => {
                dispatch(addEntry(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

export const saveEntry = (id = null, formData, competition = null, onSuccess = () => {}, onError = () => {}) => (
    dispatch,
    _getState
) => {
    const data = {
        competition,
        ...formData,
    };

    if (id) {
        return dispatch(updateEntry(id, data, onSuccess, onError));
    }

    dispatch(createEntry(data, onSuccess, onError));
};

export const createEntry = (formData, onSuccess, onError) => (dispatch, _getState) => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.CREATE, formData)
        .then(data => {
            batch(() => {
                dispatch(addEntry(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess(data.id);
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
            onError(error);
        });
};

export const updateEntry = (id, formData, onSuccess, onError) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.UPDATE, id), formData)
        .then(data => {
            batch(() => {
                dispatch(addEntry(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess();
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
            onError(error);
        });
};

export const deleteEntry = (id, onSuccess = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.DELETE, id))
        .then(() => {
            batch(() => {
                dispatch(removeEntry(id));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess();
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
        });
};

// TODO Move this into its own duck
export const updateContributor = (id, entryId, input, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.UPDATE_CONTRIBUTOR, id), input)
        .then(data => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError());
                dispatch(fetchEntry(entryId));
            });
            onSuccess(data);
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
            onError(error);
        });
};

// TODO Move this into its own duck
export const createContributor = (entryId, input, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.CREATE_CONTRIBUTOR, {
        entry: entryId,
        ...input,
    })
        .then(data => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError());
                dispatch(fetchEntry(entryId));
            });
            onSuccess(data);
        })
        .catch(error => {
            dispatch(setError(error));
            dispatch(setIsFetching(false));
            onError(error);
        });
};

// TODO Move this into its own duck
export const deleteContributor = (id, entryId, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.DELETE_CONTRIBUTOR, id))
        .then(data => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError());
                dispatch(fetchEntry(entryId));
            });
            onSuccess(data);
        })
        .catch(error => {
            batch(() => {
                dispatch(setError(error));
                dispatch(setIsFetching(false));
            });
            onError(error);
        });
};

export const qualifyEntry = (id, onSuccess = () => null, onError = () => null) => dispatch =>
    dispatch(updateEntry(id, { status: STATUS_QUALIFIED }, onSuccess, onError));

export const disqualifyEntry = (id, comment, onSuccess = () => null, onError = () => null) => dispatch =>
    dispatch(updateEntry(id, { status: STATUS_DISQUALIFIED, comment }, onSuccess, onError));

export const preSelect = (id, onSuccess = () => null, onError = () => null) => dispatch => {
    dispatch(updateEntry(id, { status: STATUS_NOT_PRE_SELECTED }, onSuccess, onError));
};

export default contributors.reducer;
