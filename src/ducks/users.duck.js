import { createSlice } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { userHasRole, ROLE_CREW } from '../utils/roles';
import { unicornFetch, appendToUrlObject } from '../utils/apiFetch';
import { getState, commonReducers } from '../utils/ducks';

export const TEAM_AI = 1;
export const TEAM_HUMAN = 2;

const initialState = {
    data: {},
    isFetching: false,
    error: null,
    choices: {
        data: {},
        createdAt: null,
    },
};

const name = 'users';

// getters

const getUsersState = getState(name);
export const isFetching = state => getUsersState(state).isFetching;
export const getError = state => getUsersState(state).error;
export const getChoices = state => getUsersState(state).choices || null;
export const getUsers = state => Object.values(getUsersState(state).data) || [];
export const getUser = (state, id) => getUsersState(state).data[id] || null;
export const userIsCrew = (state, userId) => userHasRole(getUser(state, userId), ROLE_CREW);
export const getDisplayNameFormatChoices = state => getChoices(state).data['user:display_name_format'] || null;

// reducers

const users = createSlice({
    name,
    initialState,
    reducers: {
        ...commonReducers,
        addUsers: (state, { payload }) => {
            payload.forEach(user => (state.data[user.id] = user));
        },
        addUser: (state, { payload }) => {
            state.data[payload.id] = payload;
        },
        addChoices: (state, { payload }) => {
            state.choices.data = payload;
            state.choices.createdAt = new Date().toISOString();
        },
    },
});

//actions

const { setIsFetching, setError, addUsers, addUser, addChoices } = users.actions;

const endpoints = {
    get: {
        url: 'users/users',
    },
    update: {
        url: 'users/users',
        method: 'PATCH',
    },
    choices: {
        url: 'users/_choices',
    },
};

export const fetchUsers = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.get)
        .then(data => {
            batch(() => {
                dispatch(addUsers(data.results));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const fetchUser = id => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.get, id))
        .then(data => {
            batch(() => {
                dispatch(addUser(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export const updateUser = (id, input, onSuccess = () => {}, onError = () => {}) => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(appendToUrlObject(endpoints.update, id), input)
        .then(data => {
            batch(() => {
                dispatch(addUser(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
            onSuccess();
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
            onError();
        });
};

export const fetchChoices = () => dispatch => {
    dispatch(setIsFetching());

    unicornFetch(endpoints.choices)
        .then(data => {
            batch(() => {
                dispatch(addChoices(data));
                dispatch(setIsFetching(false));
                dispatch(setError());
            });
        })
        .catch(error => {
            batch(() => {
                dispatch(setIsFetching(false));
                dispatch(setError(error));
            });
        });
};

export default users.reducer;
