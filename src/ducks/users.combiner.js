import * as authenticationDuck from './authentication.duck';
import * as usersDuck from './users.duck';

export const getOwnUserObject = state => {
    const uid = authenticationDuck.getUserId(state);

    if (uid) {
        return usersDuck.getUser(state, uid);
    }
};

export const updateOwnUser = (data, onSuccess = () => {}, onError = () => {}) => (dispatch, _getState) => {
    const id = authenticationDuck.getUserId(_getState());
    dispatch(usersDuck.updateUser(id, data, onSuccess, onError));
};

export const fetchOwnUser = () => (dispatch, _getState) => {
    const id = authenticationDuck.getUserId(_getState());
    dispatch(usersDuck.fetchUser(id));
};
