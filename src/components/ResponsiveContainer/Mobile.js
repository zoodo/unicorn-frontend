import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Responsive, Sidebar, Menu, Segment, Container } from 'semantic-ui-react';
import HeaderMenu from '../HeaderMenu';
import SideMenu from '../SideMenu';

const Mobile = ({ children, location }) => {
    const [sidebarOpened, setOpen] = useState(false);

    const hide = () => setOpen(false);
    const handleToggle = () => setOpen(!sidebarOpened);

    useEffect(() => {
        hide();
    }, [location]);

    return (
        <Responsive as={Sidebar.Pushable} maxWidth={Responsive.onlyMobile.maxWidth}>
            <Sidebar as={Menu} animation="push" inverted onHide={hide} vertical visible={sidebarOpened}>
                <SideMenu mobile />
            </Sidebar>

            <Sidebar.Pusher dimmed={sidebarOpened}>
                <Segment inverted textAlign="center" style={{ padding: '1em 0em' }} vertical>
                    <Container>
                        <HeaderMenu mobile handleToggle={handleToggle} />
                    </Container>
                </Segment>

                {children}
            </Sidebar.Pusher>
        </Responsive>
    );
};

Mobile.propTypes = {
    children: PropTypes.node,
};

export default memo(Mobile);
