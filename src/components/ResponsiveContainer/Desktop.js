import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Responsive, Menu, Segment, Visibility, Grid } from 'semantic-ui-react';
import HeaderMenu from '../HeaderMenu';
import SideMenu from '../SideMenu';

const Desktop = ({ children }) => (
    <Responsive minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility once={false}>
            <Segment inverted textAlign="center" vertical>
                <HeaderMenu />
            </Segment>
        </Visibility>

        <Segment vertical className="no-padding">
            <Grid>
                <Grid.Column width={3} className="no-padding-r">
                    <Menu vertical fluid>
                        <SideMenu />
                    </Menu>
                </Grid.Column>
                <Grid.Column stretched width={13} className="no-padding-l">
                    {children}
                </Grid.Column>
            </Grid>
        </Segment>
    </Responsive>
);

Desktop.propTypes = {
    children: PropTypes.node,
};

export default memo(Desktop);
