import React from 'react';
import * as Sentry from '@sentry/browser';
import { Container, Header, Image, Segment, Divider } from 'semantic-ui-react';

class ErrorBoundary extends React.Component {
    state = {
        eventId: null,
        hasError: false,
    };

    static getDerivedStateFromError(error) {
        return {
            hasError: true,
        };
    }

    componentDidCatch(error, errorInfo) {
        Sentry.withScope(scope => {
            scope.setExtras(errorInfo);
            const eventId = Sentry.captureException(error);
            this.setState({ eventId });
        });
    }

    render() {
        if (this.state.hasError) {
            return (
                <div
                    style={{
                        height: '100vh',
                        width: '100%',
                        backgroundColor: '#f0a8a8',
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    <Container textAlign="center">
                        <Header as="h1">Oh noes! A dead unicorn appears!</Header>

                        <Divider hidden section />
                        <Image size="large" centered src="/images/dedicorn.png" />
                        <Divider hidden section />

                        {this.state.eventId && (
                            <Segment basic>
                                <Header disabled>Unicorn ID: {this.state.eventId}</Header>
                                <Header>The unicorn herders have been notified, and will look into the cause.</Header>
                            </Segment>
                        )}
                    </Container>
                </div>
            );
        }

        return this.props.children;
    }
}

export default ErrorBoundary;
