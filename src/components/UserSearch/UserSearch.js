import React from 'react';
import { Dropdown, Message, Header } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';

import { unicornFetch } from '../../utils/apiFetch';
import { parseError } from '../../utils/error';

const endpoint = {
    url: 'users/search',
};

class UserSearch extends React.Component {
    state = {
        error: null,
        isFetching: false,
        searchQuery: null,
        value: '',
        options: [],
    };

    handleChange = (e, { value }) => {
        this.setState({ value });
        this.props.onChange(value);
    };

    handleSearchChange = (e, { searchQuery }) => this.setState({ searchQuery }, () => this.handleSearch());

    handleSearch = throttle(() => this.fetchOptions(), 500);

    fetchOptions = () => {
        if (this.state.searchQuery.length < 4) {
            return;
        }

        this.setState({ isFetching: true });

        let input = {};

        if (this.props.nfc) {
            input.usercard = this.state.searchQuery;
        } else {
            input.q = this.state.searchQuery;
        }

        unicornFetch(endpoint, input)
            .then(data => {
                this.setState({
                    isFetching: false,
                    options: data.results.map(user => ({
                        value: user.id,
                        key: user.id,
                        text: user.display_name,
                    })),
                    error: null,
                });
            })
            .catch(error => {
                this.setState({
                    error: parseError(error),
                });
            });
    };

    render() {
        const { error, options, isFetching, value } = this.state;

        return (
            <>
                {error && error.map(err => <Message error>{err}</Message>)}
                <Header inverted={!this.props.inverted} size="tiny">
                    Note: For a user to be searchable, they must have been logged in at least once
                </Header>
                <Dropdown
                    fluid
                    selection
                    search
                    options={options}
                    value={value}
                    placeholder="Search for users by name, nick or e-mail"
                    onChange={this.handleChange}
                    onSearchChange={this.handleSearchChange}
                    disabled={isFetching}
                    loading={isFetching}
                />
            </>
        );
    }

    static defaultProps = {
        onChange: () => {},
    };

    static propTypes = {
        onChange: PropTypes.func,
    };
}

export default UserSearch;
