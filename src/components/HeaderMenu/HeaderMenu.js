import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button, Menu, Icon, Dropdown } from 'semantic-ui-react';
import isEqual from 'lodash/isEqual';
import { getOscarUrl } from '../../utils/apiFetch';

const HeaderMenu = ({ isLoggedIn, logout, path, mobile, handleToggle, user, isFetching }) => (
    <Menu size="large" inverted fluid>
        {mobile && <MobileButtons onClick={handleToggle} />}
        {!mobile && <DesktopButtons />}
        {/* {user && user.team && (
            <Menu.Item>
                <Header color={user.team.id === 1 ? 'red' : 'yellow'}>{user.team.name}</Header>
            </Menu.Item>
        )} */}
        <Menu.Menu position="right">
            {user && !mobile && (
                <Dropdown item simple text={`Welcome, ${user.display_name}`}>
                    <Dropdown.Menu>
                        <Dropdown.Item as={Link} to="/preferences">
                            <Icon name="options" />
                            Preferences
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            )}
            <Menu.Item>
                {!isLoggedIn && (
                    <Button as="a" href={getOscarUrl({ path })} className="navbar-item" loading={isFetching}>
                        Login
                    </Button>
                )}
                {isLoggedIn && <Button onClick={logout}>Log out</Button>}
            </Menu.Item>
        </Menu.Menu>
    </Menu>
);

const MobileButtons = ({ onClick }) => (
    <>
        <Menu.Item onClick={onClick}>
            <Icon name="sidebar" />
        </Menu.Item>
        <Menu.Item>
            <Link to="/">UNICORN</Link>
        </Menu.Item>
    </>
);

const DesktopButtons = () => (
    <>
        <Menu.Item>
            <Link to="/">UNICORN</Link>
        </Menu.Item>
        <Menu.Item>
            <a href="https://gathering.org" target="_blank" rel="noreferrer noopener">
                GATHERING.ORG
            </a>
        </Menu.Item>
    </>
);

HeaderMenu.defaultProps = {
    fixed: true,
    mobile: false,
    handleToggle: () => null,
    isFetching: false,
};

HeaderMenu.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
    fixed: PropTypes.bool,
    mobile: PropTypes.bool,
    isFetching: PropTypes.bool,
    handleToggle: PropTypes.func,
};

export default memo(HeaderMenu, (prevProps, nextProps) => isEqual(prevProps, nextProps));
