import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as authenticationDucks from '../../ducks/authentication.duck';
import * as usersCombiner from '../../ducks/users.combiner';

import HeaderMenuComponent from './HeaderMenu';

const mapStateToProps = (state, ownProps) => {
    const user = usersCombiner.getOwnUserObject(state);

    return {
        isLoggedIn: authenticationDucks.userIsLoggedin(state),
        isFetching: authenticationDucks.isFetching(state),
        path: ownProps.location.pathname.startsWith('/login') ? '/' : ownProps.location.pathname,
        isCrew: user && user.role.value === 1,
        user,
    };
};

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(authenticationDucks.logout()),
});

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(HeaderMenuComponent)
);
