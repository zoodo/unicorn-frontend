import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Dimmer, Loader } from 'semantic-ui-react';

/**
 * General loading component inspired by Thomas Haugland Rudfoss
 *
 * If you're in need of a great React course in Norway, contact https://github.com/rudfoss
 */

/**
 * TODO: Only show spinner if a certain time has passed
 */

class LoadingGate extends React.PureComponent {
    // Begin some loading operation on mount
    componentDidMount() {
        if (this.props.onStartLoading && this.props.shouldStartLoading) {
            this.props.onStartLoading();
        }
    }

    // Provides a way to halt any loading operations to avoid unneccessary work
    componentWillUnmount() {
        if (this.props.onStopLoading) {
            this.props.onStopLoading();
        }
    }

    render() {
        const {
            spinner,
            isLoading,
            children,
            as,
            onStartLoading,
            shouldStartLoading,
            onStopLoading,
            ...rest
        } = this.props;
        const As = as;

        if (!Object.keys(rest).length) {
            rest.basic = true;
            rest.className = 'no-padding';
        }

        return (
            <As {...rest}>
                {spinner && (
                    <Dimmer active={isLoading} inverted>
                        <Loader />
                    </Dimmer>
                )}

                {children}
            </As>
        );
    }

    static defaultProps = {
        shouldStartLoading: true,
        isLoading: false,
        onStopLoading: () => null,
        spinner: true,
        as: Segment,
    };

    static propTypes = {
        onStartLoading: PropTypes.func.isRequired,
        isLoading: PropTypes.bool,
        children: PropTypes.node,
        onStopLoading: PropTypes.func,
        shouldStartLoading: PropTypes.bool,
        spinner: PropTypes.bool,
        as: PropTypes.elementType,
    };
}

export default LoadingGate;
