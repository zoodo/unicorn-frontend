import React from 'react';
import PropTypes from 'prop-types';

import cx from 'classnames';

class TimeLine extends React.Component {
    state = {
        items: [
            {
                enabled: false,
                start: '',
                end: '',
                label: 'Registration',
            },
            {
                enabled: false,
                start: '',
                end: '',
                label: 'Run time',
            },
            {
                enabled: false,
                start: '',
                end: '',
                label: 'Voting',
            },
        ],
    };

    reset = id => {
        const items = [...this.state.items];
        items[id] = {
            ...items[id],
            enabled: false,
            start: '',
            end: '',
        };

        this.setState({ items });
    };

    render() {
        return (
            <div className="timeline">
                <ul>
                    {this.state.items.map((item, i) => {
                        const tagClass = cx('tag', 'is-large', {
                            'is-success': item.enabled,
                            'is-dark': !item.enabled,
                        });

                        return (
                            <li>
                                <progress className="progress is-primary" value="15" max="100" />
                                <span className={tagClass}>
                                    {item.label}
                                    {item.enabled && <button className="delete" onClick={this.reset(i)} />}
                                </span>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

export default TimeLine;
