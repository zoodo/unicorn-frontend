import React from 'react';
import { shallow, mount } from 'enzyme';
import { Grid } from 'semantic-ui-react';

import FormList from './FormList';

const data = ['Hello', 'World'];
const onRemove = jest.fn(i => data.splice(i, 1));
const onAdd = jest.fn(() => data.push(Math.random()));

const w = mount(
    <FormList onAdd={onAdd} onRemove={onRemove}>
        {data.map(d => (
            <p key={d}>{d}</p>
        ))}
    </FormList>
);

describe('FormList renders as expected', () => {
    it('render matches snapshot', () =>
        expect(
            shallow(
                <FormList>
                    {data.map(d => (
                        <p key={d}>{d}</p>
                    ))}
                </FormList>
            )
        ).toMatchSnapshot());

    it('handles no children', () => expect(shallow(<FormList>{[].map(d => d)}</FormList>)).toMatchSnapshot());

    it('can add items', () => {
        const count = data.length;

        w.find(Grid)
            .last()
            .find('.button')
            .simulate('click');

        expect(count).toBeLessThan(data.length);
        expect(onAdd).toHaveBeenCalled();
    });

    it('can remove items', () => {
        const count = data.length;

        w.find(Grid)
            .first()
            .find('.button')
            .simulate('click');

        expect(count).toBeGreaterThan(data.length);
        expect(onRemove).toHaveBeenCalled();
    });
});
