import React from 'react';
import PropTypes from 'prop-types';

import { Button, Grid, Icon } from 'semantic-ui-react';

class FormList extends React.PureComponent {
    render() {
        const { children, onAdd, onRemove, emptyText, staticFirst } = this.props;

        if (React.Children.count(children) === 0) {
            return (
                <Button type="button" color="yellow" onClick={onAdd}>
                    {emptyText}
                </Button>
            );
        }

        return React.Children.map(children, (child, i) => (
            <Grid>
                <Grid.Row columns={2}>
                    <Grid.Column width={14}>{child}</Grid.Column>
                    <Grid.Column width={2}>
                        {(i === 0 && staticFirst) ||
                            (React.Children.count(children) - 1 !== i && (
                                <Button type="button" icon color="red" onClick={() => onRemove(i)}>
                                    <Icon name="minus" />
                                </Button>
                            ))}
                        {React.Children.count(children) - 1 === i && (
                            <Button type="button" icon color="green" onClick={onAdd}>
                                <Icon name="plus" />
                            </Button>
                        )}
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        ));
    }

    static defaultProps = {
        onAdd: () => {},
        onRemove: () => {},
        emptyText: 'Add item',
        staticFirst: false,
    };

    static propTypes = {
        children: PropTypes.node.isRequired,
        onAdd: PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
        emptyText: PropTypes.string,
        staticFirst: PropTypes.bool,
    };
}

export default FormList;
