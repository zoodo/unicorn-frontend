import React from 'react';
import PropTypes from 'prop-types';

import { Transition, Message } from 'semantic-ui-react';

const ErrorRenderer = ({ errorList }) => {
    if (Array.isArray(errorList) && errorList.length) {
        return (
            <Transition duration={150}>
                {errorList.map(error => {
                    return <Message />;
                })}
            </Transition>
        );
    }

    return Object.entries(errorList).map(([title, errors]) => (
        <Message negative key={title} header={title} icon="exclamation" list={errors} />
    ));
};

ErrorRenderer.defaultProps = {
    errorList: [],
};

ErrorRenderer.propTypes = {
    errorList: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

export default ErrorRenderer;
