import React from 'react';
import { Message, Header, Input, Card, Segment, Label } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { unicornFetch } from '../../utils/apiFetch';
import { parseError } from '../../utils/error';

const endpoint = {
    url: 'users/search',
};

class NfcSearch extends React.Component {
    state = {
        error: null,
        isFetching: false,
        searchQuery: '',
        options: [],
    };

    handleChange = e => {
        this.setState({ options: [], searchQuery: '' });
        this.props.onChange(e);
    };

    onKeyUp = e => {
        if (e.key === 'Enter') {
            this.fetchOptions();
        }
    };

    handleSearchChange = (e, { value }) => this.setState({ searchQuery: value });

    reset = () => {
        this.setState({ searchQuery: '' });
    };

    fetchOptions = () => {
        if (this.state.searchQuery.length < 4) {
            return;
        }

        this.setState({ isFetching: true });

        unicornFetch(endpoint, { usercard: this.state.searchQuery })
            .then(data => {
                this.setState({
                    isFetching: false,
                    error: !data.results.length ? ['No results found'] : null,
                });
                const mapUser = user => ({
                    value: user.id,
                    key: user.id,
                    text: user.display_name,
                });
                if (data.results.length > 1) {
                    this.setState({
                        options: data.results.map(mapUser),
                    });
                }
                if (data.results.length === 1) {
                    const user = data.results[0];
                    this.handleChange(mapUser(user));
                }
            })
            .catch(error => {
                this.setState({
                    error: parseError(error),
                });
            });
    };

    render() {
        const { error, options, isFetching, searchQuery } = this.state;

        return (
            <>
                <Header size="tiny">
                    Note: For a user to be searchable, they must have been logged into Unicorn at least once. Crew MUST
                    use the Wannabe login alternative
                </Header>
                <Segment vertical>
                    <Label pointing="right">Scan participant wrist band or crew badge</Label>
                    <Input
                        autoFocus
                        onChange={this.handleSearchChange}
                        onKeyUp={this.onKeyUp}
                        value={searchQuery}
                        loading={isFetching}
                    />
                </Segment>
                {error &&
                    error.map(err => (
                        <Message key={err} error>
                            {err}
                        </Message>
                    ))}
                <Segment vertical>
                    <Card.Group>
                        {options.map(o => (
                            <Card key={o.key} onClick={() => this.handleChange(o)} style={{ cursor: 'pointer' }}>
                                <Card.Content>{o.text}</Card.Content>
                            </Card>
                        ))}
                    </Card.Group>
                </Segment>
            </>
        );
    }

    static defaultProps = {
        onChange: () => {},
    };

    static propTypes = {
        onChange: PropTypes.func,
    };
}

export default NfcSearch;
