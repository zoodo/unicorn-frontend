import React from 'react';
import PropTypes from 'prop-types';

const InputLength = ({ length, max, children }) => (
    <>
        {children}
        <span className="input-length-helper">
            {length}/{max}
        </span>
    </>
);

InputLength.propTypes = {
    length: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    children: PropTypes.node.isRequired,
};

export default InputLength;
