import React from 'react';
import { shallow } from 'enzyme';

import InputLength from '.';

describe('<InputLength />', () => {
    const max = 12;
    let str = 'Hello, world';
    let component;

    beforeEach(
        () =>
            (component = shallow(
                <InputLength
                    length={str.length}
                    max={max}
                    children={<input type="text" onChange={e => (str = e.target.value)} value={str} />}
                />
            ))
    );
    it('renders', () => {
        expect(component).toMatchSnapshot();
    });

    it('shows updated length', () => {
        expect(component.text()).toEqual(`${str.length}/${max}`);
        component.find('input').simulate('change', { target: { value: 'Hello' } });
        component.setProps({ length: str.length });
        expect(component.text()).toEqual(`${str.length}/${max}`);
    });
});
