import View from './View';
import ViewHeader from './ViewHeader';

export { View };
export { ViewHeader };

export default View;
