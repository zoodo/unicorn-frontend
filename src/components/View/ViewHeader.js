import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Header, Divider } from 'semantic-ui-react';

const ViewHeader = ({ content, icon }) => (
    <>
        <Segment basic>
            <Header size="large" content={content} icon={icon} />
        </Segment>
        <Divider />
    </>
);

ViewHeader.defaultProps = {
    icon: null,
};

ViewHeader.propTypes = {
    icon: PropTypes.string,
    content: PropTypes.string.isRequired,
};

export default ViewHeader;
