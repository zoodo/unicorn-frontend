import React from 'react';
import PropTypes from 'prop-types';
import { Segment } from 'semantic-ui-react';
import ViewHeader from './ViewHeader';

const View = ({ children, header, icon }) => (
    <>
        {!!header && <ViewHeader content={header} icon={icon} />}
        <Segment basic className={`${!header ? 'no-padding' : ''}`}>
            {children}
        </Segment>
    </>
);

View.defaultProps = {
    header: null,
    icon: null,
};

View.propTypes = {
    children: PropTypes.node.isRequired,
    header: PropTypes.string,
    icon: PropTypes.string,
};

export default View;
