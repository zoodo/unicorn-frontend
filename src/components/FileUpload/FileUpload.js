import React from 'react';
import { Card, Icon, Header, Image } from 'semantic-ui-react';
import tus from 'tus-js-client';
import ProgressRing from '../ProgressRing/ProgressRing';

class FileUpload extends React.PureComponent {
    static getFileConstant = file => {
        switch (file) {
            case 'picture':
                return FileUpload.FILE_PICTURE;

            case 'archive':
                return FileUpload.FILE_ARCHIVE;

            case 'music':
                return FileUpload.FILE_MUSIC;

            case 'video':
                return FileUpload.FILE_VIDEO;

            default:
                return null;
        }
    };

    static STAGE_U;

    static FILE_PICTURE = {
        types: ['png', 'jpg'],
    };

    static FILE_ARCHIVE = {
        types: ['zip', 'rar'],
    };

    static FILE_MUSIC = {
        types: ['mp3', 'wav'],
    };

    static FILE_VIDEO = {
        types: ['mp4 h264'],
    };

    state = {
        progress: 0,
        stage: 3,
    };

    setupTus = e => {
        const file = e.target.files[0];

        if (!file) {
            return;
        }

        this.upload = new tus.Upload(file, {
            endpoint: process.env.REACT_APP_URL + '/upload/',
            metadata: {
                filename: file.name,
                filetype: file.type,
            },
            chunkSize: 15 * 1024 * 1024,
            headers: {
                'X-Unicorn-Entry-Id': this.props.entry.id,
                'X-Unicorn-File-Type': this.props.uploadType,
                Authorization: 'JWT ' + this.props.token,
            },
            onError: e => console.log('Upload failed: ' + e),
            onProgress: (bytesUploaded, bytesTotal) => {
                const progress = ((bytesUploaded / bytesTotal) * 100).toFixed(0);
                this.setState({ progress });
            },
            onSuccess: () => {
                this.setState({ stage: 3 });
                if (this.props.refreshEntry) {
                    this.props.refreshEntry();
                }
            },
        });

        this.setState({ stage: 2 });
        this.upload.start();
    };

    render() {
        const { inputId, fileType, label, file } = this.props;
        const { stage, progress } = this.state;

        return (
            <Card className="fileupload">
                <input type="file" name="file" id={inputId} className="inputfile" onChange={this.setupTus} />
                {stage === 1 && (
                    <Card.Content textAlign="center">
                        <label htmlFor={inputId}>
                            <Icon name="upload" color="teal" inverted circular size="huge" />
                            <Header>No file uploaded yet</Header>
                            <Header textAlign="center" disabled>
                                {label} ({FileUpload.getFileConstant(fileType).types.join(', ')})
                            </Header>
                        </label>
                    </Card.Content>
                )}
                {stage === 2 && (
                    <Card.Content textAlign="center">
                        <ProgressRing progress={progress} size={112} />
                        <Header textAlign="center" disabled>
                            {label} ({FileUpload.getFileConstant(fileType).types.join(', ')})
                        </Header>
                    </Card.Content>
                )}
                {stage === 3 && (
                    <>
                        {file && fileType === 'picture' && (
                            <a href={file.url} target="_blank" rel="noopener noreferrer">
                                <Image src={file.url} />
                            </a>
                        )}

                        {file && fileType !== 'picture' && (
                            <Card.Content textAlign="center">
                                <a href={file.url} target="_blank" rel="noopener noreferrer">
                                    <Icon name="download" size="huge" color="teal" />
                                </a>
                            </Card.Content>
                        )}

                        <Card.Content textAlign="center">
                            <Header textAlign="center" disabled>
                                {label} ({FileUpload.getFileConstant(fileType).types.join(', ')})
                            </Header>
                        </Card.Content>
                        <Card.Content>
                            <label htmlFor={inputId}>
                                <Icon name="upload" color="teal" inverted circular />
                                <Header icon>Upload new version</Header>
                            </label>
                        </Card.Content>
                    </>
                )}
            </Card>
        );
    }
}

export default FileUpload;
