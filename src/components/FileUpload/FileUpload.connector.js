import { connect } from 'react-redux';

import * as authenticationDuck from '../../ducks/authentication.duck';

import FileUpload from './FileUpload';

const mapStateToProps = state => {
    const token = authenticationDuck.getAccessToken(state);

    return {
        token,
    };
};

export default connect(mapStateToProps)(FileUpload);
