import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Button, Grid, Segment, Icon, Divider, Form as SemanticForm } from 'semantic-ui-react';
import { Form, FormSpy } from 'react-final-form';

const Wizard = ({ children, onSubmit, onChange, changeSubscription, initialValues }) => {
    const [page, setPage] = useState(0);
    const [formValues, setFormValues] = useState(initialValues);

    useEffect(() => {
        setFormValues(initialValues);
    }, [initialValues]);

    // Filter out pages that should not be shown
    const availablePages = useMemo(() => {
        const c = [];
        React.Children.forEach(React.Children.toArray(children), child => {
            if (!child.props.skip) {
                c.push(child);
            }
        });
        return c;
    }, [children]);

    const pageCount = React.Children.count(availablePages);
    const isLastPage = page === pageCount - 1;
    const currentPage = useMemo(() => availablePages[page], [availablePages, page]);

    // Go to last page if index goes over available pages
    useEffect(() => {
        if (page > pageCount) {
            setPage(pageCount - 1);
        }
    }, [pageCount, page]);

    const onPrevious = () => page > 0 && setPage(page - 1);
    const onNext = values => {
        !isLastPage && setPage(page + 1);
        values && setFormValues(values);
    };

    const validate = values => (currentPage.props.validate ? currentPage.props.validate(values) : {});
    // const validate = () => {};

    const submit = values => {
        if (isLastPage) {
            onSubmit(values);
        } else {
            onNext(values);
        }
    };

    const { props } = currentPage;

    return (
        <Form initialValues={formValues} validate={validate} onSubmit={submit}>
            {({ handleSubmit, submitting }) => (
                <SemanticForm onSubmit={handleSubmit}>
                    {onChange && changeSubscription && (
                        <FormSpy subscription={changeSubscription} onChange={onChange} />
                    )}
                    <Segment>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column>{currentPage}</Grid.Column>
                            </Grid.Row>

                            <Divider />

                            <Grid.Row columns={3}>
                                <Grid.Column>
                                    <Button floated="left" type="button" onClick={onPrevious} disabled={page === 0}>
                                        <Icon name="arrow left" />
                                        BACK
                                    </Button>
                                </Grid.Column>

                                <Grid.Column textAlign="center">
                                    {page + 1}/{pageCount}
                                </Grid.Column>

                                <Grid.Column>
                                    <Button
                                        floated="right"
                                        type="submit"
                                        icon
                                        disabled={props.disabled}
                                        loading={submitting}
                                    >
                                        {isLastPage ? (
                                            <>
                                                SUBMIT <Icon name="save" />
                                            </>
                                        ) : (
                                            <>
                                                NEXT <Icon name="arrow right" />
                                            </>
                                        )}
                                    </Button>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Segment>
                </SemanticForm>
            )}
        </Form>
    );
};

Wizard.defaultProps = {
    onSubmit: () => null,
    onChange: null,
    changeSubscription: null,
};

Wizard.propTypes = {
    children: PropTypes.node.isRequired,
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    changeSubscription: PropTypes.object,
};

export default Wizard;
