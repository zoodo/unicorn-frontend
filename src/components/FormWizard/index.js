import Wizard from './Wizard';
import Slide from './Slide';

export { Wizard };
export { Slide };

export default Wizard;
