import PropTypes from 'prop-types';

const Slide = ({ children }) => children;

Slide.defaultProps = {
    skip: false,
    disabled: false,
    validate: () => {},
};

Slide.propTypes = {
    children: PropTypes.node.isRequired,
    skip: PropTypes.bool,
    disabled: PropTypes.bool,
    validate: PropTypes.func,
};

export default Slide;
