import React from 'react';
import { mount } from 'enzyme';

import { Wizard, Slide } from '.';

describe('<FormWizard />', () => {
    const submit = jest.fn();

    const component = mount(
        <Wizard onSubmit={submit}>
            <Slide>Hello</Slide>
            <Slide>World</Slide>
        </Wizard>
    );

    it('renders', () => {
        expect(component).toMatchSnapshot();
    });

    it('changes page', () => {
        expect(component.text()).toContain('Hello');
        component
            .find('button')
            .at(1)
            .simulate('submit');
        expect(component.text()).toContain('World');
    });

    it('submits', () => {
        component
            .find('button')
            .at(1)
            .simulate('submit');
        component
            .find('button')
            .at(1)
            .simulate('submit');
        expect(submit).toHaveBeenCalled();
    });
});
