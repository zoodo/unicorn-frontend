import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import * as authenticationDucks from '../../ducks/authentication.duck';
import * as usersCombiner from '../../ducks/users.combiner';
import LoginBox from '../LoginBox';
import { ROLES, userHasRole } from '../../utils/roles';

const ProtectedRoute = ({ component: Component, requireAuth, requireUserlevel, redirect, ...rest }) => {
    const isAuthenticated = useSelector(state => authenticationDucks.tokenIsValid(state));

    const hasRole = useSelector(state => userHasRole(usersCombiner.getOwnUserObject(state), requireUserlevel));

    const canRender = useMemo(() => {
        if (requireAuth && !isAuthenticated) {
            return false;
        }

        if (requireUserlevel && !hasRole) {
            return false;
        }

        return true;
    }, [requireAuth, isAuthenticated, requireUserlevel, hasRole]);

    return (
        <Route
            {...rest}
            render={props =>
                canRender ? (
                    <Component {...props} />
                ) : redirect ? (
                    <Redirect
                        to={{
                            pathname: redirect,
                            state: { from: props.location },
                        }}
                    />
                ) : (
                    <LoginBox />
                )
            }
        />
    );
};

ProtectedRoute.defaultProps = {
    requireAuth: true,
};

ProtectedRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
    requireAuth: PropTypes.bool,
    requireUserlevel: PropTypes.oneOf(ROLES),
    redirect: PropTypes.string,
};

export default ProtectedRoute;
