import { connect } from 'react-redux';

import * as authenticationDuck from '../../ducks/authentication.duck';
import * as usersCombiner from '../../ducks/users.combiner';

import SideMenu from './SideMenu';

const mapStateToProps = state => {
    const user = usersCombiner.getOwnUserObject(state);

    return {
        authenticated: authenticationDuck.userIsLoggedin(state),
        isCrew: user && Number(user.role.value) === 1,
        user,
    };
};

const mapDispatchToProps = dispatch => ({
    fetchUsers: () => dispatch(usersCombiner.fetchOwnUser()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SideMenu);
