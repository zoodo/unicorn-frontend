import React from 'react';
import { withRouter, NavLink } from 'react-router-dom';
import { Menu, Icon } from 'semantic-ui-react';

/* const menu = [
    {
        label: <Icon icon="home" />,
        path: '/',
    },
    {
        label: 'Achievements',
        path: '/achievements',
    },
    {
        label: 'Competitions',
        path: '/competitions',
    },
    {
        label: 'Looking for Group',
        path: '/lfg',
    },
    {
        label: 'Competitions',
    },
    [
        {
            label: 'Overview',
            path: '/competitions',
        },
        {
            label: 'Create New',
            path: '/competitions/new',
        },
    ],
]; */

/*
const parseMenu = (menu, url) => {
    return (
        <MenuList>
            {menu.map(item => {
                if (Array.isArray(item)) {
                    return <li key={uuid()}>{parseMenu(item, url)}</li>;
                }

                if (!has(item, 'path')) {
                    return <MenuLabel key={item.label}>{item.label}</MenuLabel>;
                }

                if (Array.isArray(item) || !has(item, 'path')) {
                    return false;
                }

                return (
                    <Route
                        path={item.path}
                        exact
                        children={() => (
                            <MenuLink
                                key={item.path}
                                isActive={url === item.path}
                                render={props => (
                                    <li>
                                        <NavLink {...props} to={item.path}>
                                            {item.label}
                                        </NavLink>
                                    </li>
                                )}
                            />
                        )}
                    />
                );
            })}
        </MenuList>
    );
};
*/

class SideMenu extends React.Component {
    render() {
        // const ParsedMenu = parseMenu(menu, this.props.location.pathname);

        const { authenticated, mobile } = this.props;

        return (
            <>
                <Menu.Item as={NavLink} to="/" exact>
                    <Icon name="home" /> Home
                </Menu.Item>
                {/* {authenticated && (
                    <Menu.Item as={NavLink} to="/achievements">
                        <Icon name="trophy" /> Achievements
                    </Menu.Item>
                )} */}
                <Menu.Item as={NavLink} to="/competitions">
                    <Icon name="game" />
                    Competitions
                </Menu.Item>
                {authenticated && (
                    <Menu.Item as={NavLink} to="/vote">
                        <Icon name="heart" />
                        Vote
                    </Menu.Item>
                )}
                {/*     {authenticated && (
                    <Menu.Item as={NavLink} to="/lfg">
                        <Icon name="hand spock" />
                        Looking for Group
                    </Menu.Item>
                )} */}
                {/* {authenticated && (
                    <LoadingGate onStartLoading={fetchUsers} spinner={false}>
                        {isCrew && (
                            <>
                                <Menu.Item>
                                    <Menu.Header>Achievements</Menu.Header>

                                    <Menu.Menu>
                                        <Menu.Item as={NavLink} to="/admin/achievements" exact>
                                            Manage
                                        </Menu.Item>
                                    </Menu.Menu>

                                    <Menu.Menu>
                                        <Menu.Item as={NavLink} to="/admin/achievements/handout" exact>
                                            Hand out
                                        </Menu.Item>
                                    </Menu.Menu>
                                </Menu.Item>
                            </>
                        )}
                    </LoadingGate>
                )} */}
                {authenticated && mobile && (
                    <Menu.Item as={NavLink} to="/preferences">
                        Preferences
                    </Menu.Item>
                )}
            </>
        );
    }
}

export default withRouter(SideMenu);
