import React from 'react';
import renderer from 'react-test-renderer';

import DatePicker from './DatePicker';

it('renders', () => {
    const tree = renderer.create(<DatePicker date={new Date('2016')} />).toJSON();
    expect(tree).toMatchSnapshot();
});
