import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Popup } from 'semantic-ui-react';
import dayjs from 'dayjs';

import { isDate, getDateISO, getTime } from '../../utils/calendar';
import Calendar from './DatePicker';

class DateTimePicker extends React.Component {
    state = {
        date: null,
        time: '',
    };

    handleDateChange = date => {
        const { date: currentDate } = this.state;
        const newDate = date ? getDateISO(date) : null;

        currentDate !== newDate && this.setState({ date: newDate });
    };

    handleTimeChange = ({ target }) => {
        const { value } = target;

        this.setState({ time: value });
    };

    componentDidMount() {
        const { value: date } = this.props;
        const newDate = date && new Date(date);

        isDate(newDate) && this.setState({ date: getDateISO(newDate), time: getTime(newDate) });
    }

    componentDidUpdate(prevProps, prevState) {
        const { value: date, onChange } = this.props;
        const { value: prevDate } = prevProps;

        const pDate = new Date(date);
        const pPrevDate = new Date(prevDate);

        // Check if date is changed externally
        const dateISO = getDateISO(pDate);
        const prevDateISO = getDateISO(pPrevDate);
        const dateChanged = dateISO !== prevDateISO;
        // dateChanged && this.setState({ date: dateISO });

        // Check if time is changed externally
        const time = getTime(pDate);
        const prevTime = getTime(pPrevDate);
        const timeChanged = time !== prevTime;
        // timeChanged && this.setState({ time: time });

        if (dateChanged || timeChanged) {
            return this.setState({
                date: dateISO,
                time: time,
            });
        }

        // Only update parent if time is valid with both date and time
        if (this.state.date && this.validTime(this.state.time)) {
            const stringTime = this.state.date + ' ' + this.state.time;
            const stateChange = prevState.time !== this.state.time || prevState.date !== this.state.date;
            stateChange && isDate(new Date(stringTime)) && onChange(stringTime);
        }
    }

    validTime = time => /\d{2}:\d{2}/.test(time);

    render() {
        const { label, error } = this.props;
        const { date, time } = this.state;

        return (
            <>
                <Form.Field>
                    <label>{label}</label>
                </Form.Field>

                <Form.Field>
                    <Form.Input
                        action={
                            <Popup
                                trigger={
                                    <Button type="button">
                                        {date ? dayjs(date).format('DD / MM / YYYY') : 'DD / MM / YYYY'}
                                    </Button>
                                }
                                content={
                                    <Calendar date={date && new Date(date)} onDateChanged={this.handleDateChange} />
                                }
                                on="click"
                            />
                        }
                        actionPosition="left"
                        placeholder="HH:MM"
                        style={{ width: '85px' }}
                        value={time}
                        onChange={this.handleTimeChange}
                        error={error}
                    />
                </Form.Field>
            </>
        );
    }
}

DateTimePicker.defaultProps = {
    label: 'Enter Date',
    onChange: () => {},
    error: '',
};

DateTimePicker.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    error: PropTypes.string,
};

export default DateTimePicker;
