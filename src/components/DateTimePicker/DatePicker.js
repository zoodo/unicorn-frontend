// Based on this great article: https://blog.logrocket.com/react-datepicker-217b4aa840da
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import calendar, {
    isDate,
    isSameDay,
    isSameMonth,
    getDateISO,
    getNextMonth,
    getPreviousMonth,
    WEEK_DAYS,
    CALENDAR_MONTHS,
} from '../../utils/calendar';
import { Button, Grid, Icon } from 'semantic-ui-react';

class Calendar extends Component {
    state = { ...this.resolveStateFromProp(), today: new Date() };

    static resolveStateFromDate(date) {
        const isDateObject = isDate(date);
        const _date = isDateObject ? date : new Date();

        return {
            current: isDateObject ? date : null,
            month: _date.getMonth() + 1,
            year: _date.getFullYear(),
        };
    }

    componentDidMount() {
        const now = new Date();
        const tomorrow = new Date().setHours(0, 0, 0, 0) + 24 * 60 * 60 * 1000;
        const ms = tomorrow - now;

        this.dayTimeout = setTimeout(() => {
            this.setState({ today: new Date() }, this.clearDayTimeout);
        }, ms);
    }

    componentDidUpdate(prevProps, prevState) {
        const { date, onDateChanged } = this.props;
        const { date: prevDate } = prevProps;
        const dateMatch = date === prevDate || isSameDay(date, prevDate);

        const stateChanged = prevState.current !== this.state.current;

        !dateMatch &&
            this.setState(Calendar.resolveStateFromDate(date), () => {
                stateChanged && dateMatch && onDateChanged(date);
            });
    }

    clearDayTimeout = () => {
        this.dayTimeout && clearTimeout(this.dayTimeout);
    };

    componentWillUnmount() {
        this.clearPressurtimer();
        this.clearDayTimeout();
    }

    resolveStateFromProp() {
        return Calendar.resolveStateFromDate(this.props.date);
    }

    getCalendarDates = () => {
        const { current, month, year } = this.state;
        const calendarMonth = month || current.getMonth();
        const calendarYear = year || current.getFullYear();

        return calendar(calendarMonth, calendarYear);
    };

    gotoDate = date => e => {
        e && e.preventDefault();
        const { current } = this.state;
        const { onDateChanged } = this.props;

        !(current && isSameDay(date, current)) &&
            this.setState(Calendar.resolveStateFromDate(date), () => {
                typeof onDateChanged === 'function' && onDateChanged(date);
            });
    };

    gotoPreviousMonth = () => {
        const { month, year } = this.state;
        this.setState(getPreviousMonth(month, year));
    };

    gotoNextMonth = () => {
        const { month, year } = this.state;
        this.setState(getNextMonth(month, year));
    };

    gotoPreviousYear = () => {
        const { year } = this.state;
        this.setState({ year: year - 1 });
    };

    gotoNextYear = () => {
        const { year } = this.state;
        this.setState({ year: year + 1 });
    };

    handlePressure = fn => {
        if (typeof fn === 'function') {
            fn();
            this.pressureTimeout = setTimeout(() => {
                this.pressureTimer = setInterval(fn, 100);
            }, 500);
        }
    };

    clearPressurtimer = () => {
        this.pressureTimer && clearInterval(this.pressureTimer);
        this.pressureTimeout && clearTimeout(this.pressureTimeout);
    };

    handlePrevious = e => {
        e && e.preventDefault();
        this.handlePressure(this.gotoPreviousMonth());
    };

    handleNext = e => {
        e && e.preventDefault();
        this.handlePressure(this.gotoNextMonth());
    };

    handlePreviousYear = e => {
        e && e.preventDefault();
        this.handlePressure(this.gotoPreviousYear());
    };

    handleNextYear = e => {
        e && e.preventDefault();
        this.handlePressure(this.gotoNextYear());
    };

    render() {
        return (
            <div className="calendar">
                {this.renderMonthAndYear()}

                <div className="calendar-grid calendar-weekdays">
                    <Fragment>{Object.keys(WEEK_DAYS).map(this.renderDayLabel)}</Fragment>
                </div>

                <div className="calendar-grid">
                    <Fragment>{this.getCalendarDates().map(this.renderCalendarDate)}</Fragment>
                </div>
            </div>
        );
    }

    renderMonthAndYear = () => {
        const { month, year } = this.state;

        const monthName = Object.keys(CALENDAR_MONTHS)[Math.max(0, Math.min(month - 1, 11))];

        return (
            <Grid className="calendar-heading">
                <Grid.Row columns={3}>
                    <Grid.Column>
                        <Button onMouseDown={this.handlePreviousYear} onMouseUp={this.clearPressureTimer}>
                            <Icon name="chevron left" />
                        </Button>
                    </Grid.Column>
                    <Grid.Column textAlign="center">{year}</Grid.Column>
                    <Grid.Column>
                        <Button onMouseDown={this.handleNextYear} onMouseUp={this.clearPressureTimer} floated="right">
                            <Icon name="chevron right" />
                        </Button>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={3}>
                    <Grid.Column>
                        <Button onMouseDown={this.handlePrevious} onMouseUp={this.clearPressureTimer}>
                            <Icon name="chevron left" />
                        </Button>
                    </Grid.Column>
                    <Grid.Column textAlign="center">{monthName}</Grid.Column>
                    <Grid.Column>
                        <Button onMouseDown={this.handleNext} onMouseUp={this.clearPressureTimer} floated="right">
                            <Icon name="chevron right" />
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    };

    renderDayLabel = (day, index) => {
        const label = WEEK_DAYS[day].toUpperCase();

        return (
            <div key={index} index={index}>
                {label}
            </div>
        );
    };

    renderCalendarDate = (date, index) => {
        const { current, month, year, today } = this.state;
        const _date = new Date(date.join('-'));

        const isToday = isSameDay(_date, today);

        const isCurrent = current && isSameDay(_date, current);

        const inMonth = month && year && isSameMonth(_date, new Date([year, month, 1].join('-')));

        const onClick = this.gotoDate(_date);

        const props = { index, onClick, title: _date.toDateString() };

        const classNames = inMonth ? (isCurrent ? 'is-current' : isToday ? 'is-today' : '') : '';

        return (
            <div key={getDateISO(_date)} {...props} className={!inMonth ? 'is-othermonth' : ''}>
                <Button className={`date-item ${classNames}`}>{_date.getDate()}</Button>
            </div>
        );
    };
}

Calendar.defaultProps = {
    onDateChanged: () => {},
};

Calendar.propTypes = {
    date: PropTypes.instanceOf(Date),
    onDateChanged: PropTypes.func,
};

export default Calendar;
