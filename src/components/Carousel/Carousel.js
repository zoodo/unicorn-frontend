import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Segment, Label } from 'semantic-ui-react';
import { CSSTransition } from 'react-transition-group';
import useInterval from '../../utils/useInterval';

const Carousel = ({ children, interval, fade, activeColor, inactiveColor, pillWidth, backgroundColor }) => {
    const [index, setIndex] = useState(0);
    const [nextSlide, setNextSlide] = useState(null);
    const [delay, setDelay] = useState(null);
    const [changing, setChanging] = useState(true);
    const [hovering, setHovering] = useState(false);
    const childArr = React.Children.toArray(children);

    useEffect(() => {
        setDelay(interval);
    }, [interval]);

    useEffect(() => {
        if (React.Children.count(children) <= 1) {
            setDelay(null);
        }
    }, [children]);

    useInterval(() => {
        triggerNext();
    }, delay);

    // eslint-disable-next-line
    const previous = () => {
        if (index - 1 < 0) {
            return setIndex(childArr.length - 1);
        }

        return setIndex(index - 1);
    };

    const next = () => childArr.length && setIndex(nextId());
    const nextId = () => childArr.length && (index + 1) % childArr.length;

    const pause = () => setDelay(null);
    const unPause = () => setDelay(interval);

    const triggerNext = id => {
        if (hovering && id === undefined) {
            return;
        }

        pause();
        setChanging(false);
        if (id !== undefined) {
            setNextSlide(id);
        }
    };

    const onExit = () => {
        setChanging(true);
        if (nextSlide !== null) {
            setIndex(nextSlide);
            setNextSlide(null);
        } else {
            next();
        }
    };

    const onEnter = () => {
        unPause();
    };

    return (
        <Segment
            vertical
            onMouseEnter={() => setHovering(true)}
            onMouseLeave={() => setHovering(false)}
            className="carousel"
            style={{ background: backgroundColor, paddingBottom: 0, paddingTop: 0 }}
        >
            <CSSTransition
                in={changing}
                timeout={fade}
                classNames="carousel-slide"
                onExited={onExit}
                onEntered={onEnter}
            >
                <div className="carousel-slide" style={{ transition: `all ${fade}ms ease-in-out` }}>
                    {childArr[index]}
                </div>
            </CSSTransition>
            <div style={{ position: 'absolute', width: '100%', bottom: 0, zIndex: 2 }}>
                <Segment basic textAlign="center">
                    {React.Children.count(children) > 1 &&
                        React.Children.map(children, (child, i) => (
                            <Label
                                color={i === index ? activeColor : inactiveColor}
                                onClick={() => triggerNext(i)}
                                style={{ cursor: 'pointer', minWidth: pillWidth }}
                            />
                        ))}
                </Segment>
            </div>
        </Segment>
    );
};

Carousel.defaultProps = {
    inactiveColor: 'grey',
    activeColor: 'black',
    backgroundColor: 'white',
    interval: 10000,
    fade: 150,
    pillWidth: '3rem',
};

Carousel.propTypes = {
    children: PropTypes.node.isRequired,
    inactiveColor: PropTypes.string,
    activeColor: PropTypes.string,
    backgroundColor: PropTypes.string,
    interval: PropTypes.number,
    fade: PropTypes.number,
    pillWidth: PropTypes.string,
};

export default memo(Carousel);
