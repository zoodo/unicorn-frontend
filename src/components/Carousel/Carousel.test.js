import React from 'react';
import { shallow } from 'enzyme';
import { Label } from 'semantic-ui-react';
import Carousel from './Carousel';

const slides = ['Slide #1', 'Slide #2', 'Slide #3', 'Slide #4', 'Slide #5'];

describe('Carousel renders as expected', () => {
    const wrapper = shallow(
        <Carousel fade={1}>
            {slides.map(slide => (
                <h1 key={slide}>{slide}</h1>
            ))}
        </Carousel>
    );

    it('Render matches snapshot', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('Renders only first item', () => {
        expect(wrapper.find('.carousel-slide').text()).toEqual(slides[0]);
    });

    it('Renders only a single item', () => {
        expect(wrapper.find('.carousel-slide')).toHaveLength(1);
    });

    it('Is interactive', () => {
        wrapper
            .find(Label)
            .at(1)
            .simulate('click');
        expect(wrapper.find('.carousel-slide').text()).toEqual(slides[0]);
    });
});
