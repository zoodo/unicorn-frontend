import React from 'react';
import { Button, Grid, Header, Segment } from 'semantic-ui-react';
import { getOscarUrl } from '../../utils/apiFetch';

const LoginBox = () => (
    <Grid textAlign="center" style={{ minHeight: 'inherit' }} verticalAlign="middle">
        <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="teal" textAlign="center">
                Log in to your account
            </Header>
            <Segment>
                <Button as="a" color="teal" fluid size="large" href={getOscarUrl()}>
                    Click here to log in
                </Button>
            </Segment>
        </Grid.Column>
    </Grid>
);

export default React.memo(LoginBox);
