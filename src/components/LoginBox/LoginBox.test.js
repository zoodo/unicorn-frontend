import React from 'react';
import { shallow } from 'enzyme';

import LoginBox from './LoginBox';

describe('<LoginBox />', () => {
    it('renders', () => {
        const component = shallow(<LoginBox />);
        expect(component).toMatchSnapshot();
    });
});
