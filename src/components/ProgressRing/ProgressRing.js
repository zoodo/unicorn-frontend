import React from 'react';
import PropTypes from 'prop-types';

// https://codepen.io/bbrady/pen/ozrjKE/?editors=0110

const ProgressRing = ({ progress, size, strokeWidth }) => {
    // Size of the enclosing square
    const sqSize = size;
    // SVG centers the stroke width on the radius, subtract out so circle fits in square
    const radius = (size - strokeWidth) / 2;
    // Enclose cicle in a circumscribing square
    const viewBox = `0 0 ${sqSize} ${sqSize}`;
    // Arc length at 100% coverage is the circle circumference
    const dashArray = radius * Math.PI * 2;
    // Scale 100% coverage overlay with the actual percent
    const dashOffset = dashArray - (dashArray * progress) / 100;

    return (
        <svg width={size} height={size} viewBox={viewBox}>
            <circle
                className="progress-ring-background"
                cx={size / 2}
                cy={size / 2}
                r={radius}
                strokeWidth={`${strokeWidth}px`}
            />
            <circle
                className="progress-ring-progress"
                cx={size / 2}
                cy={size / 2}
                r={radius}
                strokeWidth={`${strokeWidth}px`}
                // Start progress marker at 12 O'Clock
                transform={`rotate(-90 ${size / 2} ${size / 2})`}
                style={{
                    strokeDasharray: dashArray,
                    strokeDashoffset: dashOffset,
                }}
            />
            <text className="progress-ring-text" x="50%" y="50%" dy=".3em" textAnchor="middle">
                {`${progress}%`}
            </text>
        </svg>
    );
};

ProgressRing.defaultProps = {
    size: 150,
    strokeWidth: 3,
    progress: 0,
};

ProgressRing.propTypes = {
    size: PropTypes.number,
    strokeWidth: PropTypes.number,
    prorgess: PropTypes.number,
};

export default ProgressRing;
