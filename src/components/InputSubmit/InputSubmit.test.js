import React from 'react';
import { mount } from 'enzyme';

import InputSubmit from '.';

describe('<InputSubmit />', () => {
    let component;
    const cancelClick = jest.fn();
    const saveClick = jest.fn();

    beforeEach(() => {
        component = mount(<InputSubmit initialValue={'Yolo, swag!'} onCancel={cancelClick} onSave={saveClick} />);
    });

    it('renders', () => {
        expect(component).toMatchSnapshot();
    });

    it('enables edit mode', () => {
        component.find('input').simulate('click');
        expect(component.find('button').length).toEqual(2);
    });

    it('saves', () => {
        component.find('input').simulate('click');
        component
            .find('button')
            .at(0)
            .simulate('click');
        expect(saveClick).toHaveBeenCalled();
    });

    it('saves with data', () => {
        component.find('input').simulate('click');
        component.find('input').simulate('change', { target: { value: 'Hello, world!' } });
        component
            .find('button')
            .at(0)
            .simulate('click');
        expect(saveClick).toHaveBeenCalledWith('Hello, world!');
    });

    it('cancels', () => {
        component.find('input').simulate('click');
        component
            .find('button')
            .at(1)
            .simulate('click');
        expect(cancelClick).toHaveBeenCalled();
    });
});
