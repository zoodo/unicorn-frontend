import React, { useState, useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon } from 'semantic-ui-react';

const InputSubmit = ({ onSave, onCancel, placeholder, initialValue, label, ...props }) => {
    const [editMode, setEdit] = useState(false);
    const [input, setInput] = useState('');

    useEffect(() => {
        setInput(initialValue);
    }, [initialValue]);

    const cancelClick = () => {
        setEdit(false);
        setInput(initialValue);
        onCancel();
    };

    const saveClick = () => {
        setEdit(false);
        onSave(input);
    };

    return (
        <Form.Input
            label={label}
            value={input}
            onChange={e => setInput(e.target.value)}
            onClick={() => setEdit(true)}
            action={editMode}
            fluid
            placeholder={placeholder || 'Edit'}
            {...props}
        >
            <input />
            {editMode && (
                <>
                    <Button icon color="green" onClick={saveClick}>
                        <Icon name="check" />
                    </Button>
                    <Button icon color="red" onClick={cancelClick}>
                        <Icon name="close" />
                    </Button>
                </>
            )}
        </Form.Input>
    );
};

InputSubmit.defaultProps = {
    onSave: () => null,
    onCancel: () => null,
    placeholder: '',
    initialValue: '',
    label: '',
};

InputSubmit.propTypes = {
    onSave: PropTypes.func,
    onCancel: PropTypes.func,
    placeholder: PropTypes.string,
    initialValue: PropTypes.string,
    label: PropTypes.string,
};

export default memo(InputSubmit);
