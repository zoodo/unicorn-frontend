import React from 'react';
import { Segment, Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const Uuid4Detector = ({ shouldChange }) => {
    if (!shouldChange) {
        return null;
    }

    return (
        <Segment basic>
            <Message negative>
                Your nick seems to be auto generated. Click <Link to="/preferences">here</Link> to set your nick
            </Message>
        </Segment>
    );
};

export default Uuid4Detector;
