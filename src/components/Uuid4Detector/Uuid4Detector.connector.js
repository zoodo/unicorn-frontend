import { connect } from 'react-redux';

import Uuid4Connector from './Uuid4Detector';

import * as usersCombiner from '../../ducks/users.combiner';

const mapStateToProps = state => {
    const user = usersCombiner.getOwnUserObject(state);
    const re = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{6}$/i;

    return {
        shouldChange: user && re.test(user.nick),
    };
};

export default connect(mapStateToProps)(Uuid4Connector);
