import React from 'react';
import { Header, Segment } from 'semantic-ui-react';

const EntryDetails = ({ entry }) => (
    <Segment>
        <Header>{entry.title}</Header>

        <p>Message to big screen: {entry.screen_msg || <span className="has-text-grey-light">Not set</span>}</p>
        <p>Message to crew: {entry.crew_msg || <span className="has-text-grey-light">Not set</span>}</p>
    </Segment>
);

export default EntryDetails;
