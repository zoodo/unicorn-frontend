import React from 'react';
import { shallow } from 'enzyme';
import EntryDetails from '.';

describe('<EntryDetails />', () => {
    it('renders', () => {
        const component = shallow(
            <EntryDetails
                entry={{
                    title: 'This is merely a test',
                    screen_msg: 'Hello, mom!',
                    crew_msg: 'Crew rules!',
                }}
            />
        );
        expect(component).toMatchSnapshot();
    });

    it('handles missing messages', () => {
        const component = shallow(
            <EntryDetails
                entry={{
                    title: 'This is merely a test',
                }}
            />
        );
        expect(component).toMatchSnapshot();
    });
});
