import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Card } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { parseError } from '../utils/error';
import { unicornFetch } from '../utils/apiFetch';
import View from '../components/View';
import LoadingGate from '../components/LoadingGate';
import { fetchEntriesForCompetition, getEntries, isFetching } from '../ducks/entries.duck';
import VoteCard from '../features/vote/VoteCard';

const VoteOnCompetitionView = () => {
    const dispatch = useDispatch();

    const [votes, setVotes] = useState([]);
    const [isFetchingVotes, setIsFetchingVotes] = useState(false);
    const { id } = useParams();
    const isFetchingEntries = useSelector(state => isFetching(state));
    const entries = useSelector(state => Object.values(getEntries(state, id)));

    useEffect(() => {
        setIsFetchingVotes(true);

        unicornFetch({ url: 'competitions/votes' })
            .then(data => {
                setVotes(data.results);
                setIsFetchingVotes(false);
            })
            .catch(err => {
                setIsFetchingVotes(false);
                parseError(err).map(e => toast.error(e));
                console.log(err);
            });
    }, []);

    return (
        <View icon="heart" header="Cast your votes!">
            <LoadingGate
                as={Card.Group}
                onStartLoading={() => dispatch(fetchEntriesForCompetition(id, true))}
                isLoading={isFetchingEntries || isFetchingVotes}
            >
                {entries
                    .filter(entry => {
                        if (entry.obj_type !== 'full') {
                            return true;
                        }

                        if (entry.status.value === 4) {
                            return true;
                        }

                        return false;
                    })
                    .map(entry => (
                        <VoteCard key={entry.id} entry={entry} vote={votes.find(v => v.entry === entry.id)} />
                    ))}
            </LoadingGate>
        </View>
    );
};

export default VoteOnCompetitionView;
