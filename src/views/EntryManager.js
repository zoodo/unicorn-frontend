import React from 'react';
import { Divider, Header, Icon } from 'semantic-ui-react';
import { useParams, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import View from '../components/View';
import EntryManager from '../features/entries/EntryManager';
import RegisterEntry from '../features/entries/RegisterEntry';
import ContributorEditor from '../features/entries/ContributorEditor';
import UploadManager from '../features/entries/UploadManager';
import ContributorInfo from '../features/entries/ContributorInfo';
import ToornamentMatches from '../features/toornament/ToornamentMatches';
import { hasTeams, hasFileupload, hasToornament } from '../utils/competitions';
import * as competitionsDuck from '../ducks/competitions.duck';
import * as entriesDuck from '../ducks/entries.duck';

const ManageEntry = () => {
    const { id, entryId } = useParams();

    const competition = useSelector(state => competitionsDuck.getCompetition(state, id));
    const entry = useSelector(state => entriesDuck.getEntry(state, entryId));

    return (
        <View icon="picture" header="View and edit entry">
            <EntryManager />
            <Header>General</Header>
            <RegisterEntry />
            {competition && hasTeams(competition) && (
                <>
                    <Divider /> <ContributorEditor />
                </>
            )}
            {competition && hasFileupload(competition) && (
                <>
                    <Divider /> <UploadManager />
                </>
            )}
            <Divider /> <ContributorInfo entry={entry} />
            {competition && hasToornament(competition) && entry && (
                <>
                    <Divider /> <ToornamentMatches entryId={entry.id} />
                </>
            )}
            <Divider hidden />
            {competition && (
                <Link to={`/competitions/${competition.id}/admin`}>
                    <Icon name="arrow left" />
                    Back to {competition.name}
                </Link>
            )}
        </View>
    );
};

export default ManageEntry;
