import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import get from 'lodash/get';
import queryString from 'query-string';
import { withRouter } from 'react-router-dom';
import * as authenticationDucks from '../ducks/authentication.duck';
import LoginBox from '../components/LoginBox';

const Login = ({ location }) => {
    const dispatch = useDispatch();

    const query = queryString.parse(location.search);
    const willAuthenticate = get(query, 'code', false);
    const queryState = JSON.parse(get(query, 'state', '{}'));

    const login = useCallback(() => {
        dispatch(authenticationDucks.login(willAuthenticate, queryState));
    }, [dispatch, queryState, willAuthenticate]);

    useEffect(() => {
        if (willAuthenticate) {
            login();
        }
    }, [login, willAuthenticate]);

    const tokenIsValid = useSelector(state => authenticationDucks.tokenIsValid(state));

    return tokenIsValid ? null : <LoginBox />;
};

export default withRouter(Login);
