import React from 'react';
import View from '../components/View';
import DisplayNameFormat from '../features/user/DisplayNameFormat';

const ProfilePage = () => (
    <View icon="options" header="My preferences">
        <DisplayNameFormat />
    </View>
);

export default ProfilePage;
