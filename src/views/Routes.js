import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProtectedRoute from '../components/ProtectedRoute';
import { ROLE_CREW } from '../utils/roles';

import Landing from './Landing';
import Preferences from './Preferences';
import Login from './Login';

import { ParticipantAchievementPage } from '../features/achievements/ParticipantAchievementList';
import { HandoutPage } from '../features/achievements/Handout';
import { AchievementOverviewPage, AchievementManagePage } from '../features/achievements/AchievementManagerPage';

import CompetitionsOverview from './CompetitionsOverview';
import CompetitionDetails from '../features/competitions/CompetitionDetails';
import CompetitionsEntriesOverview from './CompetitionsEntriesOverview';
import EditCompetition from '../features/competitions/EditCompetition';
import ScoreOverview from '../features/competitions/ScoreOverview';
import { RegisterEntryPage } from '../features/entries/RegisterEntry';
import CompetitionGeneralRules from './CompetitionsGeneralRules';
import CompetitionsAdmin from './CompetitionsAdmin';

import EditRegistration from '../features/entries/EditRegistration';
import EntryManager from './EntryManager';
import OrderEntries from '../features/entries/OrderEntries';

import { VotePageRoot } from '../features/vote/VotePage';
import VoteOnCompetition from './VoteOnCompetitionView';
import FourZeroFour from './404';

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/" component={Landing} />
            <Route exact path="/login" component={Login} />
            <ProtectedRoute exact path="/preferences" component={Preferences} />
            <Route exact path="/competitions" component={CompetitionsOverview} />
            <Route exact path="/competitions/general_rules" component={CompetitionGeneralRules} />
            <ProtectedRoute requireUserlevel={ROLE_CREW} exact path="/competitions/new" component={CompetitionsAdmin} />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/competitions/:id/scores"
                component={ScoreOverview}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/competitions/:id/order"
                component={OrderEntries}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/competitions/:id/entries"
                component={CompetitionsEntriesOverview}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/competitions/:id/admin"
                component={CompetitionsAdmin}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/competitions/:id/edit"
                component={EditCompetition}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/competitions/:id/entries/:entryId"
                component={EntryManager}
            />
            <ProtectedRoute exact path="/competitions/:id/register" component={RegisterEntryPage} />
            <ProtectedRoute exact path="/competitions/:id/register/:entryId" component={EditRegistration} />
            <Route path="/competitions/:id" component={CompetitionDetails} />
            <ProtectedRoute exact path="/achievements" component={ParticipantAchievementPage} />
            <ProtectedRoute exact path="/vote" component={VotePageRoot} />
            <ProtectedRoute exact path="/vote/:id" component={VoteOnCompetition} />
            <Route exact path="/admin/achievements" component={AchievementOverviewPage} />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/admin/achievements/new"
                component={AchievementManagePage}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/admin/achievements/handout"
                component={HandoutPage}
            />
            <ProtectedRoute
                requireUserlevel={ROLE_CREW}
                exact
                path="/admin/achievements/:id"
                component={AchievementManagePage}
            />
            <Route path="/" component={FourZeroFour} />
        </Switch>
    );
};

export default Routes;
