import React from 'react';
import { Grid, Header } from 'semantic-ui-react';

const FourZeroFour = () => (
    <Grid textAlign="center" style={{ minHeight: 'inherit' }} verticalAlign="middle">
        <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h1">
                <Header.Content>Oh noes! Page not found.</Header.Content>
                <Header.Subheader>◔̯◔</Header.Subheader>
            </Header>
        </Grid.Column>
    </Grid>
);

export default FourZeroFour;
