import React from 'react';

import CompetitionsOverview from '../views/CompetitionsOverview';
import Uuid4Detector from '../components/Uuid4Detector';
const Landing = () => {
    // const user = useSelector(state => userCombiner.getOwnUserObject(state));
    // const isCrew = useSelector(state => (user && userDuck.userIsCrew(state, user.id)) || false);
    // const team = (user && user.team) || false;

    return (
        <>
            <Uuid4Detector />
            <CompetitionsOverview />
            {/* <Modal basic size="mini" open={user && isCrew && !team}>
                <Header icon="user secret" content="Choose team" />
                <Modal.Content>
                    <SelectTeam />
                </Modal.Content>
            </Modal> */}
        </>
    );
};

export default Landing;
