import React from 'react';
import { useParams } from 'react-router-dom';
import NewCompetition from '../features/competitions/NewCompetition';
import CompetitionDashboard from '../features/competitions/CompetitionDashboard';
import View from '../components/View';

const CompetitionsAdmin = () => {
    const isEditing = useParams().id;

    return isEditing ? (
        <View>
            <CompetitionDashboard />
        </View>
    ) : (
        <View icon="game" header="Create new competition">
            <NewCompetition />
        </View>
    );
};

export default CompetitionsAdmin;
