import React, { useState, memo } from 'react';
import { Modal, Header, Button, Icon } from 'semantic-ui-react';
import View from '../components/View';

const CompetitionGeneralRules = () => {
    const [visibleModal, setModal] = useState(false);

    const hideModal = () => setModal(false);
    const showModal = () => setModal(true);

    return (
        <View header="Generelle konkurranseregler for The Gathering" icon="balance scale">
            <Modal
                trigger={<Button onClick={showModal}>Definisjoner</Button>}
                open={visibleModal}
                onClose={hideModal}
                basic
                size="small"
            >
                <Header icon="browser" content="Definisjoner" />
                <Modal.Content>
                    <p>
                        A) Materiale betyr materiale underlagt Opphavsrett og Lignende Rettigheter som er avledet fra
                        eller basert på det Lisensierte Materialet og hvor det Lisensierte Materialet er oversatt,
                        forandret, arrangert, transformert eller på annen måte endret på en slik måte som krever
                        tillatelse under Opphavsrett og Lignende Rettigheter tilhørende Lisensgiveren. I de tilfellene
                        det Lisensierte Materialet er et musikkverk, en musikkframføring, eller et lydopptak, er
                        bearbeidet materiale alltid produsert etter denne offentlige lisensen når det lisensierte
                        materialet er synkronisert i tidsbestemt forhold til et bevegelig bilde.
                    </p>
                    <p>
                        B) Opphavsrett og Lignende rettigheter betyr opphavsrettigheter og/eller lignende
                        opphavsrettigheter, inklusive, men ikke begrenset til, framføring, kringkasting, lydopptak, og
                        databaserettigheter. Uten hensyn til hvordan rettighetene er merket eller kategorisert.{' '}
                    </p>
                    <p>
                        C) Dele/deling/delte betyr å gjøre materialet allment tilgjengelig ved hjelp av hvilket som
                        helst middel eller metode som krever tillatelse under de Lisensierte Rettigheter, slik som
                        reproduksjon, offentlig framvisning, offentlig framføring, distribusjon, formidling,
                        kommunikasjon, eller innførsel, og å gjøre materialet tilgjengelig for allmennheten på en slik
                        måte at medlemmer av allmennheten selv kan velge sted og tid for tilgang til materialet.
                    </p>
                    <p>
                        D) Du betyr det individ, eller den juridiske person som utøver de Lisensierte Rettigheter under
                        denne Offentlige Lisens. Din og Ditt har en tilsvarende mening.
                    </p>
                    <p>
                        E) Arrangør er herunder definert til å være The Gathering’s arrangør, KANDU (Kreativ Aktiv Norsk
                        DataUngdom).
                    </p>{' '}
                </Modal.Content>
                <Modal.Actions>
                    <Button color="green" onClick={hideModal} inverted>
                        <Icon name="checkmark" />
                    </Button>
                </Modal.Actions>
            </Modal>

            <h3>Rettighet til å delta</h3>
            <p>
                For å delta må du som deltager ha en (1) gyldig billett i{' '}
                <a href="https://geekevents.org">Geekevents</a>. Crew må være registrert som aktivt crew under årets
                arrangement. Personer som er tilknyttet arrangementet som sponsor må ha gyldig billett i{' '}
                <a href="https://geekevents.org">Geekevents</a>. Med gyldig billett kan du delta i både kreative- og
                spillkonkurranser.
            </p>

            <h3>Hvordan deltar jeg?</h3>
            <p>
                <a href="https://competitions.gathering.org/login">Logg deg på</a> med din{' '}
                <a href="https://geekevents.org">Geekevents</a>-konto (dersom du er crew, bruk din{' '}
                <a href="https://wannabe.gathering.org">Wannabe</a> konto). Vennligst les regelsettet for den spesifikke
                konkurransen du tenker å delta i før du melder deg på eller leverer et bidrag.
            </p>
            <p>
                Lag/grupper skal dannes før du melder deg på en konkurranse. En lagkaptein/leder bør også velges før du
                går inn i en konkurranse hvor det kreves at du er en del av et lag.
            </p>

            <h3>Hvordan vinner jeg?</h3>
            <p>
                Du kan vinne en konkurranse ved å slå motstanderne i en turnering/bracket eller gjennom stemmer
                ("popular vote", jury stemmer eller en kombinasjon av disse). Hver konkurranse har sine egne regler
                angående dette, så vær sikker på at du er kjent med regelsettet før du deltar i konkurransen.
            </p>
            <p>
                Konkurranseresultater vil bli annonsert på <a href="https://gathering.org">gathering.org</a> for hver
                konkurranse.
            </p>
            <p>
                For å motta en premie må du delta på premieutdelingen. Premieutdelingen vil bli annonsert i den
                offisielle kalenderen på <a href="https://gathering.org">gathering.org</a>.
            </p>

            <h3>Innhold</h3>
            <p>Alt innsendt materiale du laster opp skal forholde seg til Norsk Lov.</p>
            <p>Bryter det Norsk Lov vil du bli diskvalifisert.</p>
            <p>
                Ved opplasting av materiale til denne nettsiden, så bekrefter du at materialet er ditt/deres, og du/dere
                har de fulle, eller deler av rettighetene som kreves til å distribuere opplastet materiale.
            </p>
            <p>
                Deltakeren i den aktuelle konkurransen beholder selv opphavsrett og lignende rettigheter til det
                innsendte materiale. Arrangøren forbeholder seg retten til å benytte seg av alt materiale som er lastet
                opp for å markedsføre egne arrangement, med mindre annet står spesifisert i den aktuelle konkurransen
                sine regler.
            </p>
            <p>
                Opplasteren/Deltageren kan ved henvendelse til arrangør be om at deling av materiale oppheves etter 14
                virkedager fra arrangør har mottatt henvendelsen. Dette vil ikke ha tilbakevirkende kraft på allerede
                delt materiale.
            </p>

            <h3>Klager</h3>
            <p>
                Alle klager må meldes inn formelt og skriftlig til konkurranseansvarlig innen 30 minutter etter at
                hendelsen inntraff eller maksimalt 30 minutter etter kampen/runden er avsluttet. Hvis en skriftlig klage
                ikke er mulig (det vil si strømbrudd etc), bør en verbal melding rettes til konkurranseansvarlig (en
                verbal klage er ikke gyldig før det er et håndskrevet notat signert av konkurrent og en ansvarlig).
            </p>

            <h3>Regeloppdateringer</h3>
            <p>
                Det er deltakerens ansvar å sørge for alle regler blir lest og forstått, samt at man opptrer i henhold
                til de siste publiserte reglene. Unnlatelse av å lese og holde seg oppdatert på reglene er ikke grunnlag
                for en klage.
            </p>
            <p>
                Oppdatert regelsett finner du til enhver tid på konkurransens side i{' '}
                <a href="https://unicorn.gathering.org">konkurransesystemet</a>. The Gatherings crew vil ikke
                nødvendigvis annonsere endringer i reglene, og det er derfor deltakerens eget ansvar å holde seg
                oppdatert på eventuelle endringer.
            </p>
            <p>
                <strong>
                    Vær oppmerksom på at det er deltakerens ansvar å sørge for at de er i stand til å spille sine kamper
                    på riktig tidspunkt eller leverer sine bidrag innen fristen. Konkurranser kan kjøres samtidig, og
                    The Gatherings crew kan diskvalifisere en deltaker, et team eller en gruppe som ikke dukker opp
                    eller sender sitt bidrag innenfor fristen.
                </strong>
            </p>

            <h3>Minimum deltakere/konkurrenter</h3>
            <p>
                Individuelle konkurranser kan ha spesifikke minimumskrav til antall deltakere som kreves for å holde
                konkurransen. Vennligst sjekk reglene til konkurransen du ønsker å delta i for spesifikk informasjon
                angående dette.
            </p>
            <p>
                Hver deltaker kan delta i så mange konkurranser som hen vil, men bare én gang per konkurranse. Det betyr
                at du ikke kan delta med to forskjellige bidrag eller være en del av to forskjellige lag i en og samme
                konkurranse.
            </p>

            <h3>Tilgjengelighet</h3>
            <p>
                Hver deltaker må sørge å være tilgjengelig for de konkurranseansvarlige på viktige tidspunkt i løpet av
                konkurransen. Dette kan være nær en kommende kamp i en turnering/bracket (30 minutter før og en time
                etter) eller under godkjenningsperioden for innleverte bidrag. Kommunikasjon vil enten være gjennom
                konkurransesystemets chat eller på deltakerens registrerte e-postadresse.
            </p>

            <h3>Advarsler</h3>
            <p>En deltaker kan få en offisiell advarsel for følgende:</p>
            <ul>
                <li>Nekte å følge instruksene fra en konkurranseansvarlig.</li>
                <li>Bruke ufint språk og/eller utvise dårlig oppførsel.</li>
            </ul>

            <h3>Diskvalifikasjon</h3>
            <p>
                The Gathering forbeholder seg retten til å diskvalifisere deltakere, lag eller grupper. En deltaker kan
                umiddelbart diskvalifiseres for følgende:
            </p>
            <ul>
                <li>
                    Enhver form for trusler, mobbing eller vold (fysisk eller over internett) mot personer som deltar på
                    eller er tilknyttet arrangementet i noen måte.
                </li>
                <li>
                    Fusk ved anvendelse av urimelig taktikk (avbrytelse av motstander, fysisk blokkering, exploits,
                    juksekoder).
                </li>
                <li>
                    DDOS-angrep, tukling med utstyr, eller på annen måte motta hjelp fra noen utenfor det registrerte
                    laget eller gruppen.
                </li>
                <li>Oppgi misvisende informasjon eller bevisst lyve til en dommer/konkurranseansvarlig.</li>
            </ul>

            <p>
                <strong>Særregler for diskvalifikasjon fra spillkonkurransene:</strong>
            </p>
            <ul>
                <li>Utnyttelse eventuelle av feil som tydelig endrer spillet eller programmets funksjonalitet.</li>
                <li>
                    Alle kamper må spilles fra din deltagerplass eller plass oppsatt av The Gathering. Det er ikke mulig
                    å delta i e-sport turneringene fra utsiden av The Gathering.
                </li>
            </ul>

            <p>
                <strong>Særregler for diskvalifikasjon fra Creative-konkurransene:</strong>
            </p>
            <ul>
                <li>Ulovlig, støtende eller ubehagelig innhold.</li>
                <li>Opphavsrettsbeskyttet materiale brukt uten uttrykkelig tillatelse fra rettighetshaverne.</li>
                <li>Innlegg som ikke viser en betydelig mengde kreativitet eller innsats.</li>
            </ul>

            <h3>Definisjoner</h3>
            <p>
                A) Materiale betyr materiale underlagt Opphavsrett og Lignende Rettigheter som er avledet fra eller
                basert på det Lisensierte Materialet og hvor det Lisensierte Materialet er oversatt, forandret,
                arrangert, transformert eller på annen måte endret på en slik måte som krever tillatelse under
                Opphavsrett og Lignende Rettigheter tilhørende Lisensgiveren. I de tilfellene det Lisensierte Materialet
                er et musikkverk, en musikkframføring, eller et lydopptak, er bearbeidet materiale alltid produsert
                etter denne offentlige lisensen når det lisensierte materialet er synkronisert i tidsbestemt forhold til
                et bevegelig bilde.
            </p>
            <p>
                B) Opphavsrett og Lignende rettigheter betyr opphavsrettigheter og/eller lignende opphavsrettigheter,
                inklusive, men ikke begrenset til, framføring, kringkasting, lydopptak, og databaserettigheter. Uten
                hensyn til hvordan rettighetene er merket eller kategorisert.{' '}
            </p>
            <p>
                C) Dele/deling/delte betyr å gjøre materialet allment tilgjengelig ved hjelp av hvilket som helst middel
                eller metode som krever tillatelse under de Lisensierte Rettigheter, slik som reproduksjon, offentlig
                framvisning, offentlig framføring, distribusjon, formidling, kommunikasjon, eller innførsel, og å gjøre
                materialet tilgjengelig for allmennheten på en slik måte at medlemmer av allmennheten selv kan velge
                sted og tid for tilgang til materialet.
            </p>
            <p>
                D) Du betyr det individ, eller den juridiske person som utøver de Lisensierte Rettigheter under denne
                Offentlige Lisens. Din og Ditt har en tilsvarende mening.
            </p>
            <p>
                E) Arrangør er herunder definert til å være The Gathering’s arrangør, KANDU (Kreativ Aktiv Norsk
                DataUngdom).
            </p>

            <p>
                <span class="text-danger">
                    <strong>Sist oppdatert:</strong> 23.03.2018
                </span>
            </p>
        </View>
    );
};

export default memo(CompetitionGeneralRules);
