import React, { useEffect, useMemo, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Menu, Segment, Form, Button } from 'semantic-ui-react';
import { HashRouter, Link, NavLink, useLocation, Redirect } from 'react-router-dom';
import { CompetitionListComponent as CompetitionList } from '../features/competitions/CompetitionList';
import FeaturedCompetitions from '../features/competitions/FeaturedCompetitions';
import { groupCompetitionsByKey, sortByRunTimeStart } from '../utils/competitions';
import * as competitionsDuck from '../ducks/competitions.duck';
import * as genreDuck from '../ducks/genres.duck';
import { userIsCrew } from '../ducks/users.duck';
import { getUserId } from '../ducks/authentication.duck';

const FILTER_ONGOING = '#/ongoing-competitions';
const FILTER_PAST = '#/past-competitions';
const FILTER_ARR = [FILTER_ONGOING, FILTER_PAST];

const CompetitionsOverview = () => {
    const dispatch = useDispatch();
    const { hash } = useLocation();

    const [activeFilter, setActiveFilter] = useState(FILTER_ONGOING);
    const [search, setSearch] = useState('');
    const [genre, setGenre] = useState('');

    const fetchCompetitions = useCallback(() => dispatch(competitionsDuck.fetchCompetitions()), [dispatch]);
    const fetchGenres = useCallback(() => dispatch(genreDuck.fetchGenres()), [dispatch]);

    const competitions = useSelector(state => competitionsDuck.getCompetitions(state));
    const genres = useSelector(state => genreDuck.getGenres(state));
    const isAdmin = useSelector(state => userIsCrew(state, getUserId(state)));

    useEffect(() => {
        if (FILTER_ARR.includes(hash)) {
            setActiveFilter(hash);
        }
    }, [hash]);

    useEffect(() => {
        fetchCompetitions();
    }, [fetchCompetitions]);

    useEffect(() => {
        fetchGenres();
    }, [fetchGenres]);

    const competitionsByState = useMemo(
        () =>
            competitions.filter(c => {
                if (activeFilter === FILTER_ONGOING) {
                    return c.state.value !== 256;
                } else if (activeFilter === FILTER_PAST) {
                    return c.state.value === 256;
                } else {
                    return true;
                }
            }),
        [competitions, activeFilter]
    );

    const filteredCompetitions = useMemo(
        () =>
            competitionsByState
                .filter(c => c.name.toLowerCase().includes(search.toLowerCase()))
                .filter(c => (!genre ? true : genre === c.genre.id)),
        [competitionsByState, search, genre]
    );

    const groupedCompetitions = useMemo(
        () => groupCompetitionsByKey(filteredCompetitions.sort(sortByRunTimeStart).reverse()),
        [filteredCompetitions]
    );

    const featuredCompetitions = useMemo(() => competitions.filter(c => c.featured), [competitions]);

    const options = useMemo(() => genres.map(genre => ({ text: genre.name, value: genre.id })), [genres]);

    const resetFilters = () => {
        setSearch('');
        setGenre('');
    };

    return (
        <>
            <FeaturedCompetitions competitions={featuredCompetitions} />
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        <Menu pointing secondary>
                            <HashRouter>
                                {!FILTER_ARR.includes(hash) && <Redirect to="/ongoing-competitions" />}
                                <Menu.Item as={NavLink} to="/ongoing-competitions">
                                    Upcoming & Ongoing
                                </Menu.Item>
                                <Menu.Item as={NavLink} to="/past-competitions">
                                    Past competitions
                                </Menu.Item>
                            </HashRouter>
                        </Menu>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Segment basic>
                            <Form>
                                <Form.Group>
                                    <Form.Input
                                        placeholder="Search..."
                                        value={search}
                                        onChange={(e, { value }) => setSearch(value)}
                                        icon="search"
                                    />
                                    <Form.Dropdown
                                        clearable
                                        selection
                                        placeholder="Genre"
                                        disabled={!genres.length}
                                        options={options}
                                        value={genre}
                                        onChange={(e, { value }) => setGenre(value)}
                                    />
                                    <Form.Button circular basic onClick={resetFilters} disabled={!(genre || search)}>
                                        RESET FILTERS
                                    </Form.Button>
                                    {isAdmin && (
                                        <div style={{ flex: 'auto' }}>
                                            <Button
                                                circular
                                                inverted
                                                color="green"
                                                floated="right"
                                                as={Link}
                                                to="/competitions/new"
                                            >
                                                NEW COMPETITION
                                            </Button>
                                        </div>
                                    )}
                                </Form.Group>
                            </Form>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Segment basic>
                            <CompetitionList
                                competitions={
                                    activeFilter === FILTER_ONGOING ? filteredCompetitions : groupedCompetitions
                                }
                                isAdmin={isAdmin}
                            />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </>
    );
};

export default CompetitionsOverview;
