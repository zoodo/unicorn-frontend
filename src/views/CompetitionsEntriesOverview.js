import React from 'react';
import EntryTable from '../features/entries/EntryTable';
import View from '../components/View';

const CompetitionsEntriesOverview = () => {
    return (
        <View header="Entries">
            <EntryTable />
        </View>
    );
};

export default CompetitionsEntriesOverview;
