import { random, lorem, name, internet, phone, image } from 'faker';

const entryMock = {
    id: random.number(),
    title: random.word(),
    competition: {
        obj_type: 'nested',
        id: random.number(),
        name: random.words(),
        brief_description: lorem.sentence(),
        state: 256,
    },
    status: {
        value: 4,
        label: 'Qualified',
    },
    crew_msg: random.words(),
    screen_msg: random.words(),
    comment: null,
    contributors: [
        {
            obj_type: 'nested',
            id: random.number(),
            user: {
                obj_type: 'nested',
                id: random.number(),
                display_name: name.findName(),
                email: internet.email(),
                phone_number: phone.phoneNumber(),
                row: random.number(),
                seat: random.number(),
            },
            extra_info: null,
            is_owner: true,
        },
    ],
    files: [
        {
            obj_type: 'full',
            id: random.number(),
            url: image.image(),
            name: random.word() + '.png',
            type: 'entry',
            status: 1,
            active: true,
        },
        {
            obj_type: 'full',
            id: random.number(),
            url: image.image(),
            name: random.word() + '.png',
            type: 'progress1',
            status: 1,
            active: true,
        },
        {
            obj_type: 'full',
            id: random.number(),
            url: image.image(),
            name: random.word() + '.png',
            type: 'progress2',
            status: 1,
            active: true,
        },
        {
            obj_type: 'full',
            id: random.number(),
            url: image.image(),
            name: random.word() + '.png',
            type: 'progress3',
            status: 1,
            active: true,
        },
    ],
    is_contributor: false,
    is_owner: null,
    owner: {
        id: random.number(),
        display_name: name.findName(),
    },
    order: 1,
    score: random.number(),
};

export default entryMock;
