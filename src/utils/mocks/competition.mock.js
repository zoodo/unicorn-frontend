import { random, lorem, image, name } from 'faker';
import { genreMock } from '.';

const competitionMock = {
    id: random.number(),
    genre: {
        ...genreMock,
        obj_type: 'nested',
    },
    name: random.words(),
    brief_description: lorem.sentence(),
    description: '<p>' + lorem.sentence() + '</p>',
    rules: '<p>' + lorem.sentence() + '</p>',
    prizes: [random.number().toString(), random.number().toString(), random.number().toString()],
    fileupload: [
        {
            id: random.uuid(),
            file: 'picture',
            type: 'entry',
            input: 'Main entry',
        },
        {
            id: random.uuid(),
            file: 'picture',
            type: 'progress1',
            input: 'In-progress 1',
        },
        {
            id: random.uuid(),
            file: 'picture',
            type: 'progress2',
            input: 'In-progress 2',
        },
        {
            id: random.uuid(),
            file: 'picture',
            type: 'progress3',
            input: 'In-progress 3',
        },
    ],
    participant_limit: 0,
    toornament: null,
    published: true,
    report_win_loss: false,
    rsvp: false,
    header_image: image.image(),
    header_credit: name.findName(),
    team_min: 0,
    team_max: 0,
    contributor_extra: random.word(),
    external_url_info: null,
    external_url_login: null,
    state: {
        value: 256,
        label: 'Finished',
    },
    entries_count: 0,
    entries: [],
    register_time_start: null,
    register_time_end: null,
    run_time_start: '2019-04-19T10:00:00Z',
    run_time_end: '2019-04-19T10:00:00Z',
    vote_time_start: '2019-04-19T16:00:00Z',
    vote_time_end: '2019-04-20T11:15:00Z',
    show_time_start: null,
    show_time_end: null,
    team_required: false,
    created: '2019-04-17T09:52:30.754317Z',
    last_updated: '2019-04-20T22:30:44.323980Z',
    next_state: {
        value: 256,
        label: 'Finished',
    },
    featured: false,
};

export default competitionMock;
