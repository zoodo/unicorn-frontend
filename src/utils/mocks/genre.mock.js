import { random } from 'faker';

const genreMock = {
    id: random.number(),
    category: {
        value: random.number(),
        label: random.word(),
    },
    name: random.word(),
};

export default genreMock;
