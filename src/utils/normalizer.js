export const arrayNormalizer = (arr, key = 'id', modifier = item => item) => {
    return arr.reduce(
        (obj, item) => ({
            ...obj,
            [item[key]]: modifier(item),
        }),
        {}
    );
};
