import { getEventValue } from './formHelpers';

const mockEvent = (type = 'text') => ({
    type,
    value: '42',
    checked: true,
});

describe('utils/formHelpers', () => {
    it('returns event data value from a text type event', () => {
        expect(getEventValue({}, mockEvent())).toEqual('42');
    });

    it('returns event data value from a number type event', () => {
        expect(typeof getEventValue({}, mockEvent('number'))).toEqual('number');
    });

    it('returns event data value from a checked type event', () => {
        expect(getEventValue({}, mockEvent('checkbox'))).toBeTruthy();
    });

    it('returns event data value from an unkown type event', () => {
        expect(getEventValue({}, mockEvent('tast'))).toEqual('42');
    });

    it("returns event if it's not an object", () => {
        expect(getEventValue('hurr durr')).toEqual('hurr durr');
    });

    it('returns target.value if data is not supplied', () => {
        expect(
            getEventValue({
                target: {
                    value: 'snickaboo',
                },
            })
        ).toEqual('snickaboo');
    });
});
