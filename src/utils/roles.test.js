import * as role from './roles';

const userMock = {
    id: 1,
    display_name_format: {
        value: 16,
        label: '%(first)s aka. %(nick)s',
    },
    nick: 'asdf',
    team: null,
    location_tracking_consent: false,
    row: 0,
    seat: null,
    crew: null,
    display_name: ' aka. asdf',
    email: 'mail@mail.com',
    full_name: ' ',
    role: {
        value: 4,
        label: 'Other',
    },
};

describe('utils/roles', () => {
    it('handles calculating that a user has or does not have a role', () => {
        expect(role.userHasRole(userMock, role.ROLE_CREW)).toBeFalsy();
        expect(role.userHasRole(userMock, role.ROLE_OTHER)).toBeTruthy();
    });
});
