import { createNextState } from '@reduxjs/toolkit';
import { getState, _setIsFetching, _setError } from './ducks';

describe('utils/ducks', () => {
    const rootState = {
        testState: {
            data: {},
            isFetching: false,
            error: null,
        },
    };

    it('getState returns state for a given key', () => {
        expect(getState('testState')(rootState)).toEqual(rootState.testState);
    });

    it('updates state with new isFetching state', () => {
        const isFetchingState = createNextState(
            {
                ...rootState.testState,
            },
            draftState => {
                _setIsFetching(draftState, {});
            }
        );
        expect(isFetchingState.isFetching).toBeTruthy();

        const isNotFetchingState = createNextState(isFetchingState, draftState => {
            _setIsFetching(draftState, { payload: false });
        });
        expect(isNotFetchingState.isFetching).toBeFalsy();
    });

    it('updates state with new error message', () => {
        const errorMessage = 'this is an error';
        const hasErrorState = createNextState(
            {
                ...rootState.testState,
            },
            draftState => {
                _setError.reducer(
                    draftState,
                    _setError.prepare({
                        message: errorMessage,
                    })
                );
            }
        );

        expect(hasErrorState.error).toEqual(errorMessage);

        const errorIsResetState = createNextState(hasErrorState, draftState => {
            _setError.reducer(draftState, _setError.prepare());
        });

        expect(errorIsResetState.error).toBeNull();
    });
});
