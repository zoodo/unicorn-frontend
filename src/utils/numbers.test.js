import { formatNumber } from './numbers';

describe('utils/numbers', () => {
    it('formats single digit numbers based on their size', () => {
        expect(formatNumber(0)).toEqual('0');
        expect(formatNumber(1)).toEqual('1st');
        expect(formatNumber(2)).toEqual('2nd');
        expect(formatNumber(3)).toEqual('3rd');
        expect(formatNumber(5)).toEqual('5th');
    });
    it('formats multi digit numbers based on their size', () => {
        expect(formatNumber(140)).toEqual('140');
        expect(formatNumber(141)).toEqual('141st');
        expect(formatNumber(142)).toEqual('142nd');
        expect(formatNumber(143)).toEqual('143rd');
        expect(formatNumber(145)).toEqual('145th');
    });

    it('can insert a space on demand', () => {
        expect(formatNumber(0, true)).toEqual('0');
        expect(formatNumber(1, true)).toEqual('1 st');
        expect(formatNumber(2, true)).toEqual('2 nd');
        expect(formatNumber(3, true)).toEqual('3 rd');
        expect(formatNumber(5, true)).toEqual('5 th');
    });
});
