import { arrayNormalizer } from './normalizer';

const arrMock = [
    { id: 1, name: 'JoMs' },
    { id: 2, name: 'BamseJoMs' },
    { id: 3, name: 'eriktm' },
    { id: 4, name: 'heigren' },
];

describe('utils/normalizer', () => {
    it('can normalize an array of objects into an indexed object of items', () => {
        expect(arrayNormalizer(arrMock)).toEqual({
            1: { id: 1, name: 'JoMs' },
            2: { id: 2, name: 'BamseJoMs' },
            3: { id: 3, name: 'eriktm' },
            4: { id: 4, name: 'heigren' },
        });
    });
});
