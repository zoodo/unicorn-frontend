import { parseError } from './error';

const errorMock = {
    body: {
        genre: ['This field is required.'],
        name: ['This field is also required.'],
        age: 'This field is definitivily required',
    },
};

describe('utils/error', () => {
    it('splits error object into array of strings', () => {
        expect(parseError(errorMock)).toEqual([
            'genre: This field is required.',
            'name: This field is also required.',
            'age: This field is definitivily required',
        ]);
    });

    it('returns error array if supplied error is an array in stead of an object', () => {
        expect(Object.values(errorMock.body)).toEqual([
            ['This field is required.'],
            ['This field is also required.'],
            'This field is definitivily required',
        ]);
    });
});
