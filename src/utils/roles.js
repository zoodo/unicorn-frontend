import get from 'lodash/get';

export const ROLE_CREW = 1;
export const ROLE_PARTICIPANT = 2;
export const ROLE_OTHER = 4;
export const ROLE_ANONYMOUS = 8;
export const ROLE_JURY = 16;

export const ROLES = [ROLE_CREW, ROLE_PARTICIPANT, ROLE_OTHER, ROLE_ANONYMOUS, ROLE_JURY];

export const roleLabels = {
    [ROLE_CREW]: 'Crew',
    [ROLE_PARTICIPANT]: 'Participant',
    [ROLE_OTHER]: 'Other',
    [ROLE_ANONYMOUS]: 'Anonymous',
    [ROLE_JURY]: 'Jury',
};

export const userHasRole = (user, role) => get(user, 'role.value') === role;
