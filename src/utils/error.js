export const parseError = error => {
    const { body } = error;

    if (Array.isArray(body)) {
        // TODO Do something useful with error
        return error;
    } else if (typeof body === 'object') {
        return Object.entries(body).map(([key, err]) => {
            const description = key === 'detail' ? '' : key + ': ';

            if (Array.isArray(err)) {
                return description + err.join('. ');
            }

            return description + err;
        });
    }
};
