import { unicornFetch, fetchToken, authFetch, appendToUrlObject, objectToQuery } from './apiFetch';

describe('utils/apiFetch', () => {
    const API_URL = process.env.REACT_APP_API_URL;
    const d = { data: '12345' };

    beforeEach(() => {
        fetch.resetMocks();
    });

    it('unicornFetch transforms the url', () => {
        fetch.mockResponseOnce(JSON.stringify(d));
        unicornFetch({ url: 'competitions' }, { foo: 'bar' });
        expect(fetch.mock.calls[0][0].includes('foo=bar')).toBeTruthy();
    });

    it('unicornFetch adds parameters to the url', () => {
        fetch.mockResponseOnce(JSON.stringify(d));
        unicornFetch({ url: 'competitions' });
    });

    it('unicornFetch returns successfully with data', () => {
        fetch.mockResponseOnce(JSON.stringify(d));
        unicornFetch({ url: 'competitions' })
            .then(d => d)
            .catch(e => console.log(e));
        expect(fetch.mock.calls[0][0]).toEqual(`${API_URL}competitions/?`);
    });

    it('fetchToken sets correct parameters', () => {
        fetch.mockResponseOnce(
            JSON.stringify({
                access_token: 'access_token',
                expires_in: 3600,
                token_type: 'Bearer',
                scope: 'read_userdata_extended write_userdata_extended',
                refresh_token: 'refresh_token',
            })
        );
        fetchToken({ token_data: 'foobar_token_lol' });

        const call = fetch.mock.calls[0];
        expect(call[0].includes('token_data=foobar_token_lol')).toBeTruthy();
        expect(call[1].method === 'POST');
        expect(call[1].headers['content-type']).toEqual('application/x-www-form-urlencoded');
    });

    it('authFetch sets correct parameters', () => {
        fetch.mockResponseOnce(JSON.stringify({ success: true }));

        authFetch({
            url: '/api/accounts/logout',
            method: 'POST',
            mode: 'no-cors',
        });

        const call = fetch.mock.calls[0];
        expect(call[1].method === 'POST');
        expect(call[1].headers['content-type']).toEqual('application/json');
    });

    it('appendToUrlObject appends string to end of url within an object', () => {
        const url = appendToUrlObject(
            {
                foo: 'bar',
                bar: 'foo',
                url: 'snickaboo/huppedupp',
            },
            'foobar'
        );
        expect(url.url).toEqual('snickaboo/huppedupp/foobar');
        expect(url.foo).toEqual('bar');
        expect(url.bar).toEqual('foo');
    });

    it('objectToQuery adds an object to URL as url parameters', () => {
        const params = {
            foo: 'bar',
            bar: 'foo',
            foobar: ['foo', 'bar'],
        };
        expect(objectToQuery(params)).toEqual('foo=bar&bar=foo&foobar=foo&foobar=bar');

        // test CSV functionality
        expect(objectToQuery(params, true)).toEqual('foo=bar&bar=foo&foobar=foo,bar');
    });

    it('failed fetch returns error object with both statuses and body', () => {
        fetch.mockResponseOnce(JSON.stringify(d), { status: 400 });
        unicornFetch({ url: 'competitions' }).catch(e => {
            expect(e.status).toEqual(400);
            expect(e.ok).toBeFalsy();
            expect(e.statusText).toEqual('Bad Request');
            expect(e.body).toEqual(d);
        });
    });
});
