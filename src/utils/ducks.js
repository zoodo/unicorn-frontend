import get from 'lodash/get';

export const getState = name => state => get(state, name, {});

export const _setIsFetching = (state, { payload = true }) => {
    state.isFetching = payload;
};

export const _setError = {
    reducer: (state, { payload }) => {
        state.error = payload;
    },
    prepare: error => {
        if (error) {
            return { payload: error.message };
        } else {
            return { payload: null };
        }
    },
};

export const commonReducers = {
    setIsFetching: _setIsFetching,
    setError: _setError,
};
