import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import groupBy from 'lodash/groupBy';
import { convertFromRaw, convertFromHTML, ContentState } from 'draft-js';

export const CLOSED = 1;
export const REGISTRATION_OPEN = 2;
export const REGISTRATION_CLOSED = 4;
export const RUNNING_OPEN = 8;
export const RUNNING_CLOSED = 16;
export const VOTING_OPEN = 32;
export const VOTING_CLOSED = 64;
export const SHOWTIME = 128;
export const FINISHED = 256;

export const isExternal = competition => !!competition.external_url_info || !!competition.external_url_login;
export const hasPreRegistration = competition => !!competition.register_time_start || !!competition.register_time_end;
export const hasVote = competition => !!competition.vote_time_start || !!competition.vote_time_end;
export const hasTeams = competition => !!competition.team_min || !!competition.team_max;
export const hasFileupload = competition => !!competition.fileupload || !!competition.fileupload.length;
export const hasToornament = competition => !!competition.toornament;

export const getFinishedCompetitions = competitions => competitions.filter(c => c.state.value === FINISHED);
export const getUnfinishedCompetitions = competitions => competitions.filter(c => c.state.value !== FINISHED);
export const filterCompetitionsByState = (competitions, state) => competitions.filter(c => c.state.value === state);
export const filterCompetitionsByExpression = (competitions, expression) =>
    competitions.filter(c => expression(c.state.value));

export const groupCompetitionsByKey = (competitions, key = 'run_time_start') => {
    dayjs.extend(relativeTime);

    const today = dayjs();

    return groupBy(competitions, competition => {
        const startTime = dayjs(competition[key]);

        // Is the competition within the next 24hr?
        if (startTime.isBefore(today.add(24, 'hour')) && startTime.isAfter(today)) {
            // Is the competition within the next 6hr?
            // Format it a bit special
            if (startTime.isBefore(today.add(6, 'hour'))) {
                return `In ${today.to(startTime, 'hour')}`;
            } else {
                return 'Later today';
            }
        }

        return startTime.format('dddd, MMMM D');
    });
};

export const sortByRunTimeStart = (a, b) => dayjs(a.run_time_start).unix() - dayjs(b.run_time_start).unix();

/**
 * Convert draft-js raw output or HTML to ContentState
 * @param {string} rawData Rules in a stringified rawState or HTML format
 */
export const convertRawToContentState = rawData => {
    let raw = ContentState.createFromText('');

    if (typeof rawData === 'string') {
        try {
            raw = convertFromRaw(JSON.parse(rawData));
        } catch (_) {
            try {
                const blocksFromHTML = convertFromHTML(rawData);
                raw = ContentState.createFromBlockArray(blocksFromHTML.contentBlocks, blocksFromHTML.entityMap);
            } catch (_) {
                raw = ContentState.createFromText('');
            }
        }
    }

    return raw;
};
