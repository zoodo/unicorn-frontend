import get from 'lodash/get';
import { store } from '../store';
import { logout, getAccessToken } from '../ducks/authentication.duck';

export const API_URL = process.env.REACT_APP_API_URL;
export const AUTH_URL = process.env.REACT_APP_AUTH_URL;
export const OSCAR_LINK = process.env.REACT_APP_OSCAR_LINK;

export const unicornFetch = ({ url, ...rest }, payload = {}) => {
    url = API_URL + url + '/';

    return apiFetch({ url, ...rest }, payload);
};

export const fetchToken = payload => {
    const url = AUTH_URL + '/o/token/?' + objectToQuery(payload);
    return apiFetch({ url, method: 'POST', headers: { 'content-type': 'application/x-www-form-urlencoded' } });
};

export const authFetch = ({ url, ...rest }, payload = {}) => {
    url = AUTH_URL + url + '/';

    return apiFetch({ url, ...rest }, payload);
};

const apiFetch = ({ url, method = 'GET', headers = {} }, payload = {}) => {
    const options = {
        credentials: 'same-origin',
        headers: {
            ...headers,
        },
    };

    const token = getAccessToken(store.getState());

    if (token) {
        options.headers.Authorization = 'JWT ' + token;
    }

    if (method === 'GET') {
        url = `${url}?${objectToQuery({ ...payload }, true)}`;
    } else {
        options.method = method;
        options.body = typeof payload === 'object' ? JSON.stringify(payload) : payload;

        if (!options.headers['content-type']) {
            options.headers['content-type'] = 'application/json';
        }
    }

    return fetch(url, options)
        .then(res => {
            if (res.status >= 200 && res.status < 300) {
                return res;
            }

            if (res.status === 401) {
                if (!!store.getState().authentication.token) {
                    store.dispatch(logout());
                }
            }

            return res.json().then(json => {
                return Promise.reject({
                    status: res.status,
                    ok: false,
                    statusText: res.statusText,
                    body: json,
                });
            });
        })
        .then(response => {
            if (response.status === 204 || response.status === 205) {
                return null;
            }

            return response.json();
        })
        .catch(err => {
            throw err;
        });
};

export const appendToUrlObject = (obj, str) => ({
    ...obj,
    url: obj.url + '/' + str,
});

export const getOscarUrl = (state = '') => {
    const redirectUri = get(document, 'location.origin', '') + '/login';
    let uriState = '';

    if (typeof state === 'object') {
        uriState = JSON.stringify(state);
    } else {
        uriState = state;
    }

    return OSCAR_LINK + '&' + objectToQuery({ redirect_uri: redirectUri, state: uriState });
};

export const objectToQuery = (object = {}, arrayAsCsv = false) =>
    Object.entries(object)
        .reduce((params, [key, value]) => {
            if (!Array.isArray(value)) {
                params.set(key, value);
                return params;
            }
            if (arrayAsCsv) {
                params.set(key, value.join(','));
            } else {
                value.forEach(entry => params.append(key, entry));
            }
            return params;
        }, new URLSearchParams())
        .toString()
        .replace(/%2C/g, ',');
