import get from 'lodash/get';

export const getEventValue = (event, data) => {
    if (typeof event !== 'object') {
        return event;
    }

    if (data) {
        switch (data.type) {
            case 'number':
                return Number(data.value);

            case 'text':
                return data.value;

            case 'checkbox':
                return data.checked;

            default:
                return data.value;
        }
    }

    return get(event, 'target.value') ? event.target.value : event;
};
